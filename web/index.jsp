<%-- 
    Document   : index
    Created on : Jan 9, 2016, 4:07:47 PM
    Author     : czara
--%>

<jsp:useBean id="ufacade" scope="page" class="facades.UsuarioFacade"/>
<jsp:useBean id="rfacade" scope="page" class="facades.RolFacade"/>
<jsp:useBean id="sfacade" scope="page" class="facades.SucursalFacade"/>

<%
String error = "";
if(request.getParameter("email")!=null&&request.getParameter("pass")!=null){
    String mail=request.getParameter("email");
    String pass=tools.MD5.getMD5(request.getParameter("pass"));
    String key=(String)session.getAttribute("key");
    if(key.equals(request.getParameter("captcha"))){
        beans.Usuario usuario=ufacade.getUsuarioByMailPass(mail,pass);
        if(usuario!=null){
            session.setAttribute("usuario",usuario);
            session.setAttribute("rol", rfacade.getRolByID(usuario.getRol().getIdrol()));
            if(usuario.getSucursal()!=null){
                beans.Sucursal sucursal = sfacade.getSucursalByID(usuario.getSucursal().getIdsucursal());
                session.setAttribute("sucursal", sucursal);
                session.setAttribute("impuesto", sucursal.getIva().getPorcentaje());
            }
            response.sendRedirect(request.getContextPath()+"/principal.jsp");
        }else{
            error += "El email/pass no existe/coincide.";
        }
    }else{
        error += "El captcha no coincide.";
    }
}
%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="TPOS - Terminal Point of Sale">
        <meta name="author" content="MI. Carlos Zarate">
        <title>TPOS - Terminal Point of Sale</title>
        <link href="<%=request.getContextPath()%>/css/sb-admin.css" rel="stylesheet">
        <link href="<%=request.getContextPath()%>/css/bootstrap.css" rel="stylesheet">
        <link href="<%=request.getContextPath()%>/images/favicon.ico" rel="shortcut icon">
        <link href="<%=request.getContextPath()%>/font-awesome/css/font-awesome.css" rel="stylesheet">
        <script>
            function login(){
                if(document.getElementById('email').validity.valid && document.getElementById('pass').validity.valid && document.getElementById('captcha').validity.valid){
                    document.forms['form1'].submit();
                }
            }
        </script>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="login-panel panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title"><img src="<%=request.getContextPath()%>/images/logo.png"></h3>
                        </div>
                        <div class="panel-body">
                            <form role="form" name="form1" id="form1" method="post" action="<%=request.getContextPath()%>/index.jsp">
                                <fieldset>
                                    <%if(!error.equals("")){%>
                                        <div class="alert alert-danger alert-dismissable">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                            <b>Error:</b> <%=error%> 
                                        </div>
                                    <%}%>
                                    <div class="form-group">
                                        <input class="form-control input-sm" placeholder="EMail" name="email" id="email" type="email" autofocus required/>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control input-sm" placeholder="Password" name="pass" id="pass" type="password" required/>
                                    </div>
                                    <div class="form-group">
                                        Captcha: <img src="captcha.jsp"/>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control input-sm" placeholder="Captcha" name="captcha" id="captcha" type="text" maxlength="5" autocomplete="off" required/>
                                    </div>
                                    <button class="btn btn-lg btn-primary btn-block btn-sm" onclick="javascript:login();">Ingresar</button>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-1.10.2.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/sb-admin.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/bootstrap.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    </body>
</html>

<%
ufacade.close();
rfacade.close();
sfacade.close();
%>