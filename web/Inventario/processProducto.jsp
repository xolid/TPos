<%-- 
    Document   : process2
    Created on : Feb 7, 2016, 7:18:03 PM
    Author     : czara
--%>
 
<%@page import="java.util.*"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page import="org.json.simple.JSONArray"%>

<jsp:useBean id="facade" scope="page" class="facades.ProductoFacade"/>

<%
int idalmacen = 0;
if(request.getParameter("idalmacen")!=null){
    idalmacen = Integer.parseInt(String.valueOf(request.getParameter("idalmacen")));
}
int idsucursal = 0;
if(request.getParameter("idsucursal")!=null){
    idsucursal = Integer.parseInt(String.valueOf(request.getParameter("idsucursal")));
}
String[] cols = {"tipo", "codigo" , "pnombre" , "clave", "unidad" , "cnombre" };
JSONObject result = new JSONObject();
JSONArray array = new JSONArray();
int amount = 10;
int start = 0;
int echo = 0;
int col = 0;
String dir = "asc";
String sStart = request.getParameter("start");
String sAmount = request.getParameter("length");
String sEcho = request.getParameter("draw");
String sCol = request.getParameter("order[0][column]");
String sdir = request.getParameter("order[0][dir]");
String buscar = request.getParameter("search[value]");
if (sStart != null) {
    start = Integer.parseInt(sStart);
    if (start < 0)
        start = 0;
}
if (sAmount != null) {
    amount = Integer.parseInt(sAmount);
    if (amount < 10 || amount > 100)
        amount = 10;
}
if (sEcho != null) {
    echo = Integer.parseInt(sEcho);
}
if (sCol != null) {
    col = Integer.parseInt(sCol);
    if (col < 0 || col > 5)
        col = 0;
}
if (sdir != null) {
    if (!sdir.equals("asc"))
        dir = "desc";
}
String colName = cols[col];
long total = facade.getTotal();

List<Object[]> productos = new ArrayList();
if(idalmacen != 0){
    productos = facade.getProductoForProcessByAlmacenMARKED(buscar, idalmacen, colName, dir, start, amount);
}
if(idsucursal != 0){
    productos = facade.getProductoForProcessBySucursalMARKED(buscar, idsucursal, colName, dir, start, amount);
}

for(Object[] p:productos){
    JSONArray ja = new JSONArray();
    ja.add(new tools.Tipo().getNombre(Integer.parseInt(String.valueOf(p[7]))));
    if(p[6]==null){
        ja.add("<a href=\"#\" onclick=\"javascript:seleccionar("+Integer.valueOf(String.valueOf(p[0]))+",'"+String.valueOf(p[1]).replace("\"","&quot")+"','"+String.valueOf(p[2])+"','"+String.valueOf(p[3])+"','"+String.valueOf(p[4])+"','"+String.valueOf(p[5])+"',"+Integer.valueOf(String.valueOf(p[7]))+",'"+new tools.Tipo().getNombre(Integer.parseInt(String.valueOf(p[7])))+"')\">"+String.valueOf(p[2])+"</a>");
    }else{
        ja.add(String.valueOf(p[2]));
    }
    ja.add(String.valueOf(p[1]));
    ja.add(String.valueOf(p[3]));
    ja.add(String.valueOf(p[4]));
    ja.add(String.valueOf(p[5]));
    array.add(ja);
}
long totalAfterFilter = facade.getProductoForProcessCount(buscar);
result.put("draw", sEcho);
result.put("recordsTotal", total);
result.put("recordsFiltered", totalAfterFilter);
result.put("data", array);
response.setContentType("application/json");
response.setHeader("Cache-Control", "no-store");
out.print(result);

facade.close();   
%>

