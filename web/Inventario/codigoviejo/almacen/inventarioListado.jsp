<%-- 
    Document   : usuarioListado
    Created on : Jan 9, 2016, 4:50:18 PM
    Author     : czara
--%>

<jsp:useBean id="facade" scope="page" class="facades.AlmacenFacade"/>

<%
int idalmacen = 0;
if(request.getParameter("idalmacen")!=null){
    idalmacen = Integer.parseInt(String.valueOf(request.getParameter("idalmacen")));
}
%>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Modulo de Inventario - Ajuste de Inventario de Almacenes</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <select id="almacen_idalmacen" name="almacen_idalmacen" class="form-control input-sm" onchange="javascript:change();">
                    <option value="0">SELECCIONE UN ALMACEN</option>
                    <%for(beans.Almacen almacen:facade.getAlmacenAll()){%>
                        <option value="<%=almacen.getIdalmacen()%>" <%=almacen.getIdalmacen()==idalmacen?"SELECTED":""%>><%=almacen.getNombre()%></option>
                    <%}%>
                </select>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-condensed table-hover" id="dataTablesInventario">
                        <thead>
                            <th class="col-lg-1">Tipo</th>
                            <th class="col-lg-3">[B] Codigo</th>
                            <th class="col-lg-5">[B] Producto</th>
                            <th class="col-lg-1">Cantidad</th>
                            <th class="col-lg-1">Minimo</th>
                            <th class="col-lg-1">Maximo</th>
                        </thead>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
            <div class="panel-footer">
                <div class="row">
                    <div class="col-lg-12">
                        <button class="btn btn-primary btn-sm <%=idalmacen==0?"disabled":""%>" type="button" onclick="javascript:go2to('Inventario/almacen/inventario.jsp?idalmacen=<%=idalmacen%>','page-wrapper');"><i class="fa fa-asterisk fa-fw"></i> Nuevo</button>
                    </div>
                </div>
            </div>
            <!-- /.panel-footer -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<script>
    $(document).ready(function() {
        $('#dataTablesInventario').dataTable({
            "retrieve": true,
            "processing": true,  
            "serverSide": true,
            "ajax": "Inventario/almacen/processInventario.jsp?idalmacen=<%=idalmacen%>",
            "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
            }
        });
    });
    
    function change(){
        var almacen = document.getElementById("almacen_idalmacen").options[document.getElementById("almacen_idalmacen").selectedIndex].value;
        go2to('Inventario/almacen/inventarioListado.jsp?idalmacen='+almacen, 'page-wrapper');
    }
</script>

<%
facade.close();
%>