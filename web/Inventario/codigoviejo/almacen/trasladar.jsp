<%-- 
    Document   : trasladar
    Created on : 6/05/2018, 12:23:46 PM
    Author     : czara
--%>

<jsp:useBean id="facade" scope="page" class="facades.InventarioalmacenFacade"/>
<jsp:useBean id="afacade" scope="page" class="facades.AlmacenFacade"/>
<jsp:useBean id="pfacade" scope="page" class="facades.ProductoFacade"/>

<%
int idalmacen_origen = 0;
if(request.getParameter("idalmacen_origen")!=null){
    idalmacen_origen = Integer.parseInt(String.valueOf(request.getParameter("idalmacen_origen")));
}
int idalmacen_destino = 0;
if(request.getParameter("idalmacen_destino")!=null){
    idalmacen_destino = Integer.parseInt(String.valueOf(request.getParameter("idalmacen_destino")));
}
int producto_idproducto = 0;
if(request.getParameter("producto_idproducto")!=null){
    producto_idproducto = Integer.parseInt(String.valueOf(request.getParameter("producto_idproducto")));
}
int origen_final = 0;
if(request.getParameter("origen_final")!=null){
    origen_final = Integer.parseInt(String.valueOf(request.getParameter("origen_final")));
}
int destino_final = 0;
if(request.getParameter("destino_final")!=null){
    destino_final = Integer.parseInt(String.valueOf(request.getParameter("destino_final")));
}

//obtener origen
beans.Inventarioalmacen origen = facade.getInventarioalmacenByAlmacenANDProducto(idalmacen_origen, producto_idproducto);
beans.Inventarioalmacen destino = facade.getInventarioalmacenByAlmacenANDProducto(idalmacen_destino, producto_idproducto);
boolean update = false;
if(origen !=null){
    //actualizar origen
    origen.setCantidad(origen_final);
    facade.updateInventarioalmacen(origen);
    //actualizar destino
    if(destino != null){
        destino.setCantidad(destino_final);
        facade.updateInventarioalmacen(destino);
    }else{
        destino = new beans.Inventarioalmacen(afacade.getAlmacenByID(idalmacen_destino),pfacade.getProductoByID(producto_idproducto),destino_final,0,destino_final);
        facade.saveInventarioalmacen(destino);
    }
    update = true;
}
%>

<script lang="javascript">
    <%if(update){%>
        BootstrapDialog.alert({
            title: "Mensaje del Sistema",
            message: "<strong>Se realizo el traslado de inventario con exito</strong><br/>",
            type: 'type-success',
            closable: false,
            btnOKClass: 'btn-success'
        });
    <%}else{%>
        BootstrapDialog.alert({
            title: "Mensaje del Sistema",
            message: "<strong>No se pudo realizar el traslado de inventario</strong><br/>",
            type: 'type-danger',
            closable: false,
            btnOKClass: 'btn-danger'
        });
    <%}%>
    javascript:go2to('Inventario/almacen/movimiento.jsp', 'page-wrapper');
</script>

<%
facade.close();
afacade.close();
pfacade.close();
%>