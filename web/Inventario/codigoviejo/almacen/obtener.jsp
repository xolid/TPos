<%-- 
    Document   : trasladar
    Created on : 6/05/2018, 12:23:46 PM
    Author     : czara
--%>

<jsp:useBean id="facade" scope="page" class="facades.InventarioalmacenFacade"/>

<%
int idalmacen_origen = 0;
if(request.getParameter("idalmacen_origen")!=null){
    idalmacen_origen = Integer.parseInt(String.valueOf(request.getParameter("idalmacen_origen")));
}
int idalmacen_destino = 0;
if(request.getParameter("idalmacen_destino")!=null){
    idalmacen_destino = Integer.parseInt(String.valueOf(request.getParameter("idalmacen_destino")));
}
int producto_idproducto = 0;
if(request.getParameter("producto_idproducto")!=null){
    producto_idproducto = Integer.parseInt(String.valueOf(request.getParameter("producto_idproducto")));
}
//obtener disponibles en origen
int origen_disponible = 0;
beans.Inventarioalmacen origen = facade.getInventarioalmacenByAlmacenANDProducto(idalmacen_origen, producto_idproducto);
if(origen != null){
    origen_disponible = origen.getCantidad();
}
//obtener disponibles en destino
int destino_disponible = 0;
beans.Inventarioalmacen destino = facade.getInventarioalmacenByAlmacenANDProducto(idalmacen_destino, producto_idproducto);
if(destino != null){
    destino_disponible = destino.getCantidad();
}
%>

<script lang="javascript">
    window.top.document.getElementById('origen_disponible').value = <%=origen_disponible%>;
    window.top.document.getElementById('destino_disponible').value = <%=destino_disponible%>;
    var select = window.top.document.getElementById('origen_trasladar');
    select.options.length = 0;
    var i = 0;
    while (i <= <%=origen_disponible%>) {
        var option = document.createElement('option');
        option.value = i;
        option.innerHTML = i;
        select.appendChild(option);
        i++;
    }
    window.top.document.getElementById('origen_final').value = <%=origen_disponible%>;
    window.top.document.getElementById('destino_final').value = <%=destino_disponible%>;
    
    window.top.document.getElementById('origen_trasladar').disabled = false;
    window.top.document.getElementById('producto_idproducto').disabled = true;
    window.top.document.getElementById('almacen_idalmacen_origen').disabled = true;
    window.top.document.getElementById('almacen_idalmacen_destino').disabled = true;
    
    window.top.document.getElementById('updateButton').disabled = true;
    window.top.document.getElementById('searchButton').disabled = true;
</script>

<%
facade.close();
%>