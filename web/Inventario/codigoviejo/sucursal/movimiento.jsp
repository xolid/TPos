<%-- 
    Document   : inventario
    Created on : Jan 9, 2016, 7:42:35 PM
    Author     : czara
--%>

<jsp:useBean id="facade" scope="page" class="facades.SucursalFacade"/>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Modulo de Inventario - Movimiento entre Sucursales</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                Movimiento en Sucursales
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <form inventarioe="form" id="form1" name="form1" action="movimiento.jsp" method="post">
                    <div class="row">    
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Sucursal Origen:</label>
                                <select id="sucursal_idsucursal_origen" name="sucursal_idsucursal_origen" class="form-control input-sm">
                                    <option value="0">SELECCIONA UNA SUCURSAL</option>
                                    <%for(beans.Sucursal sucursal:facade.getSucursalAll()){%>
                                        <option value="<%=sucursal.getIdsucursal()%>"><%=sucursal.getNombre()%></option>
                                    <%}%>
                                </select>
                            </div>
                        </div>           
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Sucursal Destino:</label>
                                <select id="sucursal_idsucursal_destino" name="sucursal_idsucursal_destino" class="form-control input-sm">
                                    <option value="0">SELECCIONA UNA SUCURSAL</option>
                                    <%for(beans.Sucursal sucursal:facade.getSucursalAll()){%>
                                        <option value="<%=sucursal.getIdsucursal()%>"><%=sucursal.getNombre()%></option>
                                    <%}%>
                                </select>
                            </div>
                        </div>            
                    </div>
                    <div class="row">
                        <div class="col-lg-1">
                            <div class="form-group">
                                <label>&nbsp;</label>
                                <button type="button" id="searchButton" class="btn btn-info btn-block btn-sm" data-toggle="modal" data-target="#myModalSearch"><i class="fa fa-arrow-circle-right fa-fw"></i></button>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label for="producto_idproducto">Producto: </label>
                                <select id="producto_idproducto" name="producto_idproducto" class="form-control input-sm"></select>
                            </div> 
                        </div>
                        <div class="col-lg-2">
                            <div class="form-group">
                                <label for="codigo">Codigo: </label>
                                <input class="form-control input-sm" id="codigo" name="codigo" type="number" readonly/>
                            </div>      
                        </div>
                        <div class="col-lg-2">
                            <div class="form-group">
                                <label for="clave">Clave: </label>
                                <input class="form-control input-sm" id="clave" name="clave" type="text" readonly/>
                            </div>  
                        </div>
                        <div class="col-lg-2">
                            <div class="form-group">
                                <label for="unidad">Unidad: </label>
                                <input class="form-control input-sm" id="unidad" name="unidad" type="text" readonly/>
                            </div>  
                        </div>
                        <div class="col-lg-2">
                            <div class="form-group">
                                <label for="categoria">Categoria: </label>
                                <input class="form-control input-sm" id="categoria" name="categoria" type="text" readonly/>
                            </div>  
                        </div>        
                    </div>
                    <div class="row">
                        <div class="col-lg-1">
                            <div class="form-group">
                                <label>&nbsp;</label>
                                <button type="button" id="updateButton" class="btn btn-info btn-block btn-sm" onclick="javascript:updateCantidades();"><i class="fa fa-arrow-circle-right fa-fw"></i></button>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="form-group">
                                <label for="disponible">Disponibles [Origen]:</label>
                                <input class="form-control input-sm" id="origen_disponible" name="origen_disponible" type="number" disabled="true"/>
                            </div>      
                        </div>
                        <div class="col-lg-2 col-lg-offset-2">
                            <div class="form-group">
                                <label for="trasladar">Trasladar [Origen-Destino]:</label>
                                <select class="form-control input-sm" id="origen_trasladar" name="origen_trasladar" onchange="javascript:updateFinal();" disabled="true"></select>
                            </div>      
                        </div>
                        <div class="col-lg-2 col-lg-offset-2">
                            <div class="form-group">
                                <label for="actual">Existentes [Destino]:</label>
                                <input class="form-control input-sm" id="destino_disponible" name="destino_disponible" type="number" disabled="true"/>
                            </div>      
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-2 col-lg-offset-1">
                            <div class="form-group">
                                <label for="origen_final">Restates [Origen]:</label>
                                <input class="form-control input-sm" id="origen_final" name="origen_final" type="number" disabled="true"/>
                            </div>      
                        </div>
                        <div class="col-lg-2 col-lg-offset-6">
                            <div class="form-group">
                                <label for="final">Resultado [Destino]:</label>
                                <input class="form-control input-sm" id="destino_final" name="destino_final" type="number" disabled="true"/>
                            </div>      
                        </div>
                    </div>
                </form>                
            </div>
            <div class="panel-footer">
                <div class="row">
                    <div class="col-lg-12">
                        <button class="btn btn-primary btn-sm" type="button" id="execute" onclick="javascript:trasladar();" disabled="true"><i class="fa fa-play-circle fa-fw"></i> Ejecutar</button>
                        <button class="btn btn-default btn-sm" type="button" onclick="javascript:go2to('Inventario/sucursal/movimiento.jsp', 'page-wrapper');"><i class="fa fa-refresh fa-fw"></i> Restablecer</button>
                    </div>
                </div>
            </div>
            <!-- /.panel-footer -->
        </div>
        <!-- /.panel -->
    </div>
</div>
<div class="modal fade" id="myModalSearch" tabindex="-1" role="dialog" aria-labelledby="myModalLabelSearch" aria-hidden="true">
    <div class="modal-dialog modal-xlg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Seleccionar Producto</h4>
            </div>
            <div class="modal-body">
                <object data="<%=request.getContextPath()%>/Inventario/buscarProductoPROD.jsp" width="100%" height="370">
                    <embed src="<%=request.getContextPath()%>/Inventario/buscarProductoPROD.jsp" width="100%" height="370"></embed>
                </object>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<script type="text/javascript">
            
    window.closeModal = function(){
        $('#myModalSearch').modal('hide');   
    };
    
    function updateTrasladar(){
        var max = document.getElementById("disponible").value;
        var select = document.getElementById('trasladar');
        select.innerHTML = '';
        if(max > 0){
            for(var i = 1; i <= max; i++) {
                var option = document.createElement('option');
                option.innerHTML = i;
                option.value = i;
                select.appendChild(option);
            }
        }
    }
    
    function updateCantidades(){
        var str = "";
        if(document.getElementById("producto_idproducto").selectedIndex === -1){str += "* Debe seleccionar un producto.<br/>";}
        if(document.getElementById("sucursal_idsucursal_origen").selectedIndex === 0){str += "* Debe seleccionar un sucursal origen.<br/>";}
        if(document.getElementById("sucursal_idsucursal_destino").selectedIndex === 0){str += "* Debe seleccionar un sucursal destino.<br/>";}
        if(document.getElementById("sucursal_idsucursal_destino").options[document.getElementById("sucursal_idsucursal_destino").selectedIndex].value === document.getElementById("sucursal_idsucursal_origen").options[document.getElementById("sucursal_idsucursal_origen").selectedIndex].value){str += "* Sucursal origen y sucursal destino deben ser distinto.<br/>";}
        if(str === ""){
            var idsucursal_origen = document.getElementById("sucursal_idsucursal_origen").options[document.getElementById("sucursal_idsucursal_origen").selectedIndex].value;
            var idsucursal_destino = document.getElementById("sucursal_idsucursal_destino").options[document.getElementById("sucursal_idsucursal_destino").selectedIndex].value;
            var idproducto = document.getElementById("producto_idproducto").options[document.getElementById("producto_idproducto").selectedIndex].value;
            $.LoadingOverlay("show");  
            var xhr = new XMLHttpRequest();
            xhr.onreadystatechange = function (e) { 
                if (xhr.readyState === 4 && xhr.status === 200) {
                    var scs = xhr.responseText.extractScript();
                    scs.evalScript();
                    $.LoadingOverlay("hide");
                }
            };
            xhr.open("GET", "/TPos/Inventario/sucursal/obtener.jsp?idsucursal_origen="+idsucursal_origen+"&idsucursal_destino="+idsucursal_destino+"&producto_idproducto="+idproducto, true);
            xhr.setRequestHeader('Content-type', 'text/html');
            xhr.send();
        }else{
            BootstrapDialog.alert({
                title: "Mensaje del Sistema",
                message: "<strong>Se encontraron los siguientes errores:</strong><br/><br/>" + str,
                type: 'type-danger',
                closable: false,
                btnOKClass: 'btn-danger'
            });
        }
    }
    
    function updateFinal(){
        var origen_trasladar = document.getElementById("origen_trasladar").options[document.getElementById("origen_trasladar").selectedIndex].value;
        var origen_disponible = document.getElementById("origen_disponible").value;
        var destino_disponible = document.getElementById("destino_disponible").value;
        document.getElementById("origen_final").value = parseInt(origen_disponible) - parseInt(origen_trasladar);
        document.getElementById("destino_final").value = parseInt(destino_disponible) + parseInt(origen_trasladar);
        if(origen_trasladar > 0){
            document.getElementById('execute').disabled = false;
        }else{
            document.getElementById('execute').disabled = true;
        }
    }
    
    function trasladar(){
        BootstrapDialog.confirm({
            title: "Mensaje del Sistema",
            message: "Confirma desea realizar el traslado?",
            type: 'type-warning',
            closable: false,
            btnOKClass: 'btn-warning',
            callback: function(result) {
                if(result) {
                    var idsucursal_origen = document.getElementById("sucursal_idsucursal_origen").options[document.getElementById("sucursal_idsucursal_origen").selectedIndex].value;
                    var idsucursal_destino = document.getElementById("sucursal_idsucursal_destino").options[document.getElementById("sucursal_idsucursal_destino").selectedIndex].value;
                    var idproducto = document.getElementById("producto_idproducto").options[document.getElementById("producto_idproducto").selectedIndex].value;
                    var origen_final = document.getElementById("origen_final").value;
                    var destino_final = document.getElementById("destino_final").value;
                    $.LoadingOverlay("show");  
                    var xhr = new XMLHttpRequest();
                    xhr.onreadystatechange = function (e) { 
                        if (xhr.readyState === 4 && xhr.status === 200) {
                            var scs = xhr.responseText.extractScript();
                            scs.evalScript();
                            $.LoadingOverlay("hide");
                        }
                    };
                    xhr.open("GET", "/TPos/Inventario/sucursal/trasladar.jsp?idsucursal_origen="+idsucursal_origen+"&idsucursal_destino="+idsucursal_destino+"&producto_idproducto="+idproducto+"&origen_final="+origen_final+"&destino_final="+destino_final, true);
                    xhr.setRequestHeader('Content-type', 'text/html');
                    xhr.send();
                }
            }
        });
    }
</script>

<%
facade.close();
%>     