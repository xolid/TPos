<%-- 
    Document   : usuarioListado
    Created on : Jan 9, 2016, 4:50:18 PM
    Author     : czara
--%>

<jsp:useBean id="facade" scope="page" class="facades.SucursalFacade"/>

<%
int idsucursal = 0;
if(request.getParameter("idsucursal")!=null){
    idsucursal = Integer.parseInt(String.valueOf(request.getParameter("idsucursal")));
}
java.util.List<beans.Sucursal> sucursales = facade.getSucursalAll();
%>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Modulo de Inventario - Ajuste de Inventario de Sucursales</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <select id="sucursal_idsucursal" name="sucursal_idsucursal" class="form-control input-sm" onchange="javascript:change();">
                    <option value="0">SELECCIONE UNA SUCURAL</option>
                    <%for(beans.Sucursal sucursal:sucursales){%>
                        <option value="<%=sucursal.getIdsucursal()%>" <%=sucursal.getIdsucursal()==idsucursal?"SELECTED":""%>><%=sucursal.getNombre()%></option>
                    <%}%>
                </select>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-condensed table-hover" id="dataTablesInventario">
                        <thead>
                            <th class="col-lg-1">Tipo</th>
                            <th class="col-lg-3">[B] Codigo</th>
                            <th class="col-lg-5">[B] Producto</th>
                            <th class="col-lg-1">Cantidad</th>
                            <th class="col-lg-1">Minimo</th>
                            <th class="col-lg-1">Maximo</th>
                        </thead>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
            <div class="panel-footer">
                <div class="row">
                    <div class="col-lg-12">
                        <button class="btn btn-primary btn-sm <%=idsucursal==0?"disabled":""%>" type="button" onclick="javascript:go2to('Inventario/sucursal/inventario.jsp?idsucursal=<%=idsucursal%>','page-wrapper');"><i class="fa fa-asterisk fa-fw"></i> Nuevo</button>
                    </div>
                </div>
            </div>
            <!-- /.panel-footer -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<script>
    $(document).ready(function() {
        $('#dataTablesInventario').dataTable({
            "retrieve": true,
            "processing": true,  
            "serverSide": true,
            "ajax": "Inventario/sucursal/processInventario.jsp?idsucursal=<%=idsucursal%>",
            "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
            }
        });
    });
    
    function change(){
        var sucursal = document.getElementById("sucursal_idsucursal").options[document.getElementById("sucursal_idsucursal").selectedIndex].value;
        go2to('Inventario/sucursal/inventarioListado.jsp?idsucursal='+sucursal, 'page-wrapper');
    }
</script>

<%
facade.close();
%>