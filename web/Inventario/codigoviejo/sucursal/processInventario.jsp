<%-- 
    Document   : process2
    Created on : Feb 7, 2016, 7:18:03 PM
    Author     : czara
--%>
 
<%@page import="java.util.*"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page import="org.json.simple.JSONArray"%>

<jsp:useBean id="facade" scope="page" class="facades.InventariosucursalFacade"/>

<%
int idsucursal = 0;
if(request.getParameter("idsucursal")!=null){
    idsucursal = Integer.parseInt(String.valueOf(request.getParameter("idsucursal")));
}

String[] cols = {"tipo","codigo","nombre","cantidad","minimo","maximo" };
JSONObject result = new JSONObject();
JSONArray array = new JSONArray();
int amount = 10;
int start = 0;
int echo = 0;
int col = 0;
String dir = "asc";
String sStart = request.getParameter("start");
String sAmount = request.getParameter("length");
String sEcho = request.getParameter("draw");
String sCol = request.getParameter("order[0][column]");
String sdir = request.getParameter("order[0][dir]");
String buscar = request.getParameter("search[value]");
if (sStart != null) {
    start = Integer.parseInt(sStart);
    if (start < 0)
        start = 0;
}
if (sAmount != null) {
    amount = Integer.parseInt(sAmount);
    if (amount < 10 || amount > 100)
        amount = 10;
}
if (sEcho != null) {
    echo = Integer.parseInt(sEcho);
}
if (sCol != null) {
    col = Integer.parseInt(sCol);
    if (col < 0 || col > 5)
        col = 0;
}
if (sdir != null) {
    if (!sdir.equals("asc"))
        dir = "desc";
}
String colName = cols[col];
long total = facade.getTotal();
List<Object[]> inventarios = facade.getInventariosucursalForProcessJOINProducto(buscar, idsucursal, colName, dir, start, amount);
for(Object[] p:inventarios){
    JSONArray ja = new JSONArray();
    ja.add(new tools.Tipo().getNombre(Integer.valueOf(String.valueOf(p[6]))));
    ja.add("<a href=\"#\" onclick=\"javascript:go2to('Inventario/sucursal/inventario.jsp?id="+Integer.valueOf(String.valueOf(p[0]))+"','page-wrapper')\">"+String.valueOf(p[1])+"</a>");
    ja.add(String.valueOf(p[2]));
    ja.add(Integer.valueOf(String.valueOf(p[3])));
    ja.add(Integer.valueOf(String.valueOf(p[4])));
    ja.add(Integer.valueOf(String.valueOf(p[5])));
    array.add(ja);
}
java.math.BigInteger totalAfterFilter = facade.getInventariosucursalForProcessCountJOINProducto(buscar, idsucursal);
result.put("draw", sEcho);
result.put("recordsTotal", total);
result.put("recordsFiltered", totalAfterFilter.longValue());
result.put("data", array);
response.setContentType("application/json");
response.setHeader("Cache-Continventario", "no-store");
out.print(result);

facade.close();    
%>

