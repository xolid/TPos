<%-- 
    Document   : trasladar
    Created on : 6/05/2018, 12:23:46 PM
    Author     : czara
--%>

<jsp:useBean id="facade" scope="page" class="facades.InventariosucursalFacade"/>
<jsp:useBean id="afacade" scope="page" class="facades.SucursalFacade"/>
<jsp:useBean id="pfacade" scope="page" class="facades.ProductoFacade"/>

<%
int idsucursal_origen = 0;
if(request.getParameter("idsucursal_origen")!=null){
    idsucursal_origen = Integer.parseInt(String.valueOf(request.getParameter("idsucursal_origen")));
}
int idsucursal_destino = 0;
if(request.getParameter("idsucursal_destino")!=null){
    idsucursal_destino = Integer.parseInt(String.valueOf(request.getParameter("idsucursal_destino")));
}
int producto_idproducto = 0;
if(request.getParameter("producto_idproducto")!=null){
    producto_idproducto = Integer.parseInt(String.valueOf(request.getParameter("producto_idproducto")));
}
int origen_final = 0;
if(request.getParameter("origen_final")!=null){
    origen_final = Integer.parseInt(String.valueOf(request.getParameter("origen_final")));
}
int destino_final = 0;
if(request.getParameter("destino_final")!=null){
    destino_final = Integer.parseInt(String.valueOf(request.getParameter("destino_final")));
}

//obtener origen
beans.Inventariosucursal origen = facade.getInventariosucursalBySucursalANDProducto(idsucursal_origen, producto_idproducto);
beans.Inventariosucursal destino = facade.getInventariosucursalBySucursalANDProducto(idsucursal_destino, producto_idproducto);
boolean update = false;
if(origen !=null){
    //actualizar origen
    origen.setCantidad(origen_final);
    facade.updateInventariosucursal(origen);
    //actualizar destino
    if(destino != null){
        destino.setCantidad(destino_final);
        facade.updateInventariosucursal(destino);
    }else{
        destino = new beans.Inventariosucursal(pfacade.getProductoByID(producto_idproducto),afacade.getSucursalByID(idsucursal_destino),destino_final,0,destino_final);
        facade.saveInventariosucursal(destino);
    }
    update = true;
}
%>

<script lang="javascript">
    <%if(update){%>
        BootstrapDialog.alert({
            title: "Mensaje del Sistema",
            message: "<strong>Se realizo el traslado de inventario con exito</strong><br/>",
            type: 'type-success',
            closable: false,
            btnOKClass: 'btn-success'
        });
    <%}else{%>
        BootstrapDialog.alert({
            title: "Mensaje del Sistema",
            message: "<strong>No se pudo realizar el traslado de inventario</strong><br/>",
            type: 'type-danger',
            closable: false,
            btnOKClass: 'btn-danger'
        });
    <%}%>
    javascript:go2to('Inventario/sucursal/movimiento.jsp', 'page-wrapper');
</script>

<%
facade.close();
afacade.close();
pfacade.close();
%>