<%-- 
    Document   : buscar
    Created on : Jan 27, 2016, 6:09:20 PM
    Author     : czara
--%>

<%
int idalmacen = 0;
if(request.getParameter("idalmacen")!=null){
    idalmacen = Integer.parseInt(String.valueOf(request.getParameter("idalmacen")));
}
int idsucursal = 0;
if(request.getParameter("idsucursal")!=null){
    idsucursal = Integer.parseInt(String.valueOf(request.getParameter("idsucursal")));
}
%>

<html>
    <head>
        <title>Herramientas de Busqueda</title>
        <!-- Core CSS - Include with every page -->
        <link href="<%=request.getContextPath()%>/css/bootstrap.css" rel="stylesheet">
        <link href="<%=request.getContextPath()%>/font-awesome/css/font-awesome.css" rel="stylesheet">
        <link href="<%=request.getContextPath()%>/css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">
    </head>
    <body>
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-condensed table-hover" id="dataTablesProducto">
                        <thead>
                            <th class="col-lg-1">Tipo</th>
                            <th class="col-lg-2">[B] Codigo</th>
                            <th class="col-lg-3">[B] Nombre</th>
                            <th class="col-lg-2">[B] Clave</th>
                            <th class="col-lg-1">[B] Unidad</th>
                            <th class="col-lg-3">[B] Categoria</th>
                        </thead>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
        </div>
        <!-- Core Scripts - Include with every page -->
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-1.10.2.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/bootstrap.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/plugins/metisMenu/jquery.metisMenu.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/sb-admin.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/plugins/dataTables/jquery.dataTables.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/plugins/dataTables/dataTables.bootstrap.js"></script>
        <script lang="javascript">
            
            $(document).ready(function() {
                $('#dataTablesProducto').dataTable({
                    "retrieve": true,
                    "processing": true,  
                    "serverSide": true,
                    "ajax": "processProducto.jsp?<%= idalmacen!=0?("idalmacen="+idalmacen):("idsucursal="+idsucursal)%>",
                    "language": {
                        "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
                    }
                });
            });
            
            function seleccionar(id, text, codigo, clave, unidad, categoria, tipo, tipo_nombre){
                var sel = window.top.document.getElementById('producto_idproducto');
                sel.options.length = 0;
                var opt = document.createElement('option');
                opt.value = id;
                opt.innerHTML = text;
                sel.appendChild(opt);
                
                window.top.document.getElementById('codigo').value = codigo;
                window.top.document.getElementById('clave').value = clave;
                window.top.document.getElementById('unidad').value = unidad;
                window.top.document.getElementById('categoria').value = categoria;
                window.top.document.getElementById('tipo').value = tipo_nombre;
                
                if(tipo !== 0){
                    window.top.document.getElementById('cantidad').disabled = true;
                    window.top.document.getElementById('maximo').disabled = true;
                    window.top.document.getElementById('minimo').disabled = true;
                }else{
                    window.top.document.getElementById('cantidad').disabled = false;
                    window.top.document.getElementById('maximo').disabled = false;
                    window.top.document.getElementById('minimo').disabled = false;
                }
                
                window.parent.closeModal();
            }
            
        </script>
    </body>
</html>