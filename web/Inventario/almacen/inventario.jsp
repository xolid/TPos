<%-- 
    Document   : inventarioalmacen
    Created on : Jan 9, 2016, 7:42:35 PM
    Author     : czara
--%>

<jsp:useBean id="inventarioalmacen" class="beans.Inventarioalmacen" scope="page"/>
<jsp:setProperty name="inventarioalmacen" property="idinventarioalmacen" />
<jsp:setProperty name="inventarioalmacen" property="cantidad" />
<jsp:setProperty name="inventarioalmacen" property="minimo" />
<jsp:setProperty name="inventarioalmacen" property="maximo" />
<jsp:useBean id="facade" scope="page" class="facades.InventarioalmacenFacade"/>
<jsp:useBean id="afacade" scope="page" class="facades.AlmacenFacade"/>
<jsp:useBean id="pfacade" scope="page" class="facades.ProductoFacade"/>

<%if(request.getParameter("accion")!=null){
    if(!String.valueOf(request.getParameter("almacen_idalmacen")).equals("")){inventarioalmacen.setAlmacen(afacade.getAlmacenByID(Integer.parseInt(String.valueOf(request.getParameter("almacen_idalmacen")))));}
    if(!String.valueOf(request.getParameter("producto_idproducto")).equals("")){inventarioalmacen.setProducto(pfacade.getProductoByID(Integer.parseInt(String.valueOf(request.getParameter("producto_idproducto")))));}
    if(request.getParameter("accion").equals("1")){
        if(inventarioalmacen.getIdinventarioalmacen()==0){
            facade.saveInventarioalmacen(inventarioalmacen);
        }else{
            facade.updateInventarioalmacen(inventarioalmacen);
        }
    }
    if(request.getParameter("accion").equals("2")){
        facade.deleteInventarioalmacen(inventarioalmacen);        
    }
}
if(request.getParameter("id")!=null){
    int idinventarioalmacen=Integer.parseInt((String)request.getParameter("id"));
    inventarioalmacen=facade.getInventarioalmacenByID(idinventarioalmacen);
}else{
    inventarioalmacen.setIdinventarioalmacen(0);
    if(request.getParameter("idalmacen")!=null){
        inventarioalmacen.setAlmacen(afacade.getAlmacenByID(Integer.parseInt(String.valueOf(request.getParameter("idalmacen")))));
    }
    inventarioalmacen.setProducto(null);
    inventarioalmacen.setCantidad(0);
    inventarioalmacen.setMinimo(0);
    inventarioalmacen.setMaximo(0);
}%>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Modulo de Inventario - Ajuste de Inventario de Almacen</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                Definicion del Producto Inventariado en Almacen [<%=inventarioalmacen.getAlmacen().getNombre()%>]
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <form inventarioalmacene="form" id="form1" name="form1" action="inventario.jsp" method="post">
                    <input type="hidden" id="idinventarioalmacen" name="idinventarioalmacen" value="<%=inventarioalmacen.getIdinventarioalmacen()%>"/>
                    <input type="hidden" id="almacen_idalmacen" name="almacen_idalmacen" value="<%=inventarioalmacen.getAlmacen().getIdalmacen()%>"/>
                    <div class="row">
                        <div class="col-lg-1">
                            <div class="form-group">
                                <label>&nbsp;</label>
                                <button type="button" class="btn btn-info btn-block btn-sm" data-toggle="modal" data-target="#myModalSearch"><i class="fa fa-arrow-circle-right fa-fw"></i></button>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label for="producto_idproducto">Producto: </label>
                                <select id="producto_idproducto" name="producto_idproducto" class="form-control input-sm">
                                    <%if(inventarioalmacen.getProducto()!=null){%>
                                        <option value="<%=inventarioalmacen.getProducto().getIdproducto()%>"><%=inventarioalmacen.getProducto().getNombre()%></option>
                                    <%}%>
                                </select>
                            </div> 
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label for="codigo">Codigo: </label>
                                <input class="form-control input-sm" id="codigo" name="codigo" type="number" value="<%=inventarioalmacen.getProducto()!=null?inventarioalmacen.getProducto().getCodigo():""%>" readonly/>
                            </div>      
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label for="clave">Clave: </label>
                                <input class="form-control input-sm" id="clave" name="clave" type="text" value="<%=inventarioalmacen.getProducto()!=null?inventarioalmacen.getProducto().getClave():""%>" readonly/>
                            </div>  
                        </div>
                        <div class="col-lg-2">
                            <div class="form-group">
                                <label for="unidad">Unidad: </label>
                                <input class="form-control input-sm" id="unidad" name="unidad" type="text" value="<%=inventarioalmacen.getProducto()!=null?inventarioalmacen.getProducto().getUnidad():""%>" readonly/>
                            </div>  
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label for="categoria">Categoria: </label>
                                <input class="form-control input-sm" id="categoria" name="categoria" type="text" value="<%=inventarioalmacen.getProducto()!=null?inventarioalmacen.getProducto().getCategoria().getNombre():""%>" readonly/>
                            </div>  
                        </div>        
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label for="tipo">Tipo: </label>
                                <input class="form-control input-sm" id="tipo" name="tipo" type="text" value="<%=inventarioalmacen.getProducto()!=null?new tools.Tipo().getNombre(inventarioalmacen.getProducto().getTipo()):""%>" readonly/>
                            </div>      
                        </div>
                        <div class="col-lg-2">
                            <div class="form-group">
                                <label for="cantidad">Cantidad: </label>
                                <input class="form-control input-sm" id="cantidad" name="cantidad" type="number" min="0" step="1" value="<%=inventarioalmacen.getCantidad()%>" <%=inventarioalmacen.getIdinventarioalmacen()!=0?inventarioalmacen.getProducto().getTipo()!=0?"disabled=\"true\"":"":""%> autocomplete="off" required/>
                            </div>      
                        </div>
                        <div class="col-lg-2">
                            <div class="form-group">
                                <label for="minimo">Minimo: </label>
                                <input class="form-control input-sm" id="minimo" name="minimo" type="number" min="0" step="1" value="<%=inventarioalmacen.getMinimo()%>" <%=inventarioalmacen.getIdinventarioalmacen()!=0?inventarioalmacen.getProducto().getTipo()!=0?"disabled=\"true\"":"":""%> autocomplete="off" required/>
                            </div>      
                        </div>
                        <div class="col-lg-2">
                            <div class="form-group">
                                <label for="maximo">Maximo: </label>
                                <input class="form-control input-sm" id="maximo" name="maximo" type="number" min="0" step="1" value="<%=inventarioalmacen.getMaximo()%>" <%=inventarioalmacen.getIdinventarioalmacen()!=0?inventarioalmacen.getProducto().getTipo()!=0?"disabled=\"true\"":"":""%> autocomplete="off" required/>
                            </div>      
                        </div>
                    </div>
                </form>                
            </div>
            <div class="panel-footer">
                <div class="row">
                    <div class="col-lg-12">
                        <button class="btn btn-primary btn-sm" type="button" onclick="javascript:save('Inventario/almacen/inventario.jsp','Inventario/almacen/inventarioListado.jsp?idalmacen=<%=inventarioalmacen.getAlmacen().getIdalmacen()%>','form1','page-wrapper');"><i class="fa fa-save fa-fw"></i> Guardar</button>
                        <%if(inventarioalmacen.getIdinventarioalmacen()!=0){%>
                            <button class="btn btn-danger btn-sm" type="button" onclick="javascript:erase('Inventario/almacen/inventario.jsp','Inventario/almacen/inventarioListado.jsp?idalmacen=<%=inventarioalmacen.getAlmacen().getIdalmacen()%>','form1','page-wrapper');"><i class="fa fa-eraser fa-fw"></i> Borrar</button>
                        <%}%>
                        <button class="btn btn-default btn-sm" type="button" onclick="javascript:go2to('Inventario/almacen/inventarioListado.jsp?idalmacen=<%=inventarioalmacen.getAlmacen().getIdalmacen()%>','page-wrapper');"><i class="fa fa-backward fa-fw"></i> Volver</button>                 
                    </div>
                </div>
            </div>
            <!-- /.panel-footer -->
        </div>
        <!-- /.panel -->
    </div>
</div>
<div class="modal fade" id="myModalSearch" tabindex="-1" role="dialog" aria-labelledby="myModalLabelSearch" aria-hidden="true">
    <div class="modal-dialog modal-xlg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Seleccionar Producto</h4>
            </div>
            <div class="modal-body">
                <object data="<%=request.getContextPath()%>/Inventario/buscarProducto.jsp?idalmacen=<%=inventarioalmacen.getAlmacen().getIdalmacen()%>" width="100%" height="500">
                    <embed src="<%=request.getContextPath()%>/Inventario/buscarProducto.jsp?idalmacen=<%=inventarioalmacen.getAlmacen().getIdalmacen()%>" width="100%" height="500"></embed>
                </object>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<script type="text/javascript">
            
    window.closeModal = function(){
        $('#myModalSearch').modal('hide');
    };

</script>
                    
<%
facade.close();
afacade.close();
pfacade.close();
%>