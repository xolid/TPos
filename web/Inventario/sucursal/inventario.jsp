<%-- 
    Document   : inventariosucursal
    Created on : Jan 9, 2016, 7:42:35 PM
    Author     : czara
--%>

<jsp:useBean id="inventariosucursal" class="beans.Inventariosucursal" scope="page"/>
<jsp:setProperty name="inventariosucursal" property="idinventariosucursal" />
<jsp:setProperty name="inventariosucursal" property="cantidad" />
<jsp:setProperty name="inventariosucursal" property="minimo" />
<jsp:setProperty name="inventariosucursal" property="maximo" />
<jsp:useBean id="facade" scope="page" class="facades.InventariosucursalFacade"/>
<jsp:useBean id="afacade" scope="page" class="facades.SucursalFacade"/>
<jsp:useBean id="pfacade" scope="page" class="facades.ProductoFacade"/>

<%if(request.getParameter("accion")!=null){
    if(!String.valueOf(request.getParameter("sucursal_idsucursal")).equals("")){inventariosucursal.setSucursal(afacade.getSucursalByID(Integer.parseInt(String.valueOf(request.getParameter("sucursal_idsucursal")))));}
    if(!String.valueOf(request.getParameter("producto_idproducto")).equals("")){inventariosucursal.setProducto(pfacade.getProductoByID(Integer.parseInt(String.valueOf(request.getParameter("producto_idproducto")))));}
    if(request.getParameter("accion").equals("1")){
        if(inventariosucursal.getIdinventariosucursal()==0){
            facade.saveInventariosucursal(inventariosucursal);
        }else{
            facade.updateInventariosucursal(inventariosucursal);
        }
    }
    if(request.getParameter("accion").equals("2")){
        facade.deleteInventariosucursal(inventariosucursal);        
    }
}
if(request.getParameter("id")!=null){
    int idinventariosucursal=Integer.parseInt((String)request.getParameter("id"));
    inventariosucursal=facade.getInventariosucursalByID(idinventariosucursal);
}else{
    inventariosucursal.setIdinventariosucursal(0);
    if(request.getParameter("idsucursal")!=null){
        inventariosucursal.setSucursal(afacade.getSucursalByID(Integer.parseInt(String.valueOf(request.getParameter("idsucursal")))));
    }
    inventariosucursal.setProducto(null);
    inventariosucursal.setCantidad(0);
    inventariosucursal.setMinimo(0);
    inventariosucursal.setMaximo(0);
}%>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Modulo de Inventario - Ajuste de Inventario de Sucursal</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                Definicion del Producto Inventariado en Sucursal [<%=inventariosucursal.getSucursal().getNombre()%>]
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <form inventariosucursale="form" id="form1" name="form1" action="inventario.jsp" method="post">
                    <input type="hidden" id="idinventariosucursal" name="idinventariosucursal" value="<%=inventariosucursal.getIdinventariosucursal()%>"/>
                    <input type="hidden" id="sucursal_idsucursal" name="sucursal_idsucursal" value="<%=inventariosucursal.getSucursal().getIdsucursal()%>"/>
                    <div class="row">
                        <div class="col-lg-1">
                            <div class="form-group">
                                <label>&nbsp;</label>
                                <button type="button" class="btn btn-info btn-block btn-sm" data-toggle="modal" data-target="#myModalSearch"><i class="fa fa-arrow-circle-right fa-fw"></i></button>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label for="producto_idproducto">Producto: </label>
                                <select id="producto_idproducto" name="producto_idproducto" class="form-control input-sm">
                                    <%if(inventariosucursal.getProducto()!=null){%>
                                        <option value="<%=inventariosucursal.getProducto().getIdproducto()%>"><%=inventariosucursal.getProducto().getNombre()%></option>
                                    <%}%>
                                </select>
                            </div> 
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label for="codigo">Codigo: </label>
                                <input class="form-control input-sm" id="codigo" name="codigo" type="number" value="<%=inventariosucursal.getProducto()!=null?inventariosucursal.getProducto().getCodigo():""%>" readonly/>
                            </div>      
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label for="clave">Clave: </label>
                                <input class="form-control input-sm" id="clave" name="clave" type="text" value="<%=inventariosucursal.getProducto()!=null?inventariosucursal.getProducto().getClave():""%>" readonly/>
                            </div>  
                        </div>
                        <div class="col-lg-2">
                            <div class="form-group">
                                <label for="unidad">Unidad: </label>
                                <input class="form-control input-sm" id="unidad" name="unidad" type="text" value="<%=inventariosucursal.getProducto()!=null?inventariosucursal.getProducto().getUnidad():""%>" readonly/>
                            </div>  
                        </div>
                    </div>
                    <div class="row">        
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label for="categoria">Categoria: </label>
                                <input class="form-control input-sm" id="categoria" name="categoria" type="text" value="<%=inventariosucursal.getProducto()!=null?inventariosucursal.getProducto().getCategoria().getNombre():""%>" readonly/>
                            </div>  
                        </div>        
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label for="tipo">Tipo: </label>
                                <input class="form-control input-sm" id="tipo" name="tipo" type="text" value="<%=inventariosucursal.getProducto()!=null?new tools.Tipo().getNombre(inventariosucursal.getProducto().getTipo()):""%>" readonly/>
                            </div>      
                        </div>
                        <div class="col-lg-2">
                            <div class="form-group">
                                <label for="cantidad">Cantidad: </label>
                                <input class="form-control input-sm" id="cantidad" name="cantidad" type="number" min="0" step="1" value="<%=inventariosucursal.getCantidad()%>" <%=inventariosucursal.getIdinventariosucursal()!=0?inventariosucursal.getProducto().getTipo()!=0?"disabled=\"true\"":"":""%> autocomplete="off" required/>
                            </div>      
                        </div>
                        <div class="col-lg-2">
                            <div class="form-group">
                                <label for="minimo">Minimo: </label>
                                <input class="form-control input-sm" id="minimo" name="minimo" type="number" min="0" step="1" value="<%=inventariosucursal.getMinimo()%>" <%=inventariosucursal.getIdinventariosucursal()!=0?inventariosucursal.getProducto().getTipo()!=0?"disabled=\"true\"":"":""%> autocomplete="off" required/>
                            </div>      
                        </div>
                        <div class="col-lg-2">
                            <div class="form-group">
                                <label for="maximo">Maximo: </label>
                                <input class="form-control input-sm" id="maximo" name="maximo" type="number" min="0" step="1" value="<%=inventariosucursal.getMaximo()%>" <%=inventariosucursal.getIdinventariosucursal()!=0?inventariosucursal.getProducto().getTipo()!=0?"disabled=\"true\"":"":""%> autocomplete="off" required/>
                            </div>      
                        </div>
                    </div>
                </form>                
            </div>
            <div class="panel-footer">
                <div class="row">
                    <div class="col-lg-12">
                        <button class="btn btn-primary btn-sm" type="button" onclick="javascript:save('Inventario/sucursal/inventario.jsp','Inventario/sucursal/inventarioListado.jsp?idsucursal=<%=inventariosucursal.getSucursal().getIdsucursal()%>','form1','page-wrapper');"><i class="fa fa-save fa-fw"></i> Guardar</button>
                        <%if(inventariosucursal.getIdinventariosucursal()!=0){%>
                            <button class="btn btn-danger btn-sm" type="button" onclick="javascript:erase('Inventario/sucursal/inventario.jsp','Inventario/sucursal/inventarioListado.jsp?idsucursal=<%=inventariosucursal.getSucursal().getIdsucursal()%>','form1','page-wrapper');"><i class="fa fa-eraser fa-fw"></i> Borrar</button>
                        <%}%>
                        <button class="btn btn-default btn-sm" type="button" onclick="javascript:go2to('Inventario/sucursal/inventarioListado.jsp?idsucursal=<%=inventariosucursal.getSucursal().getIdsucursal()%>','page-wrapper');"><i class="fa fa-backward fa-fw"></i> Volver</button>                 
                    </div>
                </div>
            </div>
            <!-- /.panel-footer -->
        </div>
        <!-- /.panel -->
    </div>
</div>
<div class="modal fade" id="myModalSearch" tabindex="-1" role="dialog" aria-labelledby="myModalLabelSearch" aria-hidden="true">
    <div class="modal-dialog modal-xlg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Seleccionar Producto</h4>
            </div>
            <div class="modal-body">
                <object data="<%=request.getContextPath()%>/Inventario/buscarProducto.jsp?idsucursal=<%=inventariosucursal.getSucursal().getIdsucursal()%>" width="100%" height="500">
                    <embed src="<%=request.getContextPath()%>/Inventario/buscarProducto.jsp?idsucursal=<%=inventariosucursal.getSucursal().getIdsucursal()%>" width="100%" height="500"></embed>
                </object>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<script type="text/javascript">
            
    window.closeModal = function(){
        $('#myModalSearch').modal('hide');
    };

</script>
                    
<%
facade.close();
afacade.close();
pfacade.close();
%>