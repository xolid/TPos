<%-- 
    Document   : subir
    Created on : 29-may-2018, 1:18:22
    Author     : solid
--%>

<jsp:useBean id="pfacade" scope="page" class="facades.ProveedorFacade"/>
 
<%
String nombre = "";
if(request.getParameter("nombre")!=null){
    nombre = java.net.URLDecoder.decode(String.valueOf(request.getParameter("nombre")), "UTF-8");
}
String telefono = "";
if(request.getParameter("telefono")!=null){
    telefono = java.net.URLDecoder.decode(String.valueOf(request.getParameter("telefono")), "UTF-8");
}
String razon = "";
if(request.getParameter("razon")!=null){
    razon = java.net.URLDecoder.decode(String.valueOf(request.getParameter("razon")), "UTF-8");
}
String rfc = "";
if(request.getParameter("rfc")!=null){
    rfc = java.net.URLDecoder.decode(String.valueOf(request.getParameter("rfc")), "UTF-8");
}
String direccion = "";
if(request.getParameter("direccion")!=null){
    direccion = java.net.URLDecoder.decode(String.valueOf(request.getParameter("direccion")), "UTF-8");
}

beans.Proveedor proveedor = new beans.Proveedor();
proveedor.setIdproveedor(0);
proveedor.setNombre(nombre);
proveedor.setTelefono(telefono);
proveedor.setRazon(razon);
proveedor.setRfc(rfc);
proveedor.setDireccion(direccion);
pfacade.saveProveedor(proveedor);
pfacade.close();
%>