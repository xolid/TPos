<%-- 
    Document   : proveedor
    Created on : Jan 9, 2016, 7:42:35 PM
    Author     : czara
--%>

<jsp:useBean id="proveedor" class="beans.Proveedor" scope="page"/>
<jsp:setProperty name="proveedor" property="*" />
<jsp:useBean id="facade" scope="page" class="facades.ProveedorFacade"/>

<%if(request.getParameter("accion")!=null){
    if(request.getParameter("accion").equals("1")){
        if(proveedor.getIdproveedor()==0){
            facade.saveProveedor(proveedor);
        }else{
            facade.updateProveedor(proveedor);
        }
    }
    if(request.getParameter("accion").equals("2")){
        facade.deleteProveedor(proveedor);        
    }
}
if(request.getParameter("id")!=null){
    proveedor=facade.getProveedorByID(Integer.parseInt((String)request.getParameter("id")));
}else{
    proveedor.setIdproveedor(0);
    proveedor.setNombre("");
    proveedor.setRazon("");
    proveedor.setRfc("");
    proveedor.setDireccion("");
    proveedor.setTelefono("");
}%>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Catalogos Principales - Proveedor</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                Definicion del Proveedor
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <form proveedore="form" id="form1" name="form1" action="proveedor.jsp" method="post">
                    <input type="hidden" id="idproveedor" name="idproveedor" value="<%=proveedor.getIdproveedor()%>"/>
                    <div class="row">
                        <div class="col-lg-9">
                            <div class="form-group">
                                <label for="nombre">Nombre: </label>
                                <input class="form-control input-sm" id="nombre" name="nombre" type="text" maxlength="45" value="<%=proveedor.getNombre()%>" autocomplete="off" required autofocus/>
                            </div>      
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label for="telefono">Telefono: </label>
                                <input class="form-control input-sm" id="telefono" name="telefono" type="number" min="0" maxlength="10" value="<%=proveedor.getTelefono()%>" autocomplete="off" required/>
                            </div>  
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-9">
                            <div class="form-group">
                                <label for="razon">Razon Social: </label>
                                <input class="form-control input-sm" id="razon" name="razon" type="text" maxlength="80" value="<%=proveedor.getRazon()%>" autocomplete="off" required/>
                            </div>      
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label for="rfc">R.F.C.: </label>
                                <input class="form-control input-sm" id="rfc" name="rfc" type="text" maxlength="80" value="<%=proveedor.getRfc()%>" autocomplete="off" required/>
                            </div>      
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="direccion">Direccion: </label>
                                <input class="form-control input-sm" id="direccion" name="direccion" type="text" maxlength="80" value="<%=proveedor.getDireccion()%>" autocomplete="off" required/>
                            </div>      
                        </div>
                    </div>
                </form>                
            </div>
            <div class="panel-footer">
                <div class="row">
                    <div class="col-lg-12">
                        <button class="btn btn-primary btn-sm" type="button" onclick="javascript:save('Proveedor/proveedor.jsp','Proveedor/proveedorListado.jsp','form1','page-wrapper');"><i class="fa fa-save fa-fw"></i> Guardar</button>
                        <%if(proveedor.getIdproveedor()!=0){%>
                            <button class="btn btn-danger btn-sm" type="button" onclick="javascript:erase('Proveedor/proveedor.jsp','Proveedor/proveedorListado.jsp','form1','page-wrapper');"><i class="fa fa-eraser fa-fw"></i> Borrar</button>
                        <%}%>
                        <button class="btn btn-default btn-sm" type="button" onclick="javascript:go2to('Proveedor/proveedorListado.jsp','page-wrapper');"><i class="fa fa-backward fa-fw"></i> Volver</button>                 
                    </div>
                </div>
            </div>
            <!-- /.panel-footer -->
        </div>
        <!-- /.panel -->
    </div>
</div>
<!-- /.row -->

<%
facade.close();
%>