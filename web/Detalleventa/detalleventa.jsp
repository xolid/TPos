<%-- 
    Document   : detalleventa
    Created on : 7/05/2018, 02:19:56 PM
    Author     : czara
--%>

<jsp:useBean id="pfacade" scope="page" class="facades.ProductoFacade"/>

<%    
java.util.Locale mexico = new java.util.Locale("es", "MX");
java.text.NumberFormat formatoImporte = java.text.NumberFormat.getCurrencyInstance(mexico);

int consecutivo = 0;
int total_articulos = 0;
java.math.BigDecimal total_venta = new java.math.BigDecimal(0.0);
java.util.List<tools.Detalle> detalles = (java.util.List<tools.Detalle>)session.getAttribute("detalles");
double impuesto = Double.valueOf(String.valueOf(session.getAttribute("impuesto")));
%>

<form class="form" id="form2" name="form2" action="venta.jsp" method="post">
    
    <div class="table-responsive">
        <table class="table table-striped table-bordered table-condensed table-hover" id="dataTablesAgregar">
            <thead>
                <th class="col-lg-1 text-center">&nbsp;</th>
                <th class="col-lg-3">Producto</th>
                <th class="col-lg-2">Marca</th>
                <th class="col-lg-1">Unidad</th>
                <th class="col-lg-2">Categoria</th>
                <th class="col-lg-1 text-center">Cantidad</th>
                <th class="col-lg-1 text-center">Precio</th>
                <th class="col-lg-1">&nbsp;</th>
            </thead>
            <tbody>
                <tr class="success">
                    <td class="text-center"><button type="button" class="btn btn-info btn-block btn-sm" data-toggle="modal" data-target="#myModalSearch"><i class="fa fa-arrow-circle-right fa-fw"></i></button></td>
                    <td><select class="form-control input-sm" id="producto_idproducto" name="producto_idproducto" disabled><option value="0">Producto</option></select></td>
                    <td><input class="form-control input-sm" id="marca" name="marca" type="text" placeholder="Marca" readonly/></td>
                    <td><input class="form-control input-sm" id="unidad" name="unidad" type="text" placeholder="Unidad" readonly/></td>
                    <td><input class="form-control input-sm" id="categoria" name="categoria" type="text" placeholder="Categoria" readonly/></td>
                    <td><input class="form-control input-sm text-center" id="cantidad" name="cantidad" type="number" min="0" step="1" placeholder="0"/></td>
                    <td><input class="form-control input-sm text-center" id="precio" name="precio" type="number" min="0" step="0.01" placeholder="$0.00" readonly/></td>
                    <td class="text-center"><button class="btn btn-success btn-block btn-sm" type="button" onclick="javascript:agregar();"><i class="fa fa-plus-circle fa-fw"></i></button></td>
                </tr>
            </tbody>
        </table>
    </div>

    <div class="table-responsive">
        <table class="table table-striped table-bordered table-condensed table-hover" id="dataTablesDetalleVenta">
            <thead>
                <th class="col-lg-1 text-center">#</th>
                <th class="col-lg-2">Producto</th>
                <th class="col-lg-1">Marca</th>
                <th class="col-lg-1">Unidad</th>
                <th class="col-lg-1">Categoria</th>
                <th class="col-lg-1 text-center">Cantidad</th>
                <th class="col-lg-1 text-center">Precio</th>
                <th class="col-lg-1 text-center">Subtotal</th>
                <th class="col-lg-1 text-center">I.V.A. <%=impuesto%>%</th>
                <th class="col-lg-1 text-center">Total</th>
                <th class="col-lg-1">&nbsp;</th>
            </thead>
            <tbody>
                <%if(detalles != null){%>
                    <%for(tools.Detalle detalle:detalles){
                        beans.Producto producto = pfacade.getProductoByID(detalle.getIdproducto());
                        double subtotal = detalle.getPrecio().doubleValue()*detalle.getCantidad();
                        double iva = 0.0;
                        if(producto.getImpuesto()==1){
                            iva = (impuesto*subtotal)/100;
                        }
                        double total = subtotal+iva;
                        %>
                        <tr>
                            <td class="text-center"><%=(consecutivo+1)%></td>
                            <td><%=producto.getNombre()%></td>
                            <td><%=producto.getMarca()%></td>
                            <td><%=producto.getUnidad()%></td>
                            <td><%=producto.getCategoria().getNombre()%></td>
                            <td class="text-center"><%=detalle.getCantidad()%></td>
                            <td class="text-center"><%=formatoImporte.format(detalle.getPrecio())%></td>
                            <td class="text-center"><%=formatoImporte.format(subtotal)%></td>
                            <td class="text-center"><%=formatoImporte.format(iva)%></td>
                            <td class="text-center"><%=formatoImporte.format(total)%></td>
                            <td class="text-center"><button class="btn btn-warning btn-block btn-xs" type="button" onclick="javascript:borrar(<%=(consecutivo)%>);"><i class="fa fa-minus-circle fa-fw"></i></button></td>
                        </tr>
                        <%consecutivo++;
                        total_articulos += detalle.getCantidad();
                        total_venta = total_venta.add(new java.math.BigDecimal(total));
                    }
                }%>
            </tbody>
            <tfoot>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td class="text-center"><b><%=total_articulos%></b></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td class="text-center"><b><%=formatoImporte.format(total_venta)%></b></td>
                <td class="text-center"><button class="btn btn-danger btn-block btn-xs" type="button" onclick="javascript:borrar(-1);"><i class="fa fa-minus-square fa-fw"></i></button></td>
            </tfoot>
        </table>
    </div>
</form>
<div class="modal fade" id="myModalSearch" tabindex="-1" role="dialog" aria-labelledby="myModalLabelSearch" aria-hidden="true">
    <div class="modal-dialog modal-xlg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Seleccionar Participante</h4>
            </div>
            <div class="modal-body">
                <object data="<%=request.getContextPath()%>/Detalleventa/buscarProducto.jsp" width="100%" height="370">
                    <embed src="<%=request.getContextPath()%>/Detalleventa/buscarProducto.jsp" width="100%" height="370"></embed>
                </object>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<script>
    function borrar(indice){
        var params = "indice="+indice;
        processDIV("/TPos/Detalleventa/remover.jsp",params,"/TPos/Detalleventa/detalleventa.jsp","","detalle");
    }
    
    function agregar(){
        var str = "";        
        if(document.getElementById("producto_idproducto").selectedIndex === -1){str += "* Debe seleccionar un producto.<br/>";}
        if(document.getElementById("precio").value === ""){str += "* Debe ingresar un precio de venta.<br/>";}
        if(document.getElementById("cantidad").value === ""){str += "* Debe ingresar una cantidad de articulos.<br/>";}
        if(document.getElementById("cantidad").value === "0"){str += "* Debe ingresar una cantidad de articulos mayor que 0.<br/>";}
        if(str === ""){
            var idproducto = document.getElementById("producto_idproducto").options[document.getElementById("producto_idproducto").selectedIndex].value;
            var precio = document.getElementById('precio').value;
            var cantidad = document.getElementById('cantidad').value;
            var params = "idproducto="+idproducto+"&precio="+precio+"&cantidad="+cantidad;
            processDIV("/TPos/Detalleventa/agregar.jsp",params,"/TPos/Detalleventa/detalleventa.jsp","","detalle");
        }else{
            BootstrapDialog.alert({
                title: "Mensaje del Sistema",
                message: "<strong>Se encontraron los siguientes errores:</strong><br/><br/>" + str,
                type: 'type-danger',
                closable: false,
                btnOKClass: 'btn-danger'
            });
        }
    }
            
    window.closeModal = function(){
        $('#myModalSearch').modal('hide');
        document.getElementById('cantidad').focus();
    };

</script>
            
<%
pfacade.close();
%>