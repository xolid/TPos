<%-- 
    Document   : usuarioListado
    Created on : Jan 9, 2016, 4:50:18 PM
    Author     : czara
--%>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Catalogos Primarios - Almacenes</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                Listado de Almacenes Disponibles
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-condensed table-hover" id="dataTablesAlmacen">
                        <thead>
                            <th class="col-lg-12">[B] Nombre</th>
                        </thead>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
            <div class="panel-footer">
                <div class="row">
                    <div class="col-lg-12">
                        <button class="btn btn-primary btn-sm" type="button" onclick="javascript:go2to('Almacen/almacen.jsp','page-wrapper');"><i class="fa fa-asterisk fa-fw"></i> Nuevo</button>
                    </div>
                </div>
            </div>
            <!-- /.panel-footer -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<script>
    $(document).ready(function() {
        $('#dataTablesAlmacen').dataTable({
            "retrieve": true,
            "processing": true,  
            "serverSide": true,
            "ajax": "Almacen/processAlmacen.jsp",
            "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
            }
        });
    });
</script>