<%-- 
    Document   : almacen
    Created on : Jan 9, 2016, 7:42:35 PM
    Author     : czara
--%>

<jsp:useBean id="almacen" class="beans.Almacen" scope="page"/>
<jsp:setProperty name="almacen" property="idalmacen" />
<jsp:setProperty name="almacen" property="nombre" />
<jsp:useBean id="facade" scope="page" class="facades.AlmacenFacade"/>

<%if(request.getParameter("accion")!=null){
    if(request.getParameter("accion").equals("1")){
        if(almacen.getIdalmacen()==0){
            facade.saveAlmacen(almacen);
        }else{
            facade.updateAlmacen(almacen);
        }
    }
    if(request.getParameter("accion").equals("2")){
        facade.deleteAlmacen(almacen);        
    }
}
if(request.getParameter("id")!=null){
    almacen=facade.getAlmacenByID(Integer.parseInt((String)request.getParameter("id")));
}else{
    almacen.setIdalmacen(0);
    almacen.setNombre("");
}%>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Catalogos Primarios - Almacen</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                Definicion del Almacen
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <form almacene="form" id="form1" name="form1" action="almacen.jsp" method="post">
                    <input type="hidden" id="idalmacen" name="idalmacen" value="<%=almacen.getIdalmacen()%>"/>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="nombre">Nombre: </label>
                                <input class="form-control input-sm" id="nombre" name="nombre" type="text" maxlength="45" value="<%=almacen.getNombre()%>" autocomplete="off" required autofocus/>
                            </div>      
                        </div>
                    </div>
                </form>                
            </div>
            <div class="panel-footer">
                <div class="row">
                    <div class="col-lg-12">
                        <button class="btn btn-primary btn-sm" type="button" onclick="javascript:save('Almacen/almacen.jsp','Almacen/almacenListado.jsp','form1','page-wrapper');"><i class="fa fa-save fa-fw"></i> Guardar</button>
                        <%if(almacen.getIdalmacen()!=0){%>
                            <button class="btn btn-danger btn-sm" type="button" onclick="javascript:erase('Almacen/almacen.jsp','Almacen/almacenListado.jsp','form1','page-wrapper');"><i class="fa fa-eraser fa-fw"></i> Borrar</button>
                        <%}%>
                        <button class="btn btn-default btn-sm" type="button" onclick="javascript:go2to('Almacen/almacenListado.jsp','page-wrapper');"><i class="fa fa-backward fa-fw"></i> Volver</button>                 
                    </div>
                </div>
            </div>
            <!-- /.panel-footer -->
        </div>
        <!-- /.panel -->
    </div>
</div>
<!-- /.row -->

<%
facade.close();
%>