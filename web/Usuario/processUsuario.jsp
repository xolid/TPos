<%-- 
    Document   : process2
    Created on : Feb 7, 2016, 7:18:03 PM
    Author     : czara
--%>
 
<%@page import="java.util.*"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page import="org.json.simple.JSONArray"%>

<jsp:useBean id="facade" scope="page" class="facades.UsuarioFacade"/>

<%  
String[] cols = { "nombre","email","fingreso","fegreso","rol" };
JSONObject result = new JSONObject();
JSONArray array = new JSONArray();
int amount = 10;
int start = 0;
int echo = 0;
int col = 0;
String dir = "asc";
String sStart = request.getParameter("start");
String sAmount = request.getParameter("length");
String sEcho = request.getParameter("draw");
String sCol = request.getParameter("order[0][column]");
String sdir = request.getParameter("order[0][dir]");
String buscar = request.getParameter("search[value]");
if (sStart != null) {
    start = Integer.parseInt(sStart);
    if (start < 0)
        start = 0;
}
if (sAmount != null) {
    amount = Integer.parseInt(sAmount);
    if (amount < 10 || amount > 100)
        amount = 10;
}
if (sEcho != null) {
    echo = Integer.parseInt(sEcho);
}
if (sCol != null) {
    col = Integer.parseInt(sCol);
    if (col < 0 || col > 4)
        col = 0;
}
if (sdir != null) {
    if (!sdir.equals("asc"))
        dir = "desc";
}
String colName = cols[col];
long total = facade.getTotal();
List<beans.Usuario> usuarios = facade.getUsuarioForProcess(buscar, colName, dir, start, amount);
for(beans.Usuario p:usuarios){
    JSONArray ja = new JSONArray();
    ja.add("<a href=\"#\" onclick=\"javascript:go2to('Usuario/usuario.jsp?id="+p.getIdusuario()+"','page-wrapper')\">"+p.getNombre()+"</a>");
    ja.add(p.getEmail());
    ja.add(new java.text.SimpleDateFormat("yyyy-MM-dd").format(p.getFingreso()));
    ja.add(p.getFegreso()==null?"":new java.text.SimpleDateFormat("yyyy-MM-dd").format(p.getFegreso()));
    ja.add(p.getRol().getNombre());
    array.add(ja);
}
long totalAfterFilter = facade.getUsuarioForProcessCount(buscar);
result.put("draw", sEcho);
result.put("recordsTotal", total);
result.put("recordsFiltered", totalAfterFilter);
result.put("data", array);
response.setContentType("application/json");
response.setHeader("Cache-Control", "no-store");
out.print(result);

facade.close();    
%>

