<%-- 
    Document   : usuario
    Created on : Jan 9, 2016, 7:42:35 PM
    Author     : czara
--%>

<jsp:useBean id="usuario" class="beans.Usuario" scope="page"/>
<jsp:setProperty name="usuario" property="idusuario" />
<jsp:setProperty name="usuario" property="nombre" />
<jsp:setProperty name="usuario" property="email" />
<jsp:setProperty name="usuario" property="pass" />
<jsp:useBean id="facade" scope="page" class="facades.UsuarioFacade"/>
<jsp:useBean id="rfacade" scope="page" class="facades.RolFacade"/>
<jsp:useBean id="sfacade" scope="page" class="facades.SucursalFacade"/>

<%if(request.getParameter("accion")!=null){
    if(!String.valueOf(request.getParameter("fingreso")).equals("")){usuario.setFingreso(new java.text.SimpleDateFormat("yyyy-MM-dd").parse(String.valueOf(request.getParameter("fingreso"))));}
    if(!String.valueOf(request.getParameter("fegreso")).equals("")){usuario.setFegreso(new java.text.SimpleDateFormat("yyyy-MM-dd").parse(String.valueOf(request.getParameter("fegreso"))));}
    if(!String.valueOf(request.getParameter("rol_idrol")).equals("")){usuario.setRol(rfacade.getRolByID(Integer.parseInt(String.valueOf(request.getParameter("rol_idrol")))));}
    if(!String.valueOf(request.getParameter("sucursal_idsucursal")).equals("")){
        if(!String.valueOf(request.getParameter("sucursal_idsucursal")).equals("0")){
            usuario.setSucursal(sfacade.getSucursalByID(Integer.parseInt(String.valueOf(request.getParameter("sucursal_idsucursal")))));
        }
    }
    if(request.getParameter("accion").equals("1")){
        if(usuario.getIdusuario()==0){
            if(!String.valueOf(request.getParameter("pass")).equals("")){usuario.setPass(tools.MD5.getMD5(usuario.getPass()));}
            facade.saveUsuario(usuario);
        }else{
            facade.updateUsuario(usuario);
        }
    }
    if(request.getParameter("accion").equals("2")){
        facade.deleteUsuario(usuario);        
    }
}
if(request.getParameter("id")!=null){
    usuario=facade.getUsuarioByID(Integer.parseInt((String)request.getParameter("id")));
}else{
    usuario.setIdusuario(0);
    usuario.setNombre("");
    usuario.setEmail("");
    usuario.setPass("");
    usuario.setFingreso(new java.util.Date());
    usuario.setFegreso(null);
    usuario.setRol(new beans.Rol());
    usuario.setSucursal(new beans.Sucursal());
}%>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Catalogos del Sistema - Usuario</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                Definicion del Usuario
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <form role="form" id="form1" name="form1" action="usuario.jsp" method="post">
                    <input type="hidden" id="idusuario" name="idusuario" value="<%=usuario.getIdusuario()%>"/>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="nombre">Nombre: </label>
                                <input class="form-control input-sm" id="nombre" name="nombre" type="text" maxlength="75" value="<%=usuario.getNombre()%>" autocomplete="off" required autofocus/>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label for="email">EMail: </label>
                                <input class="form-control input-sm" id="email" name="email" type="email" maxlength="45" value="<%=usuario.getEmail()%>" autocomplete="off" required/>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label for="password">Password: </label>
                                <input class="form-control input-sm" id="pass" name="pass" type="password" maxlength="45" value="<%=usuario.getPass()%>" autocomplete="off" required <%=usuario.getIdusuario()!=0?"DISABLED":""%>/>
                            </div>        
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label>Fecha Ingreso</label>
                                <input class="form-control input-sm" id="fingreso" name="fingreso" type="date" value="<%=new java.text.SimpleDateFormat("yyyy-MM-dd").format(usuario.getFingreso())%>" required/>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label>Fecha Egreso</label>
                                <input class="form-control input-sm" id="fegreso" name="fegreso" type="date" value="<%=usuario.getFegreso()!=null?new java.text.SimpleDateFormat("yyyy-MM-dd").format(usuario.getFegreso()):""%>"/>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label for="rol_idrol">Rol</label>
                                <select id="rol_idrol" name="rol_idrol" class="form-control input-sm">
                                    <%for(beans.Rol rol:rfacade.getRolAll()){%>
                                        <option value="<%=rol.getIdrol()%>" <%=rol.getIdrol()==usuario.getRol().getIdrol()?"SELECTED":""%>><%=rol.getNombre()%></option>
                                    <%}%>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label for="sucursal_idsucursal">Sucursal</label>
                                <select id="sucursal_idsucursal" name="sucursal_idsucursal" class="form-control input-sm">
                                    <option value="0">USUARIO GLOBAL</option>
                                    <%for(beans.Sucursal sucursal:sfacade.getSucursalAll()){%>
                                        <option value="<%=sucursal.getIdsucursal()%>" <%=usuario.getSucursal()!=null?(sucursal.getIdsucursal()==usuario.getSucursal().getIdsucursal()?"SELECTED":""):""%>><%=sucursal.getNombre()%></option>
                                    <%}%>
                                </select>
                            </div>
                        </div>
                    </div>
                </form>                
                <!-- /.form -->
            </div>
            <!-- /.panel-body -->
            <div class="panel-footer">
                <div class="row">
                    <div class="col-lg-12">
                        <button class="btn btn-primary btn-sm" type="button" onclick="javascript:save('Usuario/usuario.jsp','Usuario/usuarioListado.jsp','form1','page-wrapper');"><i class="fa fa-save fa-fw"></i> Guardar</button>
                        <%if(usuario.getIdusuario()!=0){%>
                            <button class="btn btn-danger btn-sm" type="button" onclick="javascript:erase('Usuario/usuario.jsp','Usuario/usuarioListado.jsp','form1','page-wrapper');"><i class="fa fa-eraser fa-fw"></i> Borrar</button>
                        <%}%>
                        <button class="btn btn-default btn-sm" type="button" onclick="javascript:go2to('Usuario/usuarioListado.jsp','page-wrapper');"><i class="fa fa-backward fa-fw"></i> Volver</button>                 
                    </div>
                </div>
            </div>
            <!-- /.panel-footer -->
        </div>
        <!-- /.panel -->
    </div>
</div>
<!-- /.row -->

<%
facade.close();
rfacade.close();
sfacade.close();
%>