<%-- 
    Document   : cliente
    Created on : Jan 9, 2016, 7:42:35 PM
    Author     : czara
--%>

<jsp:useBean id="cliente" class="beans.Cliente" scope="page"/>
<jsp:setProperty name="cliente" property="*" />
<jsp:useBean id="facade" scope="page" class="facades.ClienteFacade"/>

<%if(request.getParameter("accion")!=null){
    if(request.getParameter("accion").equals("1")){
        if(cliente.getIdcliente()==0){
            facade.saveCliente(cliente);
        }else{
            facade.updateCliente(cliente);
        }
    }
    if(request.getParameter("accion").equals("2")){
        facade.deleteCliente(cliente);        
    }
}
if(request.getParameter("id")!=null){
    cliente=facade.getClienteByID(Integer.parseInt((String)request.getParameter("id")));
}else{
    cliente.setIdcliente(0);
    cliente.setNombre("");
    cliente.setRazon("");
    cliente.setRfc("");
    cliente.setDireccion("");
    cliente.setTelefono("");
}%>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Catalogos Principales - Cliente</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                Definicion del Cliente
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <form clientee="form" id="form1" name="form1" action="cliente.jsp" method="post">
                    <input type="hidden" id="idcliente" name="idcliente" value="<%=cliente.getIdcliente()%>"/>
                    <div class="row">
                        <div class="col-lg-9">
                            <div class="form-group">
                                <label for="nombre">Nombre: </label>
                                <input class="form-control input-sm" id="nombre" name="nombre" type="text" maxlength="45" value="<%=cliente.getNombre()%>" autocomplete="off" required autofocus/>
                            </div>      
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label for="telefono">Telefono: </label>
                                <input class="form-control input-sm" id="telefono" name="telefono" type="number" min="0" maxlength="10" value="<%=cliente.getTelefono()%>" autocomplete="off" required/>
                            </div>  
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-9">
                            <div class="form-group">
                                <label for="razon">Razon Social: </label>
                                <input class="form-control input-sm" id="razon" name="razon" type="text" maxlength="80" value="<%=cliente.getRazon()%>" autocomplete="off" required/>
                            </div>      
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label for="rfc">R.F.C.: </label>
                                <input class="form-control input-sm" id="rfc" name="rfc" type="text" maxlength="80" value="<%=cliente.getRfc()%>" autocomplete="off" required/>
                            </div>      
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="direccion">Direccion: </label>
                                <input class="form-control input-sm" id="direccion" name="direccion" type="text" maxlength="80" value="<%=cliente.getDireccion()%>" autocomplete="off" required/>
                            </div>      
                        </div>
                    </div>
                </form>                
            </div>
            <div class="panel-footer">
                <div class="row">
                    <div class="col-lg-12">
                        <button class="btn btn-primary btn-sm" type="button" onclick="javascript:save('Cliente/cliente.jsp','Cliente/clienteListado.jsp','form1','page-wrapper');"><i class="fa fa-save fa-fw"></i> Guardar</button>
                        <%if(cliente.getIdcliente()!=0){%>
                            <button class="btn btn-danger btn-sm" type="button" onclick="javascript:erase('Cliente/cliente.jsp','Cliente/clienteListado.jsp','form1','page-wrapper');"><i class="fa fa-eraser fa-fw"></i> Borrar</button>
                        <%}%>
                        <button class="btn btn-default btn-sm" type="button" onclick="javascript:go2to('Cliente/clienteListado.jsp','page-wrapper');"><i class="fa fa-backward fa-fw"></i> Volver</button>                 
                    </div>
                </div>
            </div>
            <!-- /.panel-footer -->
        </div>
        <!-- /.panel -->
    </div>
</div>
<!-- /.row -->

<%
facade.close();
%>