<%-- 
    Document   : sucursal
    Created on : Jan 9, 2016, 7:42:35 PM
    Author     : czara
--%>

<jsp:useBean id="sucursal" class="beans.Sucursal" scope="page"/>
<jsp:setProperty name="sucursal" property="idsucursal" />
<jsp:setProperty name="sucursal" property="nombre" />
<jsp:setProperty name="sucursal" property="direccion" />
<jsp:setProperty name="sucursal" property="telefono" />
<jsp:useBean id="facade" scope="page" class="facades.SucursalFacade"/>
<jsp:useBean id="ifacade" scope="page" class="facades.IvaFacade"/>

<%if(request.getParameter("accion")!=null){
    if(!String.valueOf(request.getParameter("iva_idiva")).equals("")){sucursal.setIva(ifacade.getIvaByID(Integer.parseInt(String.valueOf(request.getParameter("iva_idiva")))));}
    if(request.getParameter("accion").equals("1")){
        if(sucursal.getIdsucursal()==0){
            facade.saveSucursal(sucursal);
        }else{
            facade.updateSucursal(sucursal);
        }
    }
    if(request.getParameter("accion").equals("2")){
        facade.deleteSucursal(sucursal);        
    }
}
if(request.getParameter("id")!=null){
    sucursal=facade.getSucursalByID(Integer.parseInt((String)request.getParameter("id")));
}else{
    sucursal.setIdsucursal(0);
    sucursal.setNombre("");
    sucursal.setDireccion("");
    sucursal.setTelefono("");
    sucursal.setIva(new beans.Iva());
}%>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Catalogos del Sistema - Sucursal</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                Definicion de la Sucursal
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <form sucursale="form" id="form1" name="form1" action="sucursal.jsp" method="post">
                    <input type="hidden" id="idsucursal" name="idsucursal" value="<%=sucursal.getIdsucursal()%>"/>
                    <div class="row">
                        <div class="col-lg-9">
                            <div class="form-group">
                                <label for="nombre">Nombre: </label>
                                <input class="form-control input-sm" id="nombre" name="nombre" type="text" maxlength="45" value="<%=sucursal.getNombre()%>" autocomplete="off" required autofocus/>
                            </div>      
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label for="telefono">Telefono: </label>
                                <input class="form-control input-sm" id="telefono" name="telefono" type="number" min="0" maxlength="10" value="<%=sucursal.getTelefono()%>" autocomplete="off" required/>
                            </div>  
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-9">
                            <div class="form-group">
                                <label for="direccion">Direccion: </label>
                                <input class="form-control input-sm" id="direccion" name="direccion" type="text" maxlength="80" value="<%=sucursal.getDireccion()%>" autocomplete="off" required/>
                            </div>      
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label for="iva_idiva">I.V.A.: </label>
                                <select id="iva_idiva" name="iva_idiva" class="form-control input-sm">
                                    <%for(beans.Iva iva:ifacade.getIvaAll()){%>
                                        <option value="<%=iva.getIdiva()%>" <%=iva.getIdiva()==sucursal.getIva().getIdiva()?"SELECTED":""%>><%=iva.getNombre()%></option>
                                    <%}%>
                                </select>
                            </div>      
                        </div>
                    </div>
                </form>                
            </div>
            <div class="panel-footer">
                <div class="row">
                    <div class="col-lg-12">
                        <button class="btn btn-primary btn-sm" type="button" onclick="javascript:save('Sucursal/sucursal.jsp','Sucursal/sucursalListado.jsp','form1','page-wrapper');"><i class="fa fa-save fa-fw"></i> Guardar</button>
                        <%if(sucursal.getIdsucursal()!=0){%>
                            <button class="btn btn-danger btn-sm" type="button" onclick="javascript:erase('Sucursal/sucursal.jsp','Sucursal/sucursalListado.jsp','form1','page-wrapper');"><i class="fa fa-eraser fa-fw"></i> Borrar</button>
                        <%}%>
                        <button class="btn btn-default btn-sm" type="button" onclick="javascript:go2to('Sucursal/sucursalListado.jsp','page-wrapper');"><i class="fa fa-backward fa-fw"></i> Volver</button>                 
                    </div>
                </div>
            </div>
            <!-- /.panel-footer -->
        </div>
        <!-- /.panel -->
    </div>
</div>
<!-- /.row -->

<%
facade.close();
ifacade.close();
%>