<%-- 
    Document   : compra
    Created on : Jan 9, 2016, 7:42:35 PM
    Author     : czara
--%>
<%-- 
    Document   : compra
    Created on : Jan 9, 2016, 7:42:35 PM
    Author     : czara
--%>

<jsp:useBean id="compra" class="beans.Compra" scope="page"/>
<jsp:setProperty name="compra" property="idcompra" />
<jsp:setProperty name="compra" property="estado" />
<jsp:useBean id="facade" scope="page" class="facades.CompraFacade"/>
<jsp:useBean id="detfacade" scope="page" class="facades.DetallecompraFacade"/>


<%if(request.getParameter("id")!=null){
    int idcompra=Integer.parseInt((String)request.getParameter("id"));
    compra=facade.getCompraByID(idcompra);
}
java.util.Locale mexico = new java.util.Locale("es", "MX");
java.text.NumberFormat formatoImporte = java.text.NumberFormat.getCurrencyInstance(mexico);
%>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Compra - Definicion de la Compra</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                Definicion de la Compra
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <form comprae="form" id="form1" name="form1" action="compra.jsp" method="post">
                    <input type="hidden" id="idcompra" name="idcompra" value="<%=compra.getIdcompra()%>"/>
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label for="fcompra">Fecha: </label>
                                <input class="form-control input-sm" id="fcompra" name="fcompra" type="text" value="<%=new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(compra.getFcompra())%>" readonly/>
                            </div>      
                        </div>
                        <div class="col-lg-3 col-lg-offset-6">
                            <div class="form-group">
                                <label for="estado">Estado: </label>
                                <select id="estado" name="estado" class="form-control input-sm" disabled="true">
                                    <option value="<%=compra.getEstado()%>"><%=new tools.Estado().getNombre(compra.getEstado())%></option>
                                </select>
                            </div>  
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="proveedor_idproveedor">Proveedor: </label>
                                <select id="proveedor_idproveedor" name="proveedor_idproveedor" class="form-control input-sm" disabled="true">
                                    <option value="<%=compra.getProveedor()%>"><%=compra.getProveedor()%></option>
                                </select>
                            </div>      
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="usuario_idusuario">Usuario: </label>
                                <select id="usuario_idusuario" name="usuario_idusuario" class="form-control input-sm" disabled="true">
                                    <option value="<%=compra.getUsuario()%>"><%=compra.getUsuario()%></option>
                                </select>
                            </div>      
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="almacen_idalmacen">Almacen de Recepcion: </label>
                                <select id="almacen_idalmacen" name="almacen_idalmacen" class="form-control input-sm" disabled="true">
                                    <option value="<%=compra.getAlmacen().getIdalmacen()%>"><%=compra.getAlmacen().getNombre()%></option>
                                </select>
                            </div>      
                        </div>
                    </div>
                </form>                
            </div>
        </div>
        <!-- /.panel -->
    </div>
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                Detalles de la Compra
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body" id="detalle">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-condensed table-hover" id="dataTablesDetalleCompra">
                        <thead>
                            <th class="col-lg-1 text-center">#</th>
                            <th class="col-lg-2">Producto</th>
                            <th class="col-lg-1">Marca</th>
                            <th class="col-lg-1">Unidad</th>
                            <th class="col-lg-1">Categoria</th>
                            <th class="col-lg-1 text-center">Cantidad</th>
                            <th class="col-lg-1 text-center">Precio</th>
                            <th class="col-lg-1 text-center">Subtotal</th>
                            <th class="col-lg-1 text-center">I.V.A.</th>
                            <th class="col-lg-1 text-center">Total</th>
                        </thead>
                        <tbody>
                            <%int consecutivo = 0;
                            int total_articulos = 0;
                            java.math.BigDecimal total_compra = new java.math.BigDecimal(0.0);
                            for(beans.Detallecompra detallecompra:detfacade.getDetallecompraByCompra(compra.getIdcompra())){
                                double subtotal = detallecompra.getPrecio().doubleValue()*detallecompra.getCantidad();
                                double iva = 0.0;
                                if(detallecompra.getImpuesto()!=0.0){
                                    iva = (detallecompra.getImpuesto()*subtotal)/100;
                                }
                                double total = subtotal+iva;
                                %>
                                <tr>
                                    <td class="text-center"><%=(consecutivo+1)%></td>
                                    <td><%=detallecompra.getProducto()%></td>
                                    <td><%=detallecompra.getMarca()%></td>
                                    <td><%=detallecompra.getUnidad()%></td>
                                    <td><%=detallecompra.getCategoria()%></td>
                                    <td class="text-center"><%=detallecompra.getCantidad()%></td>
                                    <td class="text-center"><%=formatoImporte.format(detallecompra.getPrecio())%></td>
                                    <td class="text-center"><%=formatoImporte.format(subtotal)%></td>
                                    <td class="text-center"><%=formatoImporte.format(iva)%></td>
                                    <td class="text-center"><%=formatoImporte.format(total)%></td>
                                </tr>
                                <%consecutivo++;
                                total_articulos += detallecompra.getCantidad();
                                total_compra = total_compra.add(new java.math.BigDecimal(total));
                            }%>
                        </tbody>
                        <tfoot>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td class="text-center"><b><%=total_articulos%></b></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td class="text-center"><b><%=formatoImporte.format(total_compra)%></b></td>
                        </tfoot>
                    </table>
                </div>
            </div>
            <div class="panel-footer">
                <div class="row">
                    <div class="col-lg-12">
                        <%if(compra.getEstado()==1){%>
                            <button class="btn btn-primary btn-sm" type="button" onclick="javascript:actualizar(0);"><i class="fa fa-tasks fa-fw"></i> Aplicar</button>
                        <%}if(compra.getEstado()!=2){%>
                            <button class="btn btn-danger btn-sm" type="button" onclick="javascript:actualizar(1);"><i class="fa fa-eraser fa-fw"></i> Cancelar</button>
                        <%}%>
                        <button class="btn btn-default btn-sm" type="button" onclick="javascript:go2to('Compra/compraListado.jsp','page-wrapper');"><i class="fa fa-backward fa-fw"></i> Volver</button>                 
                    </div>
                </div>
            </div>
            <!-- /.panel-footer -->
        </div>
        <!-- /.panel -->
    </div>
</div>
<!-- /.row -->
<script lang="javascript">
    
    function actualizar(opcion){
        BootstrapDialog.confirm({
            title: "Mensaje del Sistema",
            message: "�Confirma desea "+(opcion===0?"APLICAR":"CANCELAR")+" la compra?",
            type: 'type-warning',
            closable: false,
            btnOKClass: 'btn-warning',
            callback: function(result) {
                if(result) {
                    var idcompra = document.getElementById('idcompra').value;
                    var params = "opcion="+opcion+"&idcompra="+idcompra;
                    var params2 = "id="+idcompra;
                    executeEXEC("/TPos/Compra/actualizar.jsp",params,"loadPanel('/TPos/Compra/compraMostrar.jsp','"+params2+"','page-wrapper')");
                }
            }
        });
    }
</script>

<%
facade.close();
detfacade.close();
%>