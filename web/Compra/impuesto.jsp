<%-- 
    Document   : impuesto
    Created on : 26-may-2018, 23:54:35
    Author     : solid
--%>

<jsp:useBean id="ivafacade" scope="page" class="facades.IvaFacade"/>

<%
int idiva = 0;
if(request.getParameter("idiva")!=null){
    idiva = Integer.parseInt(String.valueOf(request.getParameter("idiva")));
}
session.removeAttribute("impuesto");
beans.Iva iva = ivafacade.getIvaByID(idiva);
if(iva!=null){
    session.setAttribute("impuesto",iva.getPorcentaje());
}

ivafacade.close();
%>