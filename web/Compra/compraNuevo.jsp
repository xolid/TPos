<%-- 
    Document   : compra
    Created on : Jan 9, 2016, 7:42:35 PM
    Author     : czara
--%>

<jsp:useBean id="compra" class="beans.Compra" scope="page"/>
<jsp:setProperty name="compra" property="idcompra" />
<jsp:setProperty name="compra" property="estado" />
<jsp:setProperty name="compra" property="proveedor" />
<jsp:setProperty name="compra" property="usuario" />
<jsp:useBean id="facade" scope="page" class="facades.CompraFacade"/>
<jsp:useBean id="pfacade" scope="page" class="facades.ProveedorFacade"/>
<jsp:useBean id="afacade" scope="page" class="facades.AlmacenFacade"/>
<jsp:useBean id="ifacade" scope="page" class="facades.InventarioalmacenFacade"/>
<jsp:useBean id="dcfacade" scope="page" class="facades.DetallecompraFacade"/>
<jsp:useBean id="profacade" scope="page" class="facades.ProductoFacade"/>
<jsp:useBean id="ivafacade" scope="page" class="facades.IvaFacade"/>

<%
if(request.getParameter("accion")!=null){
    if(!String.valueOf(request.getParameter("fcompra")).equals("")){compra.setFcompra(new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(String.valueOf(request.getParameter("fcompra"))));}
    if(!String.valueOf(request.getParameter("almacen_idalmacen")).equals("")){
        int idalmacen = Integer.parseInt(String.valueOf(request.getParameter("almacen_idalmacen")));
        compra.setAlmacen(afacade.getAlmacenByID(idalmacen));
    }
    java.util.List<tools.Detalle> detalles = (java.util.List<tools.Detalle>)session.getAttribute("detalles");
    if(request.getParameter("accion").equals("1")){
        //guardar compra
        int idcompra = facade.saveCompra(compra);
        compra = facade.getCompraByID(idcompra);
        //guardar detalles de compra
        for(tools.Detalle detalle:detalles){
            beans.Producto producto = profacade.getProductoByID(detalle.getIdproducto());
            beans.Detallecompra detallecompra = new beans.Detallecompra();
            detallecompra.setIddetallecompra(0);
            detallecompra.setCompra(compra);
            detallecompra.setProducto(producto.getNombre());
            detallecompra.setMarca(producto.getMarca());
            detallecompra.setUnidad(producto.getUnidad());
            detallecompra.setCategoria(producto.getCategoria().getNombre());
            detallecompra.setCantidad(detalle.getCantidad());
            detallecompra.setPrecio(detalle.getPrecio());
            if(producto.getImpuesto()==0){
                detallecompra.setImpuesto(0.0);
            }else{
                detallecompra.setImpuesto((double)session.getAttribute("impuesto"));
            }
            
            dcfacade.saveDetallecompra(detallecompra);
        }
        //actualizar inventario
        if(compra.getEstado()==0){
            for(tools.Detalle detalle:detalles){
                beans.Inventarioalmacen inventario = ifacade.getInventarioalmacenByAlmacenANDProducto(compra.getAlmacen().getIdalmacen(),detalle.getIdproducto());
                if(inventario!=null){
                    if(inventario.getProducto().getTipo()==0){
                        inventario.setCantidad(inventario.getCantidad()+detalle.getCantidad());
                        ifacade.updateInventarioalmacen(inventario);
                    }
                }else{
                    inventario = new beans.Inventarioalmacen(compra.getAlmacen(), profacade.getProductoByID(detalle.getIdproducto()), detalle.getCantidad(), detalle.getCantidad(),detalle.getCantidad());
                    ifacade.saveInventarioalmacen(inventario);
                }
            }
        }
    }
}
session.setAttribute("detalles", new java.util.ArrayList<>());
%>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Compra - Definicion de la Compra</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                Definicion de la Compra
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <form comprae="form" id="form1" name="form1" action="compraNuevo.jsp" method="post">
                    <input type="hidden" id="idcompra" name="idcompra" value="0"/>
                    <div class="row">
                        <div class="col-lg-2">
                            <div class="form-group">
                                <label for="fcompra">Fecha: </label>
                                <input class="form-control input-sm" id="fcompra" name="fcompra" type="text" value="<%=new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())%>" required readonly/>
                            </div>      
                        </div>
                        <div class="col-lg-2 col-lg-offset-6">
                            <div class="form-group">
                                <label for="iva_idiva">Aplicar I.V.A: </label>
                                <select id="iva_idiva" name="iva_idiva" class="form-control input-sm" onchange="javascript:setIva();">
                                    <%for(beans.Iva iva:ivafacade.getIvaAll()){%>
                                        <option value="<%=iva.getIdiva()%>"><%=iva.getNombre()%></option>
                                    <%}%>
                                </select>
                            </div>      
                        </div>
                        <div class="col-lg-2">
                            <div class="form-group">
                                <label for="estado">Estado: </label>
                                <select id="estado" name="estado" class="form-control input-sm">
                                    <%for(int i=0; i< new tools.Estado().getEstados().length; i++ ){
                                        if(i!=2){%>
                                            <option value="<%=i%>"><%=new tools.Estado().getNombre(i)%></option>
                                        <%}
                                    }%>
                                </select>
                            </div>  
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="proveedor">Proveedor: </label>
                                <select id="proveedor" name="proveedor" class="form-control input-sm">
                                    <%for(beans.Proveedor proveedor:pfacade.getProveedorAll()){%>
                                        <option value="<%=proveedor.getNombre()%>"><%=proveedor.getNombre()%></option>
                                    <%}%>
                                </select>
                            </div>      
                        </div>
                        <div class="col-lg-1">
                            <div class="form-group">
                                <label for="agregar">&nbsp;</label>
                                <button type="button" class="btn btn-primary btn-block btn-sm" data-toggle="modal" data-target="#myModalNewProveedor"><i class="fa fa-plus-circle" title="Agregar un Proveedor"></i></button>
                            </div>   
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label for="usuario">Usuario: </label>
                                <select id="usuario" name="usuario" class="form-control input-sm">
                                    <option value="<%=((beans.Usuario)session.getAttribute("usuario")).getNombre()%>"><%=((beans.Usuario)session.getAttribute("usuario")).getNombre()%></option>
                                </select>
                            </div>      
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="almacen_idalmacen">Almacen de Recepcion: </label>
                                <select id="almacen_idalmacen" name="almacen_idalmacen" class="form-control input-sm">
                                    <%for(beans.Almacen almacen:afacade.getAlmacenAll()){%>
                                        <option value="<%=almacen.getIdalmacen()%>"><%=almacen.getNombre()%></option>
                                    <%}%>
                                </select>
                            </div>      
                        </div>
                    </div>
                </form>                
            </div>
        </div>
        <!-- /.panel -->
    </div>
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                Detalles de la Compra
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body" id="detalle"></div>
            <div class="panel-footer">
                <div class="row">
                    <div class="col-lg-12">
                        <button class="btn btn-primary btn-sm" type="button" onclick="javascript:save('Compra/compraNuevo.jsp','Compra/compraListado.jsp','form1','page-wrapper');"><i class="fa fa-save fa-fw"></i> Guardar</button>
                        <button class="btn btn-default btn-sm" type="button" onclick="javascript:go2to('Compra/compraListado.jsp','page-wrapper');"><i class="fa fa-backward fa-fw"></i> Volver</button>                 
                    </div>
                </div>
            </div>
            <!-- /.panel-footer -->
        </div>
        <!-- /.panel -->
    </div>
</div>
<div class="modal fade" id="myModalNewProveedor" tabindex="-1" role="dialog" aria-labelledby="myModalLabelNewProveedor" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Datos del Proveedor</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-9">
                        <div class="form-group">
                            <label for="proveedor_nombre">Nombre: (* Requerido)</label>
                            <input class="form-control input-sm" id="proveedor_nombre" name="proveedor_nombre" type="text" maxlength="45" autocomplete="off"/>
                        </div>      
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label for="proveedor_telefono">Telefono: (* Requerido)</label>
                            <input class="form-control input-sm" id="proveedor_telefono" name="proveedor_telefono" type="number" min="0" max="9999999999"/>
                        </div>  
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-9">
                        <div class="form-group">
                            <label for="proveedor_razon">Razon Social: (* Requerido)</label>
                            <input class="form-control input-sm" id="proveedor_razon" name="proveedor_razon" type="text" maxlength="80"/>
                        </div>      
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label for="proveedor_rfc">R.F.C.: (* Requerido)</label>
                            <input class="form-control input-sm" id="proveedor_rfc" name="proveedor_rfc" type="text" maxlength="80"/>
                        </div>      
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label for="proveedor_direccion">Direccion: (* Requerido)</label>
                            <input class="form-control input-sm" id="proveedor_direccion" name="proveedor_direccion" type="text" maxlength="80"/>
                        </div>      
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secundary btn-sm" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-success btn-sm" onclick="javascript:agregarProveedor();">Agregar</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<script lang="javascript">
    jQuery.datetimepicker.setLocale('es');
    jQuery(document).ready(function () {
        'use strict';
        jQuery('#fcompra').datetimepicker({
            format:'Y-m-d H:i:s',
            use24hours: true,
            lang:'es'
        });
        $("#proveedor").chosen({no_results_text: "No se encuentran resultados para: ",width: "100%"}); 
        $("#almacen_idalmacen").chosen({no_results_text: "No se encuentran resultados para: ",width: "100%"});
    });
    
    function agregarProveedor(){
        var proveedor_nombre = encodeURIComponent(document.getElementById("proveedor_nombre").value);
        var proveedor_telefono = encodeURIComponent(document.getElementById("proveedor_telefono").value);
        var proveedor_razon = encodeURIComponent(document.getElementById("proveedor_razon").value);
        var proveedor_rfc = encodeURIComponent(document.getElementById("proveedor_rfc").value);
        var proveedor_direccion = encodeURIComponent(document.getElementById("proveedor_direccion").value);
        if(proveedor_nombre !== "" && proveedor_telefono !== "" && proveedor_razon !== "" && proveedor_rfc !== "" && proveedor_direccion !== ""){
            $('#myModalNewProveedor').modal('hide');
            setTimeout(function(){
                var params = "nombre="+proveedor_nombre+"&telefono="+proveedor_telefono+"&razon="+proveedor_razon+"&rfc="+proveedor_rfc+"&direccion="+proveedor_direccion;
                processDIV("/TPos/Proveedor/agregar.jsp",params,"/TPos/Compra/compraNuevo.jsp","","page-wrapper");
            },250);
        }
    }
    
    function setIva(){
        if(document.getElementById("iva_idiva").selectedIndex !== -1){
            var idiva = document.getElementById("iva_idiva").options[document.getElementById("iva_idiva").selectedIndex].value;
            var params = "idiva="+idiva;
            processDIV("/TPos/Compra/impuesto.jsp",params,"/TPos/Detallecompra/detallecompra.jsp","","detalle");
        }
    }
    
    setIva();
</script>

<%
facade.close();
pfacade.close();
afacade.close();
ifacade.close();
dcfacade.close();
profacade.close();
%>