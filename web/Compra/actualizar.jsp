<%-- 
    Document   : impuesto
    Created on : 26-may-2018, 23:54:35
    Author     : solid
--%>

<jsp:useBean id="facade" scope="page" class="facades.CompraFacade"/>
<jsp:useBean id="pfacade" scope="page" class="facades.ProductoFacade"/>
<jsp:useBean id="dfacade" scope="page" class="facades.DetallecompraFacade"/>
<jsp:useBean id="ifacade" scope="page" class="facades.InventarioalmacenFacade"/>

<%
int opcion = -1;
if(request.getParameter("opcion")!=null){
    opcion = Integer.parseInt(String.valueOf(request.getParameter("opcion")));
}
int idcompra = 0;
if(request.getParameter("idcompra")!=null){
    idcompra = Integer.parseInt(String.valueOf(request.getParameter("idcompra")));
}
String mensaje = "";
String tipo = "";
beans.Compra compra = facade.getCompraByID(idcompra);
if(compra!=null){
    mensaje += "<br/>";
    //aplicar
    if(opcion==0){
        tipo = "info";
        if(compra.getEstado()==1){
            for(beans.Detallecompra detallecompra:dfacade.getDetallecompraByCompra(compra.getIdcompra())){
                beans.Producto producto = pfacade.getProductoByNombre(detallecompra.getProducto());
                if(producto!=null){
                    if(producto.getTipo()==0){
                        beans.Inventarioalmacen inventario = ifacade.getInventarioalmacenByAlmacenANDProducto(compra.getAlmacen().getIdalmacen(),producto.getIdproducto());
                        if(inventario!=null){
                            inventario.setCantidad(inventario.getCantidad()+detallecompra.getCantidad());
                            ifacade.updateInventarioalmacen(inventario);
                            mensaje += "["+producto.getNombre()+"] se agrega a almacen ["+compra.getAlmacen().getNombre()+"] actualizando a ["+inventario.getCantidad()+"] unidades.<br/>";
                        }else{
                            inventario = new beans.Inventarioalmacen(compra.getAlmacen(), producto, detallecompra.getCantidad(), detallecompra.getCantidad(),detallecompra.getCantidad());
                            ifacade.saveInventarioalmacen(inventario);
                            mensaje += "["+producto.getNombre()+"] no existe en almacen ["+compra.getAlmacen().getNombre()+"] pero se agrega actualizando a ["+inventario.getCantidad()+"] unidades.<br/>";
                        }
                    }else{
                        mensaje += "["+producto.getNombre()+"] no representa un incremento numerico al tratarse de un servicio.<br/>";
                    }
                }else{
                    mensaje += "["+detallecompra.getProducto()+"] no existe en la base de datos por lo que no se aplica.<br/>";
                }
            }
        }else{
            mensaje += "No se aplican cambios en el inventario al no ser una compra pendiente.<br/>";
        }
        compra.setEstado(0);
        facade.saveCompra(compra);
    }
    //cancelar
    if(opcion==1){
        tipo = "danger";
        if(compra.getEstado()==0){
            for(beans.Detallecompra detallecompra:dfacade.getDetallecompraByCompra(compra.getIdcompra())){
                beans.Producto producto = pfacade.getProductoByNombre(detallecompra.getProducto());
                if(producto!=null){
                    if(producto.getTipo()==0){
                        beans.Inventarioalmacen inventario = ifacade.getInventarioalmacenByAlmacenANDProducto(compra.getAlmacen().getIdalmacen(),producto.getIdproducto());
                        if(inventario!=null){
                            inventario.setCantidad(inventario.getCantidad()-detallecompra.getCantidad());
                            ifacade.updateInventarioalmacen(inventario);
                            mensaje += "["+producto.getNombre()+"] se retira de almacen ["+compra.getAlmacen().getNombre()+"] actualizando a ["+(inventario.getCantidad())+"] unidades.<br/>";
                        }else{
                            inventario = new beans.Inventarioalmacen(compra.getAlmacen(), producto, -detallecompra.getCantidad(), detallecompra.getCantidad(),detallecompra.getCantidad());
                            ifacade.saveInventarioalmacen(inventario);
                            mensaje += "["+producto.getNombre()+"] no existe en almacen ["+compra.getAlmacen().getNombre()+"] pero causa numeros negativos actualizando a ["+inventario.getCantidad()+"] unidades.<br/>";
                        }
                    }else{
                        mensaje += "["+producto.getNombre()+"] no representa un decremento numerico al tratarse de un servicio.<br/>";
                    }
                }else{
                    mensaje += "["+detallecompra.getProducto()+"] no existe en la base de datos por lo que no se aplica.<br/>";
                }
            }
        }else{
            mensaje += "No se aplican cambios en el inventario al no se una compra aplicada.<br/>";
        }
        compra.setEstado(2);
        facade.saveCompra(compra);
    }
}

out.print(mensaje+"|"+tipo);

facade.close();
pfacade.close();
dfacade.close();
ifacade.close();
%>