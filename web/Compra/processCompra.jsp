<%-- 
    Document   : process2
    Created on : Feb 7, 2016, 7:18:03 PM
    Author     : czara
--%>
 
<%@page import="java.util.*"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page import="org.json.simple.JSONArray"%>

<jsp:useBean id="facade" scope="page" class="facades.CompraFacade"/>

<%  
String[] cols = { "idcompra","fcompra","proveedor","usuario","estado" };
JSONObject result = new JSONObject();
JSONArray array = new JSONArray();
int amount = 10;
int start = 0;
int echo = 0;
int col = 0;
String dir = "asc";
String sStart = request.getParameter("start");
String sAmount = request.getParameter("length");
String sEcho = request.getParameter("draw");
String sCol = request.getParameter("order[0][column]");
String sdir = request.getParameter("order[0][dir]");
String buscar = request.getParameter("search[value]");
if (sStart != null) {
    start = Integer.parseInt(sStart);
    if (start < 0)
        start = 0;
}
if (sAmount != null) {
    amount = Integer.parseInt(sAmount);
    if (amount < 10 || amount > 100)
        amount = 10;
}
if (sEcho != null) {
    echo = Integer.parseInt(sEcho);
}
if (sCol != null) {
    col = Integer.parseInt(sCol);
    if (col < 0 || col > 4)
        col = 0;
}
if (sdir != null) {
    if (!sdir.equals("asc"))
        dir = "desc";
}
String colName = cols[col];
long total = facade.getTotal();
List<beans.Compra> compras = facade.getCompraForProcess(buscar, colName, dir, start, amount);
for(beans.Compra p:compras){
    JSONArray ja = new JSONArray();
    ja.add("<a href=\"#\" onclick=\"javascript:go2to('Compra/compraMostrar.jsp?id="+p.getIdcompra()+"','page-wrapper')\">"+p.getIdcompra()+"</a>");
    ja.add(new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(p.getFcompra()));
    ja.add(p.getProveedor());
    ja.add(p.getUsuario());
    ja.add(new tools.Estado().getNombre(p.getEstado()));
    array.add(ja);
}
Long totalAfterFilter = facade.getCompraForProcessCount(buscar);
result.put("draw", sEcho);
result.put("recordsTotal", total);
result.put("recordsFiltered", totalAfterFilter);
result.put("data", array);
response.setContentType("application/json");
response.setHeader("Cache-Contcompra", "no-store");
out.print(result);

facade.close();    
%>

