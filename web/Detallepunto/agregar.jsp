<%-- 
    Document   : agregar
    Created on : 8/05/2018, 12:48:53 AM
    Author     : czara
--%>

<%
int idproducto = 0;
if(request.getParameter("idproducto")!=null){
    idproducto = Integer.parseInt(String.valueOf(request.getParameter("idproducto")));
}
int cantidad = 0;
if(request.getParameter("cantidad")!=null){
    cantidad = Integer.parseInt(String.valueOf(request.getParameter("cantidad")));
}
java.math.BigDecimal precio = new java.math.BigDecimal(0.0);
if(request.getParameter("precio")!=null){
    precio = new java.math.BigDecimal(String.valueOf(request.getParameter("precio")));
}
//cargar detalles existentes
java.util.List<tools.Detalle> detalles = new java.util.ArrayList();
if(session.getAttribute("detallespunto")!=null){
    detalles = (java.util.List<tools.Detalle>)session.getAttribute("detallespunto");
}
tools.Detalle detalle = new tools.Detalle();
detalle.setIdproducto(idproducto);
detalle.setCantidad(cantidad);
detalle.setPrecio(precio);
detalles.add(detalle);
session.setAttribute("detallespunto",detalles);
%>