<%-- 
    Document   : detallepunto
    Created on : Oct 6, 2019, 12:28:10 PM
    Author     : czarate
--%>

<jsp:useBean id="cfacade" scope="page" class="facades.ClienteFacade"/>
<jsp:useBean id="pfacade" scope="page" class="facades.ProductoFacade"/>
<jsp:useBean id="tfacade" scope="page" class="facades.TipoFacade"/>

<%    
int consecutivo = 0;
java.math.BigDecimal total_venta = new java.math.BigDecimal(0.0);
java.math.BigDecimal total_pago = new java.math.BigDecimal(0.0);

java.util.Locale mexico = new java.util.Locale("es", "MX");
java.text.NumberFormat formatoImporte = java.text.NumberFormat.getCurrencyInstance(mexico);

java.util.List<tools.Pago> pagos = (java.util.List<tools.Pago>)session.getAttribute("detallespago");
java.util.List<tools.Detalle> detalles = (java.util.List<tools.Detalle>)session.getAttribute("detallespunto");
double impuesto = ((double)session.getAttribute("impuesto"));
%>


<div class="table-responsive">
    <table class="table table-striped table-bordered table-condensed table-hover" id="dataTablesDetalleCompra">
        <thead>
            <th class="col-lg-1 text-center">#</th>
            <th class="col-lg-2 text-center">Producto</th>
            <th class="col-lg-1 text-center">Cantidad</th>
            <th class="col-lg-1 text-center">Precio</th>
            <th class="col-lg-1 text-center">Subtotal</th>
            <th class="col-lg-1 text-center">I.V.A. <%=impuesto%>%</th>
            <th class="col-lg-1 text-center">Total</th>
            <th class="col-lg-1 text-center">&nbsp;</th>
        </thead>
        <tbody>
            <%if(detalles != null){%>
                <%for(tools.Detalle detalle:detalles){
                    beans.Producto producto = pfacade.getProductoByID(detalle.getIdproducto());
                    double subtotal = detalle.getPrecio().doubleValue()*detalle.getCantidad();
                    double iva = 0.0;
                    if(producto.getImpuesto()==1){
                        iva = (impuesto*subtotal)/100;
                    }
                    double total = subtotal+iva;
                    %>
                    <tr>
                        <td rowspan="2" class="text-center" style="vertical-align: middle; border-bottom:1pt solid black;"><%=(consecutivo+1)%></td>
                        <td rowspan="2" class="text-center" style="vertical-align: middle; border-bottom:1pt solid black;"><%=producto.getNombre()%></td>
                        <td class="text-center"><%=detalle.getCantidad()%></td>
                        <td class="text-center"><%=formatoImporte.format(detalle.getPrecio())%></td>
                        <td class="text-center"><%=formatoImporte.format(subtotal)%></td>
                        <td class="text-center"><%=formatoImporte.format(iva)%></td>
                        <td class="text-center"><%=formatoImporte.format(total)%></td>
                        <td rowspan="2" class="text-center" style="vertical-align: middle; border-bottom:1pt solid black;"><button class="btn btn-warning btn-block btn-lg" type="button" onclick="javascript:borrar(<%=(consecutivo)%>);"><i class="fa fa-minus-circle fa-fw"></i></button></td>
                    </tr>
                    <tr>
                        <td colspan="5" class="text-center" style="border-bottom:1pt solid black;">Marca [<%=producto.getMarca()%>] - Unidad [<%=producto.getUnidad()%>] - Categoria [<%=producto.getCategoria().getNombre()%>]</td>
                    </tr>
                    <%consecutivo++;
                    total_venta = total_venta.add(new java.math.BigDecimal(total));
                }
            }%>
        </tbody>
    </table>
</div>
<%
if(pagos!=null){
    for(tools.Pago pago:pagos){
        total_pago = total_pago.add(pago.getMonto());
    }    
}
java.math.BigDecimal resta = total_venta.subtract(total_pago);
%>

<div class="row">
    <div class="col-lg-6">
        <button class="btn btn-primary btn-lg btn-block" type="button" data-toggle="modal" data-target="#myModalNewPago" <%=resta.compareTo(java.math.BigDecimal.ZERO)<=0?"DISABLED":""%>><i class="fa fa-plus fa-fw"></i> Agregar Pago</button>
    </div>
    <div class="col-lg-6">
        <button class="btn btn-success btn-lg btn-block" type="button" onclick="javascript:guardar();" <%=resta.compareTo(java.math.BigDecimal.ZERO)>0?"DISABLED":""%>><i class="fa fa-save fa-fw"></i> Completar Venta</button>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 text-right">
        <%out.print("<h3><small>Importe: </small>"+formatoImporte.format(total_venta)+"</h3>");    
        if(pagos!=null){
            for(tools.Pago pago:pagos){
                out.print("<h3><small>Pago ["+pago.getTipo()+"]: </small>"+formatoImporte.format(pago.getMonto())+"</h3>");
            }
        }else{
            out.print("<h3><small>Pagado: </small>$0.00</h3>");
        }
        if(resta.compareTo(java.math.BigDecimal.ZERO) > 0){
            out.print("<h3 class='text-danger'><small>Resta: </small>"+formatoImporte.format(resta)+"</h3>");
        }else{
            out.print("<h3 class='text-success'><small>Cambio: </small>"+formatoImporte.format(resta)+"</h3>");
        }%>
    </div>
</div>

<script>
    
    function borrar(indice){
        var params = "indice="+indice;
        processDIV("/TPos/Detallepunto/remover.jsp",params,"/TPos/Detallepunto/detallepunto.jsp","","detalle");
    }
    
</script>          
    
    
<%
cfacade.close();
pfacade.close();
tfacade.close();
%>