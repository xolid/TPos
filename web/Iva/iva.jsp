<%-- 
    Document   : iva
    Created on : Jan 9, 2016, 7:42:35 PM
    Author     : czara
--%>

<jsp:useBean id="iva" class="beans.Iva" scope="page"/>
<jsp:setProperty name="iva" property="*" />
<jsp:useBean id="facade" scope="page" class="facades.IvaFacade"/>

<%if(request.getParameter("accion")!=null){
    if(request.getParameter("accion").equals("1")){
        if(iva.getIdiva()==0){
            facade.saveIva(iva);
        }else{
            facade.updateIva(iva);
        }
    }
    if(request.getParameter("accion").equals("2")){
        facade.deleteIva(iva);        
    }
}
if(request.getParameter("id")!=null){
    iva=facade.getIvaByID(Integer.parseInt((String)request.getParameter("id")));
}else{
    iva.setIdiva(0);
    iva.setNombre("");
    iva.setPorcentaje(0.0);
}%>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Catalogos Primarios - Iva de Pago</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                Definicion del Iva de Pago
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <form ivae="form" id="form1" name="form1" action="iva.jsp" method="post">
                    <input type="hidden" id="idiva" name="idiva" value="<%=iva.getIdiva()%>"/>
                    <div class="row">
                        <div class="col-lg-10">
                            <div class="form-group">
                                <label for="nombre">Nombre: </label>
                                <input class="form-control input-sm" id="nombre" name="nombre" type="text" maxlength="45" value="<%=iva.getNombre()%>" autocomplete="off" required autofocus/>
                            </div>      
                        </div>
                        <div class="col-lg-2">
                            <div class="form-group">
                                <label for="porcentaje">Porcentaje: </label>
                                <input class="form-control input-sm" id="porcentaje" name="porcentaje" type="number" max="100" min="0" step="0.01" value="<%=iva.getPorcentaje()%>" required/>
                            </div>      
                        </div>
                    </div>
                </form>                
            </div>
            <div class="panel-footer">
                <div class="row">
                    <div class="col-lg-12">
                        <button class="btn btn-primary btn-sm btn-sm" type="button" onclick="javascript:save('Iva/iva.jsp','Iva/ivaListado.jsp','form1','page-wrapper');"><i class="fa fa-save fa-fw"></i> Guardar</button>
                        <%if(iva.getIdiva()!=0){%>
                            <button class="btn btn-danger btn-sm btn-sm" type="button" onclick="javascript:erase('Iva/iva.jsp','Iva/ivaListado.jsp','form1','page-wrapper');"><i class="fa fa-eraser fa-fw"></i> Borrar</button>
                        <%}%>
                        <button class="btn btn-default btn-sm btn-sm" type="button" onclick="javascript:go2to('Iva/ivaListado.jsp','page-wrapper');"><i class="fa fa-backward fa-fw"></i> Volver</button>                 
                    </div>
                </div>
            </div>
            <!-- /.panel-footer -->
        </div>
        <!-- /.panel -->
    </div>
</div>
<!-- /.row -->

<%
facade.close();
%>