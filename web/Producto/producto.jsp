<%-- 
    Document   : producto
    Created on : Jan 9, 2016, 7:42:35 PM
    Author     : czara
--%>

<jsp:useBean id="producto" class="beans.Producto" scope="page"/>
<jsp:setProperty name="producto" property="idproducto" />
<jsp:setProperty name="producto" property="codigo" />
<jsp:setProperty name="producto" property="nombre" />
<jsp:setProperty name="producto" property="marca" />
<jsp:setProperty name="producto" property="clave" />
<jsp:setProperty name="producto" property="unidad" />
<jsp:setProperty name="producto" property="tipo" />
<jsp:setProperty name="producto" property="impuesto" />
<jsp:useBean id="facade" scope="page" class="facades.ProductoFacade"/>
<jsp:useBean id="cfacade" scope="page" class="facades.CategoriaFacade"/>

<%    
if(request.getParameter("accion")!=null){
    if(!String.valueOf(request.getParameter("precio")).equals("")){producto.setPrecio(new java.math.BigDecimal(String.valueOf(request.getParameter("precio"))));}
    if(!String.valueOf(request.getParameter("categoria_idcategoria")).equals("")){
        int idcategoria = Integer.parseInt(String.valueOf(request.getParameter("categoria_idcategoria")));
        producto.setCategoria(cfacade.getCategoriaByID(idcategoria));
    }
    if(request.getParameter("accion").equals("1")){
        if(producto.getIdproducto()==0){
            facade.saveProducto(producto);
        }else{
            facade.updateProducto(producto);
        }
    }
    if(request.getParameter("accion").equals("2")){
        facade.deleteProducto(producto);        
    }
}
if(request.getParameter("id")!=null){
    int idproducto=Integer.parseInt((String)request.getParameter("id"));
    producto=facade.getProductoByID(idproducto);
}else{
    producto.setIdproducto(0);
    producto.setCodigo(0);
    producto.setNombre("");
    producto.setMarca("");
    producto.setClave("");
    producto.setUnidad("");
    producto.setPrecio(new java.math.BigDecimal(0.0));
    producto.setCategoria(new beans.Categoria());
    producto.setTipo(0);
    producto.setImpuesto(1);
}%>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Catalogos del Sistema - Producto</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-9">
        <div class="panel panel-primary">
            <div class="panel-heading">
                Definicion del Producto
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <form productoe="form" id="form1" name="form1" action="producto.jsp" method="post">
                    <input type="hidden" id="idproducto" name="idproducto" value="<%=producto.getIdproducto()%>"/>
                    <div class="row">
                        <div class="col-lg-2">
                            <div class="form-group">
                                <label for="codigo">Codigo: </label>
                                <input class="form-control input-sm" id="codigo" name="codigo" type="number" min="0" step="1" value="<%=producto.getCodigo()%>" autocomplete="off" required autofocus/>
                            </div>      
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="nombre">Nombre: </label>
                                <input class="form-control input-sm" id="nombre" name="nombre" type="text" maxlength="75" value="<%=producto.getNombre().replace("\"","&quot")%>" autocomplete="off" required/>
                            </div>      
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="marca">Marca: </label>
                                <input class="form-control input-sm" id="marca" name="marca" type="text" maxlength="45" value="<%=producto.getMarca().replace("\"","&quot")%>" autocomplete="off" required/>
                            </div>      
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-2">
                            <div class="form-group">
                                <label for="clave">Clave: </label>
                                <input class="form-control input-sm" id="clave" name="clave" type="text" maxlength="45" value="<%=producto.getClave()%>" autocomplete="off" required/>
                            </div>  
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="categoria_idcategoria">Categoria: </label>
                                <select id="categoria_idcategoria" name="categoria_idcategoria" class="form-control input-sm">
                                    <%for(beans.Categoria categoria:cfacade.getCategoriaAll()){%>
                                        <option value="<%=categoria.getIdcategoria()%>" <%=categoria.getIdcategoria()==producto.getCategoria().getIdcategoria()?"SELECTED":""%>><%=categoria.getNombre()%></option>
                                    <%}%>
                                </select>
                            </div>  
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="tipo">Tipo de Producto: </label>
                                <select id="tipo" name="tipo" class="form-control input-sm">
                                    <%for(int i=0;i<new tools.Tipo().getTipos().length;i++){%>
                                        <option value="<%=i%>" <%=producto.getTipo()==i?"SELECTED":""%>><%=new tools.Tipo().getNombre(i)%></option>
                                    <%}%>
                                </select>
                            </div>      
                        </div>
                        <div class="col-lg-2">
                            <div class="form-group">
                                <label for="unidad">Unidad: </label>
                                <input class="form-control input-sm" id="unidad" name="unidad" type="text" maxlength="45" value="<%=producto.getUnidad()%>" autocomplete="off" required/>
                            </div>  
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-2">
                            <div class="form-group">
                                <label for="precio">Precio: </label>
                                <input class="form-control input-sm" id="precio" name="precio" type="number" min="0" step="0.01" value="<%=producto.getPrecio()%>" autocomplete="off" required/>
                            </div>      
                        </div>
                        <div class="col-lg-2">
                            <div class="form-group">
                                <label for="impuesto">Impuesto: </label>
                                <select id="impuesto" name="impuesto" class="form-control input-sm">
                                    <option value="0" <%=producto.getImpuesto()==0?"SELECTED":""%>>NO</option>
                                    <option value="1" <%=producto.getImpuesto()==1?"SELECTED":""%>>SI</option>
                                </select>
                            </div>      
                        </div>
                    </div>
                </form>                
            </div>
            <div class="panel-footer">
                <div class="row">
                    <div class="col-lg-12">
                        <button class="btn btn-primary btn-sm" type="button" onclick="javascript:save('Producto/producto.jsp','Producto/productoListado.jsp','form1','page-wrapper');"><i class="fa fa-save fa-fw"></i> Guardar</button>
                        <%if(producto.getIdproducto()!=0){%>
                            <button class="btn btn-danger btn-sm" type="button" onclick="javascript:erase('Producto/producto.jsp','Producto/productoListado.jsp','form1','page-wrapper');"><i class="fa fa-eraser fa-fw"></i> Borrar</button>
                        <%}%>
                        <button class="btn btn-default btn-sm" type="button" onclick="javascript:go2to('Producto/productoListado.jsp','page-wrapper');"><i class="fa fa-backward fa-fw"></i> Volver</button>                 
                    </div>
                </div>
            </div>
            <!-- /.panel-footer -->
        </div>
        <!-- /.panel -->
    </div>
    <%if(producto.getIdproducto()>0){%>
        <div class="col-md-3">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Imagen del Producto
                </div>
                <div class="panel-body">
                    <div class="text-center">
                        <%if(producto.getFoto()==null){%>
                            <img class="profile-user-img img-fluid img-circle" src="<%=request.getContextPath()%>/images/default-150x150.png" height="150" width="150" alt="User profile picture">
                        <%}else{%>
                            <img class="profile-user-img img-fluid img-circle" src="<%=request.getContextPath()%>/Upload/descargar.jsp?id=<%=producto.getIdproducto()%>&tipo=producto" height="150" width="150" alt="Product profile picture">
                        <%}%>
                    </div>
                </div>
                <div class="panel-footer">
                    <div class="row">
                        <div class="col-lg-6">
                            <a href="#" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModalUploadProducto"><i class="fa fa-picture-o fa-fw"></i> <%=producto.getFoto()==null?"Subir":"Cambiar"%></a>
                        </div>
                        <div class="col-lg-6 text-right">
                            <%if(producto.getFoto()!=null){%>
                                <a href="#" class="btn btn-danger btn-sm" onclick="javascript:remove();"><i class="fa fa-times fa-fw"></i> Remover</a>
                            <%}%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <%}%>
</div>
<!-- /.row -->
<div class="modal fade" id="myModalUploadProducto" tabindex="-1" role="dialog" aria-labelledby="myModalLabelProducto" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Seleccionar Archivo</h4>
            </div>
            <div class="modal-body">
                <object data="<%=request.getContextPath()%>/Upload/seleccionarIMG.jsp?id=<%=producto.getIdproducto()%>&tipo=producto" width="100%" height="120">
                    <embed src="<%=request.getContextPath()%>/Upload/seleccionarIMG.jsp?id=<%=producto.getIdproducto()%>&tipo=producto" width="100%" height="120"></embed>
                </object>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
              
<script>

    window.closeModal = function(){
        $('#myModalUploadProducto').modal('hide');
        setTimeout(function(){
            loadPanel('Producto/producto.jsp','id=<%=producto.getIdproducto()%>','page-wrapper');
        },250);
    };

    function remove(){
        var params = "id=<%=producto.getIdproducto()%>&tipo=producto";
        processDIV("/TPos/Upload/remover.jsp",params,"Producto/producto.jsp","id=<%=producto.getIdproducto()%>","page-wrapper");
    }

</script>

<%
facade.close();
cfacade.close();
%>