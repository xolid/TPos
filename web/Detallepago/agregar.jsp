<%-- 
    Document   : agregar
    Created on : 8/05/2018, 12:48:53 AM
    Author     : czara
--%>

<%
String tipo = "";
if(request.getParameter("tipo")!=null){
    tipo = String.valueOf(request.getParameter("tipo"));
}
java.math.BigDecimal monto = new java.math.BigDecimal(0.0);
if(request.getParameter("monto")!=null){
    monto = new java.math.BigDecimal(String.valueOf(request.getParameter("monto")));
}
//cargar detalles existentes
java.util.List<tools.Pago> pagos = new java.util.ArrayList();
if(session.getAttribute("detallespago")!=null){
    pagos = (java.util.List<tools.Pago>)session.getAttribute("detallespago");
}
tools.Pago pago = new tools.Pago();
pago.setTipo(tipo);
pago.setMonto(monto);
pagos.add(pago);
session.setAttribute("detallespago",pagos);
%>