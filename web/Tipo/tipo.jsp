<%-- 
    Document   : tipo
    Created on : Jan 9, 2016, 7:42:35 PM
    Author     : czara
--%>

<jsp:useBean id="tipo" class="beans.Tipo" scope="page"/>
<jsp:setProperty name="tipo" property="*" />
<jsp:useBean id="facade" scope="page" class="facades.TipoFacade"/>

<%if(request.getParameter("accion")!=null){
    if(request.getParameter("accion").equals("1")){
        if(tipo.getIdtipo()==0){
            facade.saveTipo(tipo);
        }else{
            facade.updateTipo(tipo);
        }
    }
    if(request.getParameter("accion").equals("2")){
        facade.deleteTipo(tipo);        
    }
}
if(request.getParameter("id")!=null){
    tipo=facade.getTipoByID(Integer.parseInt((String)request.getParameter("id")));
}else{
    tipo.setIdtipo(0);
    tipo.setNombre("");
}%>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Catalogos Primarios - Tipo de Pago</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                Definicion del Tipo de Pago
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <form tipoe="form" id="form1" name="form1" action="tipo.jsp" method="post">
                    <input type="hidden" id="idtipo" name="idtipo" value="<%=tipo.getIdtipo()%>"/>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="nombre">Nombre: </label>
                                <input class="form-control input-sm" id="nombre" name="nombre" type="text" maxlength="45" value="<%=tipo.getNombre()%>" autocomplete="off" required autofocus/>
                            </div>      
                        </div>
                    </div>
                </form>                
            </div>
            <div class="panel-footer">
                <div class="row">
                    <div class="col-lg-12">
                        <button class="btn btn-primary btn-sm btn-sm" type="button" onclick="javascript:save('Tipo/tipo.jsp','Tipo/tipoListado.jsp','form1','page-wrapper');"><i class="fa fa-save fa-fw"></i> Guardar</button>
                        <%if(tipo.getIdtipo()!=0){%>
                            <button class="btn btn-danger btn-sm btn-sm" type="button" onclick="javascript:erase('Tipo/tipo.jsp','Tipo/tipoListado.jsp','form1','page-wrapper');"><i class="fa fa-eraser fa-fw"></i> Borrar</button>
                        <%}%>
                        <button class="btn btn-default btn-sm btn-sm" type="button" onclick="javascript:go2to('Tipo/tipoListado.jsp','page-wrapper');"><i class="fa fa-backward fa-fw"></i> Volver</button>                 
                    </div>
                </div>
            </div>
            <!-- /.panel-footer -->
        </div>
        <!-- /.panel -->
    </div>
</div>
<!-- /.row -->

<%
facade.close();
%>