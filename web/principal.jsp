<%-- 
    Document   : principal
    Created on : Jan 9, 2016, 4:23:51 PM
    Author     : czara
--%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="TPOS - Terminal Point of Sale">
        <meta name="author" content="MI. Carlos Zarate">
        <title>TPOS - Terminal Point of Sale</title>
        <link rel="shortcut icon" href="<%=request.getContextPath()%>/images/favicon.ico">
        <link href="<%=request.getContextPath()%>/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="<%=request.getContextPath()%>/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="<%=request.getContextPath()%>/css/sb-admin.css" rel="stylesheet" type="text/css"/>
        <link href="<%=request.getContextPath()%>/css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="<%=request.getContextPath()%>/css/bootstrap-dialog/bootstrap-dialog.css" rel="stylesheet" type="text/css"/>  
        <link href="<%=request.getContextPath()%>/css/plugins/morris/morris-0.4.3.min.css" rel="stylesheet" type="text/css"/>
        <link href="<%=request.getContextPath()%>/css/easy-autocomplete.min.css" rel="stylesheet" type="text/css"/>
        <link href="<%=request.getContextPath()%>/css/jquery.datetimepicker.css" type="text/css" rel="stylesheet"/>
        <link href="<%=request.getContextPath()%>/css/chosen/bootstrap-chosen.css" rel="stylesheet"/>
    </head>
    <body>
        <div id="wrapper">
            <nav class="navbar navbar-default navbar-fixed-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="principal.jsp">TPOS - Terminal Point of Sale</a>
                </div>
                <!-- /.navbar-header -->
                <ul class="nav navbar-top-links navbar-right">
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" onclick="javascript:buscar();"><i class="fa fa-search fa-fw"></i></a>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown"><div id="datetime"></div></a>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-building-o fa-fw"></i> <%=((beans.Sucursal)session.getAttribute("sucursal"))==null?"Global":((beans.Sucursal)session.getAttribute("sucursal")).getNombre()%></a>
                    </li>
                    <!-- /.dropdown -->
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-user fa-fw"></i> <%=((beans.Usuario)session.getAttribute("usuario")).getEmail()%> <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdow-user">    
                            <li><a><i class="fa fa-user fa-fw"></i> <%=((beans.Usuario)session.getAttribute("usuario")).getNombre()%></a></li>
                            <li><a><i class="fa fa-unlock fa-fw"></i> <%=((beans.Rol)session.getAttribute("rol")).getNombre()%></a></li>
                            <li><a href="#" onclick="javascript:go2to('cambiar.jsp','page-wrapper');"><i class="fa fa-key fa-fw"></i> Cambiar Password</a></li>
                            <li class="divider"></li>
                            <li><a href="index.jsp"><i class="fa fa-sign-out fa-fw"></i> Cerrar Sesion</a></li>
                        </ul>
                        <!-- /.dropdown-user -->
                    </li>
                    <!-- /.dropdown -->
                </ul>
                <!-- /.navbar-top-links -->
                <div class="navbar-default navbar-static-side" role="navigation">
                    <div class="sidebar-collapse">
                        <ul class="nav" id="side-menu">
                            <%if(((beans.Rol)session.getAttribute("rol")).getReporte()==1){%>
                                <li>
                                    <a href="#" onclick="javascript:go2to('Dashboard/dashboard.jsp', 'page-wrapper');"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                                </li>
                            <%}if(((beans.Rol)session.getAttribute("rol")).getPunto()==1 && ((beans.Usuario)session.getAttribute("usuario")).getSucursal()!=null){%>  
                                <li>
                                    <a href="#" onclick="javascript:go2to('Punto/punto.jsp', 'page-wrapper');"><i class="fa fa-shopping-cart fa-fw"></i> Punto de Venta</a>
                                </li>
                            <%}if(((beans.Rol)session.getAttribute("rol")).getVende()==1){%>
                                <li>
                                    <a href="#" onclick="javascript:go2to('Venta/ventaListado.jsp', 'page-wrapper');"><i class="fa fa-arrow-right fa-fw"></i> Admon. Ventas</a>
                                </li>
                            <%}if(((beans.Rol)session.getAttribute("rol")).getCompra()==1){%>
                                <li>
                                    <a href="#" onclick="javascript:go2to('Compra/compraListado.jsp', 'page-wrapper');"><i class="fa fa-arrow-left fa-fw"></i> Admon. Compras</a>
                                </li>
                            <%}if(((beans.Rol)session.getAttribute("rol")).getReporte()==1){%>    
                                <li>
                                    <a href="#"><i class="fa fa-file-text fa-fw"></i> Reportes del Sistema<span class="fa arrow"></span></a>
                                    <ul class="nav nav-second-level">
                                        <li>
                                            <a href="#" onclick="javascript:go2to('Reporte/ventasListado.jsp', 'page-wrapper');"><i class="fa fa-shopping-cart fa-fw"></i> RPT de Ventas</a>
                                        </li>
                                        <li>
                                            <a href="#" onclick="javascript:go2to('Reporte/comprasListado.jsp', 'page-wrapper');"><i class="fa fa-money fa-fw"></i> RPT de Compras</a>
                                        </li>
                                        <li>
                                            <a href="#" onclick="javascript:go2to('Reporte/inventarioListado.jsp', 'page-wrapper');"><i class="fa fa-list-alt fa-fw"></i> RPT de Inventario</a>
                                        </li>
                                        <li>
                                            <a href="#" onclick="javascript:go2to('Reporte/productosListado.jsp', 'page-wrapper');"><i class="fa fa-barcode fa-fw"></i> RPT de Productos</a>
                                        </li>
                                        <li>
                                            <a href="#" onclick="javascript:go2to('Reporte/bitacoraListado.jsp', 'page-wrapper');"><i class="fa fa-eye fa-fw"></i> RPT de Bitacora</a>
                                        </li>
                                    </ul>
                                    <!-- /.nav-second-level -->
                                </li>
                            <%}if(((beans.Rol)session.getAttribute("rol")).getInventario()==1){%>
                                <li>
                                    <a href="#"><i class="fa fa-list-alt fa-fw"></i> Admon. de Inventario<span class="fa arrow"></span></a>
                                    <ul class="nav nav-second-level">
                                        <li>
                                            <a href="#" onclick="javascript:go2to('Inventario/almacen/inventarioListado.jsp', 'page-wrapper');"><i class="fa fa-home fa-fw"></i> Inventario de almacenes</a>
                                        </li>
                                        <li>
                                            <a href="#" onclick="javascript:go2to('Inventario/sucursal/inventarioListado.jsp', 'page-wrapper');"><i class="fa fa-building-o fa-fw"></i> Inventario de sucursales</a>
                                        </li>                                      
                                    </ul>
                                    <!-- /.nav-second-level -->
                                </li>
                            <%}if(((beans.Rol)session.getAttribute("rol")).getCatalogo()==1){%>
                                <li>
                                    <a href="#"><i class="fa fa-briefcase fa-fw"></i> Admon. de Catalogos<span class="fa arrow"></span></a>
                                    <ul class="nav nav-second-level">
                                        <li>
                                            <a href="#" onclick="javascript:go2to('Proveedor/proveedorListado.jsp', 'page-wrapper');"><i class="fa fa-bookmark fa-fw"></i> Admon. de Proveedores</a>
                                        </li>
                                        <li>
                                            <a href="#" onclick="javascript:go2to('Cliente/clienteListado.jsp', 'page-wrapper');"><i class="fa fa-bookmark-o fa-fw"></i> Admon. de Clientes</a>
                                        </li>
                                        <li>
                                            <a href="#" onclick="javascript:go2to('Tipo/tipoListado.jsp', 'page-wrapper');"><i class="fa fa-credit-card fa-fw"></i> Admon. tipos de pago</a>
                                        </li>
                                        <li>
                                            <a href="#" onclick="javascript:go2to('Iva/ivaListado.jsp', 'page-wrapper');"><i class="fa fa-legal fa-fw"></i> Admon. Impuestos</a>
                                        </li>
                                        <li>
                                            <a href="#"><i class="fa fa-globe fa-fw"></i> Admon. de Sitios<span class="fa arrow"></span></a>
                                            <ul class="nav nav-second-level">
                                                <li>
                                                    <a href="#" onclick="javascript:go2to('Sucursal/sucursalListado.jsp', 'page-wrapper');"><i class="fa fa-building-o fa-fw"></i> Sucursales</a>
                                                </li>
                                                <li>
                                                    <a href="#" onclick="javascript:go2to('Almacen/almacenListado.jsp', 'page-wrapper');"><i class="fa fa-home fa-fw"></i> Almacenes</a>
                                                </li>
                                            </ul>
                                            <!-- /.nav-second-level -->
                                        </li>
                                        <li>
                                            <a href="#"><i class="fa fa-barcode fa-fw"></i> Admon. de Productos<span class="fa arrow"></span></a>
                                            <ul class="nav nav-second-level">
                                                <li>
                                                    <a href="#" onclick="javascript:go2to('Categoria/categoriaListado.jsp', 'page-wrapper');"><i class="fa fa-book fa-fw"></i> Categorias</a>
                                                </li>
                                                <li>
                                                    <a href="#" onclick="javascript:go2to('Producto/productoListado.jsp', 'page-wrapper');"><i class="fa fa-barcode fa-fw"></i> Productos</a>
                                                </li>
                                            </ul>
                                            <!-- /.nav-second-level -->
                                        </li>
                                    </ul>
                                </li>
                            <%}if(((beans.Rol)session.getAttribute("rol")).getAdministra()==1){%>
                                <li>
                                    <a href="#"><i class="fa fa-wrench fa-fw"></i> Admon. de Sistema<span class="fa arrow"></span></a>
                                    <ul class="nav nav-second-level">
                                        <li>
                                            <a href="#" onclick="javascript:go2to('Rol/rolListado.jsp', 'page-wrapper');"><i class="fa fa-unlock fa-fw"></i> Roles</a>
                                        </li>
                                        <li>
                                            <a href="#" onclick="javascript:go2to('Usuario/usuarioListado.jsp', 'page-wrapper');"><i class="fa fa-users fa-fw"></i> Usuarios</a>
                                        </li>
                                    </ul>
                                    <!-- /.nav-second-level -->
                                </li>
                            <%}%>
                        </ul>
                        <!-- /#side-menu -->
                    </div>
                    <!-- /.sidebar-collapse -->
                </div>
                <!-- /.navbar-static-side -->
            </nav>
            <div id="page-wrapper"></div>
            <!-- /#page-wrapper -->
        </div>
        <div class="modal fade" id="myModalBuscar" tabindex="-1" role="dialog" aria-labelledby="myModalLabelSearch" aria-hidden="true">
            <div class="modal-dialog modal-xlg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Buscar Producto o Servicio</h4>
                    </div>
                    <div class="modal-body">
                        <object data="<%=request.getContextPath()%>/buscar.jsp" width="100%" height="500">
                            <embed src="<%=request.getContextPath()%>/buscar.jsp" width="100%" height="500"></embed>
                        </object>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>                                  
        <!-- Core Scripts - Include with every page -->
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-1.10.2.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/bootstrap.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/plugins/metisMenu/jquery.metisMenu.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/sb-admin.js"></script>    
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/navigation.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/validation.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/plugins/dataTables/jquery.dataTables.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/plugins/dataTables/dataTables.bootstrap.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/bootstrap-dialog/bootstrap-dialog.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.easy-autocomplete.min.js"></script>
        <!-- DateTimePicker -->
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.datetimepicker.full.js"></script>
        <!-- BootstrapNotify -->
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/bootstrap-notify/bootstrap-notify.js"></script>
        <!-- Chosen -->
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/chosen/chosen.jquery.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/chosen/chosen.proto.js"></script>
        <script lang="javascript">
            
            function buscar(){
                $('#myModalBuscar').modal('show');
            }
            
            function date_time(id) {        
                date = new Date;
                year = date.getFullYear();
                month = date.getMonth();
                months = new Array('Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
                d = date.getDate();
                day = date.getDay();
                days = new Array('Domingo','Lunes','Martes','Miercoles','Jueves','Viernes','Sabado');
                h = date.getHours();
                if(h<10) { h = "0"+h; }
                m = date.getMinutes();
                if(m<10) { m = "0"+m; }
                s = date.getSeconds();
                if(s<10) { s = "0"+s; }
                result = ''+days[day]+' '+d+' '+months[month]+' '+year+' '+h+':'+m+':'+s;
                document.getElementById(id).innerHTML = result;
                setTimeout('date_time("'+id+'");','1000');
                return true;
            }

            date_time('datetime');
            
        </script>  
    </body>
</html>