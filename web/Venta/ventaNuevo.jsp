<%-- 
    Document   : venta
    Created on : Jan 9, 2016, 7:42:35 PM
    Author     : czara
--%>

<jsp:useBean id="venta" class="beans.Venta" scope="page"/>
<jsp:setProperty name="venta" property="idventa" />
<jsp:setProperty name="venta" property="estado" />
<jsp:setProperty name="venta" property="cliente" />
<jsp:setProperty name="venta" property="usuario" />
<jsp:useBean id="facade" scope="page" class="facades.VentaFacade"/>
<jsp:useBean id="pfacade" scope="page" class="facades.ClienteFacade"/>
<jsp:useBean id="afacade" scope="page" class="facades.SucursalFacade"/>
<jsp:useBean id="ifacade" scope="page" class="facades.InventariosucursalFacade"/>
<jsp:useBean id="dvfacade" scope="page" class="facades.DetalleventaFacade"/>
<jsp:useBean id="profacade" scope="page" class="facades.ProductoFacade"/>
<jsp:useBean id="ivafacade" scope="page" class="facades.IvaFacade"/>

<%
if(request.getParameter("accion")!=null){
    if(!String.valueOf(request.getParameter("fventa")).equals("")){venta.setFventa(new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(String.valueOf(request.getParameter("fventa"))));}
    if(!String.valueOf(request.getParameter("sucursal_idsucursal")).equals("")){
        int idsucursal = Integer.parseInt(String.valueOf(request.getParameter("sucursal_idsucursal")));
        venta.setSucursal(afacade.getSucursalByID(idsucursal));
    }
    java.util.List<tools.Detalle> detalles = (java.util.List<tools.Detalle>)session.getAttribute("detalles");
    if(request.getParameter("accion").equals("1")){
        //guardar venta
        int idventa = facade.saveVenta(venta);
        venta = facade.getVentaByID(idventa);
        //guardar detalles de venta
        for(tools.Detalle detalle:detalles){
            beans.Producto producto = profacade.getProductoByID(detalle.getIdproducto());
            beans.Detalleventa detalleventa = new beans.Detalleventa();
            detalleventa.setIddetalleventa(0);
            detalleventa.setVenta(venta);
            detalleventa.setProducto(producto.getNombre());
            detalleventa.setMarca(producto.getMarca());
            detalleventa.setUnidad(producto.getUnidad());
            detalleventa.setCategoria(producto.getCategoria().getNombre());
            detalleventa.setCantidad(detalle.getCantidad());
            detalleventa.setPrecio(detalle.getPrecio());
            if(producto.getImpuesto()==0){
                detalleventa.setImpuesto(0.0);
            }else{
                detalleventa.setImpuesto((double)session.getAttribute("impuesto"));
            }
            dvfacade.saveDetalleventa(detalleventa);
        }
        //actualizar inventario
        if(venta.getEstado()==0){
            for(tools.Detalle detalle:detalles){
                beans.Inventariosucursal inventario = ifacade.getInventariosucursalBySucursalANDProducto(venta.getSucursal().getIdsucursal(),detalle.getIdproducto());
                if(inventario!=null){
                    if(inventario.getProducto().getTipo()==0){
                        inventario.setCantidad(inventario.getCantidad()-detalle.getCantidad());
                        ifacade.updateInventariosucursal(inventario);
                    }
                }else{
                    inventario = new beans.Inventariosucursal(profacade.getProductoByID(detalle.getIdproducto()), venta.getSucursal(), -detalle.getCantidad(), detalle.getCantidad(),detalle.getCantidad());
                    ifacade.saveInventariosucursal(inventario);
                }
            }
        }
    }
}
session.setAttribute("detalles", new java.util.ArrayList<>());
%>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Venta - Definicion de la Venta</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                Definicion de la Venta
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <form ventae="form" id="form1" name="form1" action="ventaNuevo.jsp" method="post">
                    <input type="hidden" id="idventa" name="idventa" value="0"/>
                    <div class="row">
                        <div class="col-lg-2">
                            <div class="form-group">
                                <label for="fventa">Fecha: </label>
                                <input class="form-control input-sm" id="fventa" name="fventa" type="text" value="<%=new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())%>" required readonly/>
                            </div>      
                        </div>
                        <div class="col-lg-offset-8 col-lg-2">
                            <div class="form-group">
                                <label for="estado">Estado: </label>
                                <select id="estado" name="estado" class="form-control input-sm">
                                    <%for(int i=0; i< new tools.Estado().getEstados().length; i++ ){
                                        if(i!=2){%>
                                            <option value="<%=i%>"><%=new tools.Estado().getNombre(i)%></option>
                                        <%}
                                    }%>
                                </select>
                            </div>  
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="cliente">Cliente: </label>
                                <select id="cliente" name="cliente" class="form-control input-sm">
                                    <%for(beans.Cliente cliente:pfacade.getClienteAll()){%>
                                        <option value="<%=cliente.getNombre()%>"><%=cliente.getNombre()%></option>
                                    <%}%>
                                </select>
                            </div>      
                        </div>
                        <div class="col-lg-1">
                            <div class="form-group">
                                <label for="agregar">&nbsp;</label>
                                <button type="button" class="btn btn-primary btn-block btn-sm" data-toggle="modal" data-target="#myModalNewCliente"><i class="fa fa-plus-circle" title="Agregar un Cliente"></i></button>
                            </div>   
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label for="usuario">Usuario: </label>
                                <select id="usuario" name="usuario" class="form-control input-sm">
                                    <option value="<%=((beans.Usuario)session.getAttribute("usuario")).getNombre()%>"><%=((beans.Usuario)session.getAttribute("usuario")).getNombre()%></option>
                                </select>
                            </div>      
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="sucursal_idsucursal">Sucursal de Salida: </label>
                                <select id="sucursal_idsucursal" name="sucursal_idsucursal" class="form-control input-sm" onchange="javascript:setIva();">
                                    <%for(beans.Sucursal sucursal:afacade.getSucursalAll()){
                                        beans.Sucursal s = ((beans.Sucursal)session.getAttribute("sucursal"));%>
                                        <option value="<%=sucursal.getIdsucursal()%>" <%=s!=null?(sucursal.getIdsucursal().equals(s.getIdsucursal())?"SELECTED":""):""%>><%=sucursal.getNombre()%></option>
                                    <%}%>
                                </select>
                            </div>      
                        </div>
                    </div>
                </form>                
            </div>
        </div>
        <!-- /.panel -->
    </div>
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                Detalles de la Venta
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body" id="detalle"></div>
            <div class="panel-footer">
                <div class="row">
                    <div class="col-lg-12">
                        <button class="btn btn-primary btn-sm" type="button" onclick="javascript:save('Venta/ventaNuevo.jsp','Venta/ventaListado.jsp','form1','page-wrapper');"><i class="fa fa-save fa-fw"></i> Guardar</button>
                        <button class="btn btn-default btn-sm" type="button" onclick="javascript:go2to('Venta/ventaListado.jsp','page-wrapper');"><i class="fa fa-backward fa-fw"></i> Volver</button>                 
                    </div>
                </div>
            </div>
            <!-- /.panel-footer -->
        </div>
        <!-- /.panel -->
    </div>
</div>

<div class="modal fade" id="myModalNewCliente">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Datos del Cliente</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-9">
                        <div class="form-group">
                            <label for="cliente_nombre">Nombre: (* Requerido)</label>
                            <input class="form-control input-sm" id="cliente_nombre" name="cliente_nombre" type="text" maxlength="45" autocomplete="off"/>
                        </div>      
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label for="cliente_telefono">Telefono: (* Requerido)</label>
                            <input class="form-control input-sm" id="cliente_telefono" name="cliente_telefono" type="number" min="0" max="9999999999"/>
                        </div>  
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-9">
                        <div class="form-group">
                            <label for="cliente_razon">Razon Social: (* Requerido)</label>
                            <input class="form-control input-sm" id="cliente_razon" name="cliente_razon" type="text" maxlength="80"/>
                        </div>      
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label for="cliente_rfc">R.F.C.: (* Requerido)</label>
                            <input class="form-control input-sm" id="cliente_rfc" name="cliente_rfc" type="text" maxlength="80"/>
                        </div>      
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label for="cliente_direccion">Direccion: (* Requerido)</label>
                            <input class="form-control input-sm" id="cliente_direccion" name="cliente_direccion" type="text" maxlength="80"/>
                        </div>      
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secundary btn-sm" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-success btn-sm" onclick="javascript:agregarCliente();">Agregar</button>
            </div>
        </div>
    </div>
</div> 

<script lang="javascript">
    jQuery.datetimepicker.setLocale('es');
    jQuery(document).ready(function () {
        'use strict';
        jQuery('#fventa').datetimepicker({
            format:'Y-m-d H:i:s',
            use24hours: true,
            lang:'es'
        });
        $("#cliente").chosen({no_results_text: "No se encuentran resultados para: ",width: "100%"}); 
    });
    
    function agregarCliente(){
        var cliente_nombre = encodeURIComponent(document.getElementById("cliente_nombre").value);
        var cliente_telefono = encodeURIComponent(document.getElementById("cliente_telefono").value);
        var cliente_razon = encodeURIComponent(document.getElementById("cliente_razon").value);
        var cliente_rfc = encodeURIComponent(document.getElementById("cliente_rfc").value);
        var cliente_direccion = encodeURIComponent(document.getElementById("cliente_direccion").value);
        if(cliente_nombre !== "" && cliente_telefono !== "" && cliente_razon !== "" && cliente_rfc !== "" && cliente_direccion !== ""){
            $('#myModalNewCliente').modal('hide');
            setTimeout(function(){
                var params = "nombre="+cliente_nombre+"&telefono="+cliente_telefono+"&razon="+cliente_razon+"&rfc="+cliente_rfc+"&direccion="+cliente_direccion;
                processDIV("/TPos/Cliente/agregar.jsp",params,"/TPos/Venta/ventaNuevo.jsp","","page-wrapper");
            },250);
        }
    }
    
    function setIva(){
        if(document.getElementById("sucursal_idsucursal").selectedIndex !== -1){
            var idsucursal = document.getElementById("sucursal_idsucursal").options[document.getElementById("sucursal_idsucursal").selectedIndex].value;
            var params = "idsucursal="+idsucursal;
            processDIV("/TPos/Venta/impuesto.jsp",params,"/TPos/Detalleventa/detalleventa.jsp","","detalle");
        }
    }
    
    setIva();
</script>

<%
facade.close();
pfacade.close();
afacade.close();
ifacade.close();
dvfacade.close();
profacade.close();
%>