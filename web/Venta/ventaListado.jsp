<%-- 
    Document   : usuarioListado
    Created on : Jan 9, 2016, 4:50:18 PM
    Author     : czara
--%>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Ventas - Listado de Ventas</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                Listado de Ventas
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-condensed table-hover" id="dataTablesVenta">
                        <thead>
                            <th class="col-lg-1">Folio</th>
                            <th class="col-lg-2">[B] Fecha de Venta</th>
                            <th class="col-lg-4">[B] Cliente</th>
                            <th class="col-lg-3">[B] Usuario</th>
                            <th class="col-lg-2">Estado</th>
                        </thead>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
            <div class="panel-footer">
                <div class="row">
                    <div class="col-lg-12">
                        <button class="btn btn-primary btn-sm" type="button" onclick="javascript:go2to('Venta/ventaNuevo.jsp','page-wrapper');"><i class="fa fa-asterisk fa-fw"></i> Nuevo</button>
                    </div>
                </div>
            </div>
            <!-- /.panel-footer -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<script>
    $(document).ready(function() {
        $('#dataTablesVenta').dataTable({
            "retrieve": true,
            "processing": true,  
            "serverSide": true,
            "ajax": "Venta/processVenta.jsp",
            "order": [[ 0, "desc" ]],
            "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
            }
        });
    });
</script>