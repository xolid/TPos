<%-- 
    Document   : impuesto
    Created on : 26-may-2018, 23:54:35
    Author     : solid
--%>

<jsp:useBean id="facade" scope="page" class="facades.VentaFacade"/>
<jsp:useBean id="pfacade" scope="page" class="facades.ProductoFacade"/>
<jsp:useBean id="dfacade" scope="page" class="facades.DetalleventaFacade"/>
<jsp:useBean id="ifacade" scope="page" class="facades.InventariosucursalFacade"/>

<%
int opcion = -1;
if(request.getParameter("opcion")!=null){
    opcion = Integer.parseInt(String.valueOf(request.getParameter("opcion")));
}
int idventa = 0;
if(request.getParameter("idventa")!=null){
    idventa = Integer.parseInt(String.valueOf(request.getParameter("idventa")));
}
String mensaje = "";
String tipo = "";
beans.Venta venta = facade.getVentaByID(idventa);
if(venta!=null){
    mensaje += "<br/>";
    //aplicar
    if(opcion==0){
        tipo = "info";
        if(venta.getEstado()==1){
            for(beans.Detalleventa detalleventa:dfacade.getDetalleventaByVenta(venta.getIdventa())){
                beans.Producto producto = pfacade.getProductoByNombre(detalleventa.getProducto());
                if(producto!=null){
                    if(producto.getTipo()==0){
                        beans.Inventariosucursal inventario = ifacade.getInventariosucursalBySucursalANDProducto(venta.getSucursal().getIdsucursal(),producto.getIdproducto());
                        if(inventario!=null){
                            inventario.setCantidad(inventario.getCantidad()-detalleventa.getCantidad());
                            ifacade.updateInventariosucursal(inventario);
                            mensaje += "["+producto.getNombre()+"] se retira de sucursal ["+venta.getSucursal().getNombre()+"] actualizando a ["+inventario.getCantidad()+"] unidades.<br/>";
                        }else{
                            inventario = new beans.Inventariosucursal(producto, venta.getSucursal(), -detalleventa.getCantidad(), detalleventa.getCantidad(),detalleventa.getCantidad());
                            ifacade.saveInventariosucursal(inventario);
                            mensaje += "["+producto.getNombre()+"] no existe en sucursal ["+venta.getSucursal().getNombre()+"] pero se agrega actualizando a ["+inventario.getCantidad()+"] unidades.<br/>";
                        }
                    }else{
                        mensaje += "["+producto.getNombre()+"] no representa un decremento numerico al tratarse de un servicio.<br/>";
                    }
                }else{
                    mensaje += "["+detalleventa.getProducto()+"] no existe en la base de datos por lo que no se aplica.<br/>";
                }
            }
        }else{
            mensaje += "No se aplican cambios en el inventario al no ser una venta pendiente.<br/>";
        }
        venta.setEstado(0);
        facade.saveVenta(venta);
    }
    //cancelar
    if(opcion==1){
        tipo = "danger";
        if(venta.getEstado()==0){
            for(beans.Detalleventa detalleventa:dfacade.getDetalleventaByVenta(venta.getIdventa())){
                beans.Producto producto = pfacade.getProductoByNombre(detalleventa.getProducto());
                if(producto!=null){
                    if(producto.getTipo()==0){
                        beans.Inventariosucursal inventario = ifacade.getInventariosucursalBySucursalANDProducto(venta.getSucursal().getIdsucursal(),producto.getIdproducto());
                        if(inventario!=null){
                            inventario.setCantidad(inventario.getCantidad()+detalleventa.getCantidad());
                            ifacade.updateInventariosucursal(inventario);
                            mensaje += "["+producto.getNombre()+"] se agrega a sucursal ["+venta.getSucursal().getNombre()+"] actualizando a ["+(inventario.getCantidad())+"] unidades.<br/>";
                        }else{
                            inventario = new beans.Inventariosucursal(producto, venta.getSucursal(), detalleventa.getCantidad(), detalleventa.getCantidad(),detalleventa.getCantidad());
                            ifacade.saveInventariosucursal(inventario);
                            mensaje += "["+producto.getNombre()+"] no existe en sucursal ["+venta.getSucursal().getNombre()+"] pero causa numeros positivos actualizando a ["+inventario.getCantidad()+"] unidades.<br/>";
                        }
                    }else{
                        mensaje += "["+producto.getNombre()+"] no representa un incremento numerico al tratarse de un servicio.<br/>";
                    }
                }else{
                    mensaje += "["+detalleventa.getProducto()+"] no existe en la base de datos por lo que no se aplica.<br/>";
                }
            }
        }else{
            mensaje += "No se aplican cambios en el inventario al no se una venta aplicada.<br/>";
        }
        venta.setEstado(2);
        facade.saveVenta(venta);
    }
}

out.print(mensaje+"|"+tipo);

facade.close();
pfacade.close();
dfacade.close();
ifacade.close();
%>