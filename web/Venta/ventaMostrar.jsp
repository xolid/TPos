<%-- 
    Document   : venta
    Created on : Jan 9, 2016, 7:42:35 PM
    Author     : czara
--%>
<%-- 
    Document   : venta
    Created on : Jan 9, 2016, 7:42:35 PM
    Author     : czara
--%>

<jsp:useBean id="venta" class="beans.Venta" scope="page"/>
<jsp:setProperty name="venta" property="idventa" />
<jsp:setProperty name="venta" property="estado" />
<jsp:useBean id="facade" scope="page" class="facades.VentaFacade"/>
<jsp:useBean id="detfacade" scope="page" class="facades.DetalleventaFacade"/>


<%if(request.getParameter("id")!=null){
    int idventa=Integer.parseInt((String)request.getParameter("id"));
    venta=facade.getVentaByID(idventa);
}
java.util.Locale mexico = new java.util.Locale("es", "MX");
java.text.NumberFormat formatoImporte = java.text.NumberFormat.getCurrencyInstance(mexico);
%>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Venta - Definicion de la Venta</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                Definicion de la Venta
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <form ventae="form" id="form1" name="form1" action="venta.jsp" method="post">
                    <input type="hidden" id="idventa" name="idventa" value="<%=venta.getIdventa()%>"/>
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label for="fventa">Fecha: </label>
                                <input class="form-control input-sm" id="fventa" name="fventa" type="text" value="<%=new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(venta.getFventa())%>" readonly/>
                            </div>      
                        </div>
                        <div class="col-lg-3 col-lg-offset-6">
                            <div class="form-group">
                                <label for="estado">Estado: </label>
                                <select id="estado" name="estado" class="form-control input-sm" disabled="true">
                                    <option value="<%=venta.getEstado()%>"><%=new tools.Estado().getNombre(venta.getEstado())%></option>
                                </select>
                            </div>  
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="cliente_idcliente">Cliente: </label>
                                <select id="cliente_idcliente" name="cliente_idcliente" class="form-control input-sm" disabled="true">
                                    <option value="<%=venta.getCliente()%>"><%=venta.getCliente()%></option>
                                </select>
                            </div>      
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="usuario_idusuario">Usuario: </label>
                                <select id="usuario_idusuario" name="usuario_idusuario" class="form-control input-sm" disabled="true">
                                    <option value="<%=venta.getUsuario()%>"><%=venta.getUsuario()%></option>
                                </select>
                            </div>      
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="sucursal_idsucursal">Sucursal de Salida: </label>
                                <select id="sucursal_idsucursal" name="sucursal_idsucursal" class="form-control input-sm" disabled="true">
                                    <option value="<%=venta.getSucursal().getIdsucursal()%>"><%=venta.getSucursal().getNombre()%></option>
                                </select>
                            </div>      
                        </div>
                    </div>
                </form>                
            </div>
        </div>
        <!-- /.panel -->
    </div>
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                Detalles de la Venta
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body" id="detalle">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-condensed table-hover" id="dataTablesDetalleVenta">
                        <thead>
                            <th class="col-lg-1 text-center">#</th>
                            <th class="col-lg-2">Producto</th>
                            <th class="col-lg-1">Marca</th>
                            <th class="col-lg-1">Unidad</th>
                            <th class="col-lg-1">Categoria</th>
                            <th class="col-lg-1 text-center">Cantidad</th>
                            <th class="col-lg-1 text-center">Precio</th>
                            <th class="col-lg-1 text-center">Subtotal</th>
                            <th class="col-lg-1 text-center">I.V.A.</th>
                            <th class="col-lg-1 text-center">Total</th>
                        </thead>
                        <tbody>
                            <%int consecutivo = 0;
                            int total_articulos = 0;
                            java.math.BigDecimal total_venta = new java.math.BigDecimal(0.0);
                            for(beans.Detalleventa detalleventa:detfacade.getDetalleventaByVenta(venta.getIdventa())){
                                double subtotal = detalleventa.getPrecio().doubleValue()*detalleventa.getCantidad();
                                double iva = 0.0;
                                if(detalleventa.getImpuesto()!=0.0){
                                    iva = (detalleventa.getImpuesto()*subtotal)/100;
                                }
                                double total = subtotal+iva;
                                %>
                                <tr>
                                    <td class="text-center"><%=(consecutivo+1)%></td>
                                    <td><%=detalleventa.getProducto()%></td>
                                    <td><%=detalleventa.getMarca()%></td>
                                    <td><%=detalleventa.getUnidad()%></td>
                                    <td><%=detalleventa.getCategoria()%></td>
                                    <td class="text-center"><%=detalleventa.getCantidad()%></td>
                                    <td class="text-center"><%=formatoImporte.format(detalleventa.getPrecio())%></td>
                                    <td class="text-center"><%=formatoImporte.format(subtotal)%></td>
                                    <td class="text-center"><%=formatoImporte.format(iva)%></td>
                                    <td class="text-center"><%=formatoImporte.format(total)%></td>
                                </tr>
                                <%consecutivo++;
                                total_articulos += detalleventa.getCantidad();
                                total_venta = total_venta.add(new java.math.BigDecimal(total));
                            }%>
                        </tbody>
                        <tfoot>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td class="text-center"><b><%=total_articulos%></b></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td class="text-center"><b><%=formatoImporte.format(total_venta)%></b></td>
                        </tfoot>
                    </table>
                </div>
            </div>
            <div class="panel-footer">
                <div class="row">
                    <div class="col-lg-12">
                        <%if(venta.getEstado()==1){%>
                            <button class="btn btn-primary btn-sm" type="button" onclick="javascript:actualizar(0);"><i class="fa fa-tasks fa-fw"></i> Aplicar</button>
                        <%}if(venta.getEstado()!=2){%>
                            <button class="btn btn-danger btn-sm" type="button" onclick="javascript:actualizar(1);"><i class="fa fa-eraser fa-fw"></i> Cancelar</button>
                        <%}%>
                        <button class="btn btn-default btn-sm" type="button" onclick="javascript:go2to('Venta/ventaListado.jsp','page-wrapper');"><i class="fa fa-backward fa-fw"></i> Volver</button>                 
                    </div>
                </div>
            </div>
            <!-- /.panel-footer -->
        </div>
        <!-- /.panel -->
    </div>
</div>
<!-- /.row -->
<script lang="javascript">
    
    function actualizar(opcion){
        BootstrapDialog.confirm({
            title: "Mensaje del Sistema",
            message: "�Confirma desea "+(opcion===0?"APLICAR":"CANCELAR")+" la venta?",
            type: 'type-warning',
            closable: false,
            btnOKClass: 'btn-warning',
            callback: function(result) {
                if(result) {
                    var idventa = document.getElementById('idventa').value;
                    var params = "opcion="+opcion+"&idventa="+idventa;
                    var params2 = "id="+idventa;
                    executeEXEC("/TPos/Venta/actualizar.jsp",params,"loadPanel('/TPos/Venta/ventaMostrar.jsp','"+params2+"','page-wrapper')");
                }
            }
        });
    }
</script>

<%
facade.close();
detfacade.close();
%>