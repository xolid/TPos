<%-- 
    Document   : impuesto
    Created on : 26-may-2018, 23:54:35
    Author     : solid
--%>

<jsp:useBean id="sfacade" scope="page" class="facades.SucursalFacade"/>

<%
int idsucursal = 0;
if(request.getParameter("idsucursal")!=null){
    idsucursal = Integer.parseInt(String.valueOf(request.getParameter("idsucursal")));
}
session.removeAttribute("impuesto");
beans.Sucursal sucursal= sfacade.getSucursalByID(idsucursal);
if(sucursal!=null){
    session.setAttribute("impuesto",sucursal.getIva().getPorcentaje());
}

sfacade.close();
%>