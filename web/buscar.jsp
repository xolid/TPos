<%-- 
    Document   : buscar
    Created on : Jan 27, 2016, 6:09:20 PM
    Author     : czara
--%>

<html>
    <head>
        <title>Herramientas de Busqueda</title>
        <!-- Core CSS - Include with every page -->
        <link href="<%=request.getContextPath()%>/css/bootstrap.css" rel="stylesheet"/>
        <link href="<%=request.getContextPath()%>/font-awesome/css/font-awesome.css" rel="stylesheet"/>
        <link href="<%=request.getContextPath()%>/css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet"/>
        <link href="<%=request.getContextPath()%>/css/bootstrap-dialog/bootstrap-dialog.css" rel="stylesheet" type="text/css"/>  
    </head>
    <body>
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-condensed table-hover" id="dataTablesBuscar" style="width:100%">
                        <thead>
                            <th class="col-lg-1">[B] Codigo</th>
                            <th class="col-lg-3">[B] Nombre</th>
                            <th class="col-lg-2">[B] Clave</th>
                            <th class="col-lg-2">[B] Unidad</th>
                            <th class="col-lg-2">[B] Categoria</th>
                            <th class="col-lg-1">Tipo</th>
                            <th class="col-lg-1">Precio</th>
                        </thead>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
        </div>
        <!-- Core Scripts - Include with every page -->
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-1.10.2.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/bootstrap.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/plugins/metisMenu/jquery.metisMenu.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/plugins/dataTables/jquery.dataTables.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/plugins/dataTables/dataTables.bootstrap.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/bootstrap-dialog/bootstrap-dialog.js"></script>
        <script lang="javascript">
            
            $(document).ready(function() {
                $('#dataTablesBuscar').dataTable({
                    "retrieve": true,
                    "processing": true,  
                    "serverSide": true,
                    "ajax": "process.jsp",
                    "language": {
                        "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
                    }
                });
            });
            
            function abrir(idproducto,codigo){
                BootstrapDialog.show({
                    title: "Consulta en Inventario CODIGO [ "+codigo+" ]",
                    message: $('<div></div>').load('disponibilidad.jsp?idproducto='+idproducto),
                    type: 'type-info'
                });
            }
            
        </script>
    </body>
</html>