<%-- 
    Document   : error1
    Created on : Feb 9, 2016, 12:56:58 AM
    Author     : czara
--%>

<html>
    <head>
        <meta charset="ISO-8859-1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>PManager | Reporte de Proyectos</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<%=request.getContextPath()%>/font-awesome/css/font-awesome.css"/>
        <!-- Theme style -->
        <link rel="stylesheet" href="<%=request.getContextPath()%>/css/sb-admin.css"/>
        <link rel="shortcut icon" href="<%=request.getContextPath()%>/images/favicon.ico"/>
    </head>
    <body>
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Evento en el sistema</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item active"><%=new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())%></li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-warning alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h5><i class="icon fa fa-warning"></i> Error...</h5>
                            El elemento buscado no se encuentra en el sistema. <a href="<%=request.getContextPath()%>/principal.jsp">IR A PRINCIPAL</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </body>
</html>