<%-- 
    Document   : rol
    Created on : Jan 9, 2016, 7:42:35 PM
    Author     : czara
--%>

<jsp:useBean id="rol" class="beans.Rol" scope="page"/>
<jsp:setProperty name="rol" property="*" />
<jsp:useBean id="facade" scope="page" class="facades.RolFacade"/>

<%if(request.getParameter("accion")!=null){
    if(request.getParameter("accion").equals("1")){
        if(rol.getIdrol()==0){
            facade.saveRol(rol);
        }else{
            facade.updateRol(rol);
        }
    }
    if(request.getParameter("accion").equals("2")){
        facade.deleteRol(rol);        
    }
}
if(request.getParameter("id")!=null){
    int idrol=Integer.parseInt((String)request.getParameter("id"));
    rol=facade.getRolByID(idrol);
}else{
    rol.setIdrol(0);
    rol.setNombre("");
    rol.setPunto(0);
    rol.setVende(0);
    rol.setCompra(0);
    rol.setInventario(0);
    rol.setReporte(0);
    rol.setCatalogo(0);
    rol.setAdministra(0);
}%>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Catalogos del Sistema - Rol</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                Definicion del Rol
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <form role="form" id="form1" name="form1" action="rol.jsp" method="post">
                    <input type="hidden" id="idrol" name="idrol" value="<%=rol.getIdrol()%>"/>
                    <div class="row">
                        <div class="col-lg-8">
                            <div class="form-group">
                                <label for="nombre">Nombre: </label>
                                <input class="form-control input-sm" id="nombre" name="nombre" type="text" maxlength="75" value="<%=rol.getNombre()%>" autocomplete="off" required autofocus/>
                            </div>      
                        </div>
                        <div class="col-lg-2">
                            <div class="form-group">
                                <label for="administra">Administra Sistema: </label>
                                <select id="administra" name="administra" class="form-control input-sm">
                                    <option value="0" <%=rol.getAdministra()==0?"SELECTED":""%>>REVOCADO</option>
                                    <option value="1" <%=rol.getAdministra()==1?"SELECTED":""%>>ASIGNADO</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="form-group">
                                <label for="catalogo">Catalogos del Sistema: </label>
                                <select id="catalogo" name="catalogo" class="form-control input-sm">
                                    <option value="0" <%=rol.getCatalogo()==0?"SELECTED":""%>>REVOCADO</option>
                                    <option value="1" <%=rol.getCatalogo()==1?"SELECTED":""%>>ASIGNADO</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="punto">Punto de Venta: </label>
                                <select id="punto" name="punto" class="form-control input-sm">
                                    <option value="0" <%=rol.getPunto()==0?"SELECTED":""%>>REVOCADO</option>
                                    <option value="1" <%=rol.getPunto()==1?"SELECTED":""%>>ASIGNADO</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="form-group">
                                <label for="vende">Administra Ventas: </label>
                                <select id="vende" name="vende" class="form-control input-sm">
                                    <option value="0" <%=rol.getVende()==0?"SELECTED":""%>>REVOCADO</option>
                                    <option value="1" <%=rol.getVende()==1?"SELECTED":""%>>ASIGNADO</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="form-group">
                                <label for="compra">Administra Compras: </label>
                                <select id="compra" name="compra" class="form-control input-sm">
                                    <option value="0" <%=rol.getCompra()==0?"SELECTED":""%>>REVOCADO</option>
                                    <option value="1" <%=rol.getCompra()==1?"SELECTED":""%>>ASIGNADO</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="form-group">
                                <label for="inventario">Administra Inventario: </label>
                                <select id="inventario" name="inventario" class="form-control input-sm">
                                    <option value="0" <%=rol.getInventario()==0?"SELECTED":""%>>REVOCADO</option>
                                    <option value="1" <%=rol.getInventario()==1?"SELECTED":""%>>ASIGNADO</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="form-group">
                                <label for="reporte">Genera Reportes: </label>
                                <select id="reporte" name="reporte" class="form-control input-sm">
                                    <option value="0" <%=rol.getReporte()==0?"SELECTED":""%>>REVOCADO</option>
                                    <option value="1" <%=rol.getReporte()==1?"SELECTED":""%>>ASIGNADO</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </form>                
            </div>
            <div class="panel-footer">
                <div class="row">
                    <div class="col-lg-12">
                        <button class="btn btn-primary btn-sm" type="button" onclick="javascript:save('Rol/rol.jsp','Rol/rolListado.jsp','form1','page-wrapper');"><i class="fa fa-save fa-fw"></i> Guardar</button>
                        <%if(rol.getIdrol()!=0){%>
                            <button class="btn btn-danger btn-sm" type="button" onclick="javascript:erase('Rol/rol.jsp','Rol/rolListado.jsp','form1','page-wrapper');"><i class="fa fa-eraser fa-fw"></i> Borrar</button>
                        <%}%>
                        <button class="btn btn-default btn-sm" type="button" onclick="javascript:go2to('Rol/rolListado.jsp','page-wrapper');"><i class="fa fa-backward fa-fw"></i> Volver</button>                 
                    </div>
                </div>
            </div>
            <!-- /.panel-footer -->
        </div>
        <!-- /.panel -->
    </div>
</div>
<!-- /.row -->

<%
facade.close();
%>