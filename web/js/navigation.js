/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/* global BootstrapDialog */
var contexto = "/TPos";

function save(url, urlRedirect, formName, div) {
    var str = validInputs(formName);
    if (str === "") {
        BootstrapDialog.confirm({
            title: "Mensaje del Sistema",
            message: "Confirma desea salvar?",
            type: 'type-warning',
            closable: false,
            btnOKClass: 'btn-warning',
            callback: function(result) {
                if(result) {
                    var params = 'accion=1&' + inputsToParams(formName);
                    $.ajax({
                        type:"POST",
                        url: url,
                        data: params,
                        success:function(){
                            $('#'+div).html("<div class='text-center'><img src='"+contexto+"/images/loading2.gif'/></div>").load(urlRedirect);
                        },
                        beforeSend: function(){
                            $('#'+div).html("<div class='text-center'><img src='"+contexto+"/images/loading2.gif'/></div>");
                        },
                        error: function() {
                            $('#'+div).html("<div class='text-center'><img src='"+contexto+"/images/loading2.gif'/></div>").load(contexto+"/Errors/error.jsp");
                        }
                    });
                }
            }
        });
    } else {
        BootstrapDialog.alert({
            title: "Mensaje del Sistema",
            message: "<strong>Se encontraron los siguientes errores:</strong><br/><br/>" + str,
            type: 'type-danger',
            closable: false,
            btnOKClass: 'btn-danger'
        });
    }
}

function erase(url, urlRedirect, formName, div) {
    BootstrapDialog.confirm({
        title: "Mensaje del Sistema",
        message: "Confirma desea borrar?",
        type: 'type-warning',
        closable: false,
        btnOKClass: 'btn-warning',
        callback: function(result) {
            if(result) {
                var params = 'accion=2&' + inputsToParams(formName);
                $.ajax({
                    type:"POST",
                    url: url,
                    data: params,
                    success:function(){
                        $('#'+div).html("<div class='text-center'><img src='"+contexto+"/images/loading2.gif'/></div>").load(urlRedirect);
                    },
                    beforeSend: function(){
                        $('#'+div).html("<div class='text-center'><img src='"+contexto+"/images/loading2.gif'/></div>");
                    },
                    error: function() {
                        $('#'+div).html("<div class='text-center'><img src='"+contexto+"/images/loading2.gif'/></div>").load(contexto+"/Errors/error.jsp");
                    }
                });
            }
        }
    });
}

function close(strDIV) {
    document.getElementById(strDIV).innerHTML = '';
}

function clean(formName) {
    restartInputs(formName);
}

function go2to(url, div) {
    $('#'+div).html("<div class='text-center'><img src='"+contexto+"/images/loading2.gif'/></div>").load(url, function(responseTxt, statusTxt, xhr){
        if(statusTxt === "error"){
            $('#'+div).html(responseTxt);
        }
    });
}

function go2toPAR(url, params, div) {
    $('#'+div).html("<div class='text-center'><img src='"+contexto+"/images/loading2.gif'/></div>").load(url,params, function(responseTxt, statusTxt, xhr){
        if(statusTxt === "error"){
            $('#'+div).html(responseTxt);
        }
    });
}

function loadPanel(url, params, div) {
    $('#'+div).html("<div class='text-center'><img src='"+contexto+"/images/loading.gif'/></div>").load(url,params, function(responseTxt, statusTxt, xhr){
        if(statusTxt === "error"){
            $('#'+div).html(responseTxt);
        }
    });
}

function loadPanelEXEC(url, params, div, fun) {
    $('#'+div).html("<div class='text-center'><img src='"+contexto+"/images/loading.gif'/></div>").load(url,params,function(responseTxt, statusTxt, xhr){
        if(statusTxt === "success"){
            window.setTimeout(fun, 1);
        }
        if(statusTxt === "error"){
            $('#'+div).html(responseTxt);
        }
    });
}

function go2toEXEC(urlTarget, paramsTarget, divReturn, fun) {
    $.ajax({
        type:"POST",
        url: urlTarget,
        data: paramsTarget,
        success: function() {  
            window.setTimeout(fun, 1);
        },
        beforeSend: function(){
            $('#'+divReturn).html("<div class='text-center'><img src='"+contexto+"/images/loading.gif'/></div>");
        },
        error: function() {
            $('#'+divReturn).html("<div class='text-center'><img src='"+contexto+"/images/loading2.gif'/></div>").load(contexto+"/Errors/error.jsp");
        }
    });
}

function processDIV(urlTarget,paramsTarget,urlReturn,paramsReturn,divReturn) {
    $.ajax({
        type:"POST",
        url: urlTarget,
        data: paramsTarget,
        success: function() {  
            loadPanel(urlReturn,paramsReturn,divReturn);
        },
        beforeSend: function(){
            $('#'+divReturn).html("<div class='text-center'><img src='"+contexto+"/images/loading.gif'/></div>");
        },
        error: function() {
            $('#'+divReturn).html("<div class='text-center'><img src='"+contexto+"/images/loading2.gif'/></div>").load(contexto+"/Errors/error.jsp");
        }
    });
}

function processDIVandEXEC(urlTarget,paramsTarget,urlReturn,paramsReturn,divReturn,strFUN) {
    $.ajax({
        type:"POST",
        url: urlTarget,
        data: paramsTarget,
        success: function() {  
            loadPanelEXEC(urlReturn, paramsReturn, divReturn, strFUN);
        },
        beforeSend: function(){
            $('#'+divReturn).html("<div class='text-center'><img src='"+contexto+"/images/loading.gif'/></div>");
        },
        error: function() {
            $('#'+divReturn).html("<div class='text-center'><img src='"+contexto+"/images/loading2.gif'/></div>").load(contexto+"/Errors/error.jsp");
        }
    });
}

function processEXEC(urlTarget,params,fun) {
    $.ajax({ 
        type:"POST",
        url: urlTarget,
        data: params,
        success: function() {  
            window.setTimeout(fun, 1);
        }
    });
}

function execute(urlTarget,params) {
    $.ajax({ 
        type:"POST",
        url: urlTarget,
        data: params,
        success: function(text) { 
            var cadenas = text.split("|");
            $.notify({title: '<strong>Notificacion TPos:</strong> ', message: cadenas[0]},{type: cadenas[1]});
        } 
    });
}

function executeONLY(urlTarget,params) {
    $.ajax({ 
        type:"POST",
        url: urlTarget,
        data: params,
        success: function(text) { 
            $.notify({title: '<strong>Notificacion TPos:</strong> ', message: "Actualizacion terminada"},{type: "info"});
        } 
    });
}

function executeEXEC(urlTarget,params,fun) {
    $.ajax({ 
        type:"POST",
        url: urlTarget,
        data: params,
        success: function(text) { 
            var cadenas = text.split("|");
            $.notify({title: '<strong>Notificacion TPos:</strong> ',message: cadenas[0]},{type: cadenas[1]});
            window.setTimeout(fun, 1);
        } 
    });
}