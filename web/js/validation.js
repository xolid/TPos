/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function restartInputs(formName) {
    var elem = document.getElementById(formName).elements;
    for (var i = 0; i < elem.length; i++) {
        var inputTagName = elem[i].tagName;
        if (inputTagName === 'INPUT') {
            document.getElementById(elem[i].id).value = "";
        }
        if (inputTagName === 'SELECT') {
            document.getElementById(elem[i].id);
        }
        if (inputTagName === 'TEXTAREA') {
            document.getElementById(elem[i].id).value = "";
        }
    }
}

function inputsToParams(formName) {
    var string = '';
    var elem = document.getElementById(formName).elements;
    for (var i = 0; i < elem.length; i++) {
        var inputTagName = elem[i].tagName;
        if (inputTagName === 'INPUT' || inputTagName === 'SELECT' || inputTagName === 'TEXTAREA') {
            var inputName = elem[i].name;
            var inputValue = '';
            switch (elem[i].type) {
                case 'file':
                    inputValue = elem[i].value.replace("C:\\fakepath\\", "");
                    break;
                case 'select-multiple':
                    inputValue = getSelectedItems(elem[i]);
                    break;
                case 'datetime-local':
                    inputValue = elem[i].value.replace("T", " ");
                    break;
                case 'radio':
                    inputValue = getRadioCheckedValue(formName,inputName);
                    break;
                default:
                    inputValue = elem[i].value;
                    break;
            }
            if(inputName !== ""){
                if (string === "") {
                    string = inputName + "=" + inputValue;
                } else {
                    string = string + "&" + inputName + "=" + inputValue;
                }
            }
        }
    }
    return string;
}

function getRadioCheckedValue(formName,radio){
    var oRadio = document.getElementById(formName).elements[radio];
    for(var i = 0; i < oRadio.length; i++){
        if(oRadio[i].checked){
            return oRadio[i].value;
        }
    }
   return '';
}

function validInputs(formName) {
    var string = '';
    var elem = document.getElementById(formName).elements;
    for (var i = 0; i < elem.length; i++) {
        var inputTagName = elem[i].tagName;
        if (inputTagName === 'INPUT') {
            if (!elem[i].validity.valid) {
                string += "* " + elem[i].name + " no valido.<br/>";
            }
        }
        if (inputTagName === 'SELECT') {
            if (elem[i].type === 'select-multiple') {
                if (getSelectedItemsNumber(elem[i]) <= 0 && elem[i].required) {
                    string += "* " + elem[i].name + " debe seleccionar almenos un elemento.<br/>";
                }
            } else {
                if (elem[i].options.length === 0) {
                    string += "* " + elem[i].name + " no contiene elementos.<br/>";
                }
            }
        }
        if (inputTagName === 'TEXTAREA') {
            if (elem[i].value === "" && elem[i].required) {
                string += "* " + elem[i].name + " esta vacio.<br/>";
            }
        }
    }
    return string;
}

function getSelectedItems(element) {
    var selectedList = "";
    for (var i = 0; i < element.options.length; i++) {
        if (element.options[i].selected) {
            if (selectedList === "") {
                selectedList += element.options[i].value;
            } else {
                selectedList += "," + element.options[i].value;
            }
        }
    }
    return selectedList;
}

function getSelectedItemsNumber(element) {
    var number = 0;
    for (var i = 0; i < element.options.length; i++) {
        if (element.options[i].selected) {
            number++;
        }
    }
    return number;
}

function getFileName(request){
    var filename = "";
    var disposition = request.getResponseHeader('Content-Disposition');
    if (disposition && disposition.indexOf('inline') !== -1) {
        var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
        var matches = filenameRegex.exec(disposition);
        if (matches !== null && matches[1]) { 
          filename = matches[1].replace(/['"]/g, '');
        }
    }
    return filename;
}