<%-- 
    Document   : cambiar
    Created on : Jan 16, 2016, 11:49:25 AM
    Author     : czara
--%>

<jsp:useBean id="usuario" class="beans.Usuario" scope="page"/>
<jsp:setProperty name="usuario" property="*" />
<jsp:useBean id="facade" scope="page" class="facades.UsuarioFacade"/>

<%
String mensaje = "";
int confirmacion = 0;
if(request.getParameter("accion")!=null){
    if(request.getParameter("accion").equals("1")){
        if(request.getParameter("pass0")!=null&&request.getParameter("pass1")!=null&&request.getParameter("pass2")!=null){
            String pass0=tools.MD5.getMD5(request.getParameter("pass0"));
            String pass1=tools.MD5.getMD5(request.getParameter("pass1"));
            String pass2=tools.MD5.getMD5(request.getParameter("pass2"));
            if(!pass0.equals("null")&&!pass1.equals("null")&&!pass2.equals("null")){
                if(usuario.getPass().equals(pass0)){
                    if(pass1.equals(pass2)){
                        beans.Usuario u = facade.getUsuarioByID(usuario.getIdusuario());
                        u.setPass(pass2);
                        facade.saveUsuario(u);
                        confirmacion = 1;
                        mensaje = "Password satisfactoriamente cambiado";
                    }else{
                        confirmacion = 2;
                        mensaje = "Password nuevo no coincide";
                    }
                }else{
                    confirmacion = 2;
                    mensaje = "Password actual incorrecto";
                }
            }
        }
    }
}
if(session.getAttribute("usuario")!=null){
    beans.Usuario u = (beans.Usuario)session.getAttribute("usuario");
    usuario = facade.getUsuarioByID(u.getIdusuario());
}%>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Perfil de Usuario</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                Cambiar password
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <form role="form" id="form1" name="form1" action="cambiar.jsp" method="post">
                    <input type="hidden" id="idusuario" name="idusuario" value="<%=usuario.getIdusuario()%>"/>
                    <input type="hidden" id="pass" name="pass" value="<%=usuario.getPass()%>"/>
                    <%if(confirmacion != 0){
                        if(confirmacion == 1){%>
                            <div class="alert alert-success alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <b>Mensaje:</b> <%=mensaje%>
                            </div>
                        <%}
                        if(confirmacion == 2){%>
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <b>Error:</b> <%=mensaje%> 
                            </div>
                        <%}%>
                    <%}%>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="nombre">Nombre: </label>
                                <input class="form-control input-sm" id="nombre" name="nombre" type="text" value="<%=usuario.getNombre()%>" disabled/>
                            </div>     
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="email">Email: </label>
                                <input class="form-control input-sm" id="email" name="email" type="email" value="<%=usuario.getEmail()%>" disabled/>
                            </div>     
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Password Actual: </label>
                                <input class="form-control input-sm" id="pass0" name="pass0" type="password" autocomplete="off" required autofocus/>
                            </div>  
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Password Nuevo: </label>
                                <input class="form-control input-sm" id="pass1" name="pass1" type="password" autocomplete="off" required/>
                            </div>  
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Reescribir Password: </label>
                                <input class="form-control input-sm" id="pass2" name="pass2" type="password" autocomplete="off" required/>     
                            </div>  
                        </div>
                    </div>
                </form>                
                <!-- /.form -->
            </div>
            <!-- /.panel-body -->
            <div class="panel-footer">
                <div class="row">
                    <div class="col-lg-12">
                        <button class="btn btn-danger btn-sm" type="button" onclick="javascript:saveNORedirect('cambiar.jsp','form1','page-wrapper');"><i class="fa fa-key fa-fw"></i> Cambiar</button>
                    </div>
                </div>
            </div>
            <!-- /.panel-footer -->
        </div>
        <!-- /.panel -->
    </div>
</div>

<%
facade.close();
%>