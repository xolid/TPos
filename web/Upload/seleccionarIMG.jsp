<%-- 
    Document   : seleccionar
    Created on : 29-may-2018, 2:25:36
    Author     : solid
--%>

<%
int id = 0;
if(request.getParameter("id")!=null){
    id = Integer.parseInt(String.valueOf(request.getParameter("id")));
}
String tipo = "";
if(request.getParameter("tipo")!=null){
    tipo = String.valueOf(request.getParameter("tipo"));
}
%>

<html>
    <head>
        <title>Herramientas de Carga</title>
        <!-- Core CSS - Include with every page -->
        <link href="<%=request.getContextPath()%>/css/bootstrap.css" rel="stylesheet">
        <link href="<%=request.getContextPath()%>/font-awesome/css/font-awesome.css" rel="stylesheet">
        <link href="<%=request.getContextPath()%>/css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">
    </head>
    <body>
        <form role="form" id="form1" name="form1" action="subir.jsp?id=<%=id%>&tipo=<%=tipo%>" enctype="MULTIPART/FORM-DATA" method="post">
            <div class="form-group">
                <label>Seleccionar Archivo:</label>
                <input type="file" name="file" class="filestyle" accept="image/*"/>
            </div>
            <div class="form-group text-center">
                 <input type="submit" value="Subir" class="btn btn-sm btn-success"/>
            </div>
        </form>
        <!-- Core Scripts - Include with every page -->
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-1.10.2.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/bootstrap.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/plugins/metisMenu/jquery.metisMenu.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/sb-admin.js"></script>   
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/bootstrap-filestyle/bootstrap-filestyle.min.js"> </script>
    </body>
</html>