<%-- 
    Document   : subir
    Created on : 29-may-2018, 1:18:22
    Author     : solid
--%>

<%@ page import="java.util.*" %>
<%@ page import="org.apache.commons.fileupload.*" %>
<%@ page import="org.apache.commons.fileupload.disk.*" %>
<%@ page import="org.apache.commons.fileupload.servlet.*" %>
<%@ page import="org.apache.commons.io.*" %>
<%@ page import="java.io.*" %>
<jsp:useBean id="pfacade" scope="page" class="facades.ProductoFacade"/>
<jsp:useBean id="cfacade" scope="page" class="facades.CategoriaFacade"/>
 
<%
int id = 0;
if(request.getParameter("id")!=null){
    id = Integer.parseInt(String.valueOf(request.getParameter("id")));
}
String tipo = "";
if(request.getParameter("tipo")!=null){
    tipo = String.valueOf(request.getParameter("tipo"));
}

/*FileItemFactory es una interfaz para crear FileItem*/
FileItemFactory file_factory = new DiskFileItemFactory();
/*ServletFileUpload esta clase convierte los input file a FileItem*/
ServletFileUpload servlet_up = new ServletFileUpload(file_factory);
servlet_up.setSizeMax(-1);
/*sacando los FileItem del ServletFileUpload en una lista */
List items = servlet_up.parseRequest(request);
for(int i=0;i<items.size();i++){
    /*FileItem representa un archivo en memoria que puede ser pasado al disco duro*/
    FileItem item = (FileItem) items.get(i);
    /*item.isFormField() false=input file; true=text field*/
    if (! item.isFormField()){
        /*guardado en el bean adjunto para despues preservarlo en base de datos*/
        switch (tipo){
            case "producto":
                beans.Producto producto = pfacade.getProductoByID(id);
                producto.setFoto(item.get());
                pfacade.updateProducto(producto);
                break;
            case "categoria":
                beans.Categoria categoria = cfacade.getCategoriaByID(id);
                categoria.setFoto(item.get());
                cfacade.updateCategoria(categoria);
                break;
        }
    }
}

cfacade.close();
pfacade.close();
%>

<script>
    window.parent.closeModal();
</script>