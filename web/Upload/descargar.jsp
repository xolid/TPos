<%-- 
    Document   : descargar
    Created on : 29-may-2018, 12:00:35
    Author     : solid
--%>

<jsp:useBean id="pfacade" scope="page" class="facades.ProductoFacade"/>
<jsp:useBean id="cfacade" scope="page" class="facades.CategoriaFacade"/>
 
<%
int id = 0;
if(request.getParameter("id")!=null){
    id = Integer.parseInt(String.valueOf(request.getParameter("id")));
}
String tipo = "";
if(request.getParameter("tipo")!=null){
    tipo = String.valueOf(request.getParameter("tipo"));
}

String fileName = "";
java.sql.Blob blob = null;
switch (tipo){
    case "producto":
        beans.Producto producto = pfacade.getProductoByID(id);
        fileName = producto.getNombre();
        blob = new javax.sql.rowset.serial.SerialBlob(producto.getFoto());
        break;
    case "categoria":
        beans.Categoria categoria = cfacade.getCategoriaByID(id);
        fileName = categoria.getNombre();
        blob = new javax.sql.rowset.serial.SerialBlob(categoria.getFoto());
        break;
}

java.io.InputStream inputStream = blob.getBinaryStream();
int fileLength = inputStream.available();
ServletContext context = getServletContext();
String mimeType = context.getMimeType(fileName);
if (mimeType == null) {        
    mimeType = "application/octet-stream";
}
response.setContentType(mimeType);
response.setContentLength(fileLength);
String headerKey = "Content-Disposition";
String headerValue = String.format("attachment; filename=\"%s\"", fileName);
response.setHeader(headerKey, headerValue);
java.io.OutputStream outStream = response.getOutputStream();
byte[] buffer = new byte[4096];
int bytesRead = -1;
while ((bytesRead = inputStream.read(buffer)) != -1) {
    outStream.write(buffer, 0, bytesRead);
}
inputStream.close();
outStream.flush();
outStream.close();

pfacade.close();
cfacade.close();
%>