<%-- 
    Document   : subir
    Created on : 29-may-2018, 1:18:22
    Author     : solid
--%>

<jsp:useBean id="pfacade" scope="page" class="facades.ProductoFacade"/>
<jsp:useBean id="cfacade" scope="page" class="facades.CategoriaFacade"/>

<%
int id = 0;
if(request.getParameter("id")!=null){
    id = Integer.parseInt(String.valueOf(request.getParameter("id")));
}
String tipo = "";
if(request.getParameter("tipo")!=null){
    tipo = String.valueOf(request.getParameter("tipo"));
}

switch (tipo){
    case "producto":
        beans.Producto producto = pfacade.getProductoByID(id);
        producto.setFoto(null);
        pfacade.updateProducto(producto);
        break;
    case "categoria":
        beans.Categoria categoria = cfacade.getCategoriaByID(id);
        categoria.setFoto(null);
        cfacade.updateCategoria(categoria);
        break;
}

cfacade.close();
pfacade.close();
%>