<%-- 
    Document   : punto
    Created on : 10/05/2018, 10:02:01 PM
    Author     : czara
--%>

<jsp:useBean id="facade" scope="page" class="facades.VentaFacade"/>
<jsp:useBean id="cfacade" scope="page" class="facades.ClienteFacade"/>
<jsp:useBean id="ufacade" scope="page" class="facades.UsuarioFacade"/>
<jsp:useBean id="sfacade" scope="page" class="facades.SucursalFacade"/>
<jsp:useBean id="pfacade" scope="page" class="facades.ProductoFacade"/>
<jsp:useBean id="tfacade" scope="page" class="facades.TipoFacade"/>

<%
if(request.getParameter("accion")!=null){
    beans.Venta venta = new beans.Venta();
    venta.setIdventa(0);
    venta.setFventa(new java.util.Date());
    venta.setEstado(0);
    venta.setUsuario(((beans.Usuario)session.getAttribute("usuario")).getNombre());
    venta.setSucursal((beans.Sucursal)session.getAttribute("sucursal"));
    venta.setCliente(cfacade.getClienteByID(Integer.parseInt(String.valueOf(request.getParameter("cliente_idcliente")))).getNombre());
    if(request.getParameter("accion").equals("1")){
        facade.saveVenta(venta);
    }
}
session.removeAttribute("detallespunto");
session.removeAttribute("detallespago");
%>

<div class="row">
    <div class="col-lg-12">
        <h5 class="text-center">&nbsp;</h5>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="col-lg-6">
    <div class="row">
        <div class="panel panel-primary">
            <div class="panel-heading">
                Listado de Categorias
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-12" id="categorias"></div>
                </div>
                <div class="row">
                    <div class="col-lg-12 pre-scrollable" id="productos" style="max-height: 800px; min-height: 800px;"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-lg-6">
    <div class="panel panel-primary">
        <div class="panel-heading">
            Resumen de Venta
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-11">
                    <div class="form-group">
                        <label for="cliente_idcliente">Seleccionar Cliente: </label>
                        <select id="cliente_idcliente" name="cliente_idcliente" class="form-control input-sm">
                            <%for(beans.Cliente cliente:cfacade.getClienteAll()){%>
                                <option value="<%=cliente.getNombre()%>"><%=cliente.getNombre()%></option>
                            <%}%>
                        </select>
                    </div>      
                </div>
                <div class="col-lg-1">
                    <div class="form-group">
                        <label for="agregar">&nbsp;</label>
                        <button type="button" class="btn btn-primary btn-block btn-sm" data-toggle="modal" data-target="#myModalNewCliente"><i class="fa fa-plus-circle" title="Agregar un Cliente"></i></button>
                    </div>   
                </div>
            </div>
	    <div class="row">
                <div class="col-lg-12">
                    <div class="form-group">
                        <label for="producto_idproducto">Buscar Producto: </label>
                        <select id="producto_idproducto" name="producto_idproducto" class="form-control input-sm" onchange="javascript:selectedProduct()">
                            <%for(beans.Producto producto:pfacade.getProductoAll()){%>
                                <option value="<%=producto.getIdproducto()%>|1|<%=producto.getPrecio()%>">[<%=producto.getCodigo()%>] - <%=producto.getNombre()%> - [<%=producto.getClave()%>]</option>
                            <%}%>
                        </select>
                    </div>      
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 pre-scrollable" id="detalle" style="max-height: 800px; min-height: 800px;"></div>
            </div>
        </div>
        <div class="panel-footer">
            <div class="row text-right">
                <div class="col-lg-12">
                    <button class="btn btn-default btn-sm" type="button" onclick="javascript:go2to('Punto/punto.jsp', 'page-wrapper');"><i class="fa fa-times-circle fa-fw"></i> Cancelar</button>       
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="myModalNewCliente" tabindex="-1" role="dialog" aria-labelledby="myModalLabelNewCliente" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Datos del Cliente</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-9">
                        <div class="form-group">
                            <label for="cliente_nombre">Nombre: (* Requerido)</label>
                            <input class="form-control input-sm" id="cliente_nombre" name="cliente_nombre" type="text" maxlength="45" autocomplete="off"/>
                        </div>      
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label for="cliente_telefono">Telefono: (* Requerido)</label>
                            <input class="form-control input-sm" id="cliente_telefono" name="cliente_telefono" type="number" min="0" max="9999999999"/>
                        </div>  
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-9">
                        <div class="form-group">
                            <label for="cliente_razon">Razon Social: (* Requerido)</label>
                            <input class="form-control input-sm" id="cliente_razon" name="cliente_razon" type="text" maxlength="80"/>
                        </div>      
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label for="cliente_rfc">R.F.C.: (* Requerido)</label>
                            <input class="form-control input-sm" id="cliente_rfc" name="cliente_rfc" type="text" maxlength="80"/>
                        </div>      
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label for="cliente_direccion">Direccion: (* Requerido)</label>
                            <input class="form-control input-sm" id="cliente_direccion" name="cliente_direccion" type="text" maxlength="80"/>
                        </div>      
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secundary btn-sm" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-success btn-sm" onclick="javascript:agregarCliente();">Agregar</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="modal fade" id="myModalNewPago" tabindex="-1" role="dialog" aria-labelledby="myModalLabelNewPago" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Agregar Pago</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <input class="form-control input-lg" id="pago_monto" name="pago_monto" type="number" min="0.01" max="9999999999.99" step=".01"/>
                        </div>  
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <select id="pago_idtipo" name="pago_idtipo" class="form-control input-lg">
                                <%for(beans.Tipo tipo:tfacade.getTipoAll()){%>
                                    <option value="<%=tipo.getIdtipo()%>"><%=tipo.getNombre()%></option>
                                <%}%>
                            </select>
                        </div>      
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secundary btn-lg" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-success btn-lg" data-dismiss="modal" onclick="javascript:agregarPago();">Agregar</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
                        
<script>    
    
    jQuery(document).ready(function () {
        $("#cliente_idcliente").chosen({no_results_text: "No se encuentran resultados para: ",width: "100%"});
        $("#producto_idproducto").chosen({no_results_text: "No se encuentran resultados para: ",width: "100%"});
    });
    
    function loadCategory(idcategoria,nombre){
        var params = "idcategoria="+idcategoria+"&nombre="+nombre;
        loadPanel("Punto/productos.jsp",params,"productos");
    }
    
    function selectedProduct(){
        var item = document.getElementById('producto_idproducto').options[document.getElementById('producto_idproducto').selectedIndex].value;
        var cadenas = item.split("|");
        var params = "idproducto="+cadenas[0]+"&cantidad="+cadenas[1]+"&precio="+cadenas[2];
        processDIV("/TPos/Detallepunto/agregar.jsp",params,"/TPos/Detallepunto/detallepunto.jsp","","detalle");
    }
    
    function addProducto(idproducto,cantidad,precio){
        var params = "idproducto="+idproducto+"&cantidad="+cantidad+"&precio="+precio;
        processDIV("/TPos/Detallepunto/agregar.jsp",params,"/TPos/Detallepunto/detallepunto.jsp","","detalle");
    }
    
    function agregarPago(){
        var monto = document.getElementById('pago_monto').value;
        var tipo = document.getElementById('pago_idtipo').options[document.getElementById('pago_idtipo').selectedIndex].innerHTML;
        var params = "tipo="+tipo+"&monto="+monto;
        processDIV("/TPos/Detallepago/agregar.jsp",params,"/TPos/Detallepunto/detallepunto.jsp","","detalle");
    }
    
    function agregarCliente(){
        var cliente_nombre = encodeURIComponent(document.getElementById("cliente_nombre").value);
        var cliente_telefono = encodeURIComponent(document.getElementById("cliente_telefono").value);
        var cliente_razon = encodeURIComponent(document.getElementById("cliente_razon").value);
        var cliente_rfc = encodeURIComponent(document.getElementById("cliente_rfc").value);
        var cliente_direccion = encodeURIComponent(document.getElementById("cliente_direccion").value);
        if(cliente_nombre !== "" && cliente_telefono !== "" && cliente_razon !== "" && cliente_rfc !== "" && cliente_direccion !== ""){
            $('#myModalNewCliente').modal('hide');
            setTimeout(function(){
                var params = "nombre="+cliente_nombre+"&telefono="+cliente_telefono+"&razon="+cliente_razon+"&rfc="+cliente_rfc+"&direccion="+cliente_direccion;
                processDIV("/TPos/Cliente/agregar.jsp",params,"/TPos/Punto/punto.jsp","","page-wrapper");
            },250);
        }
    }
    
    function guardar(){
        BootstrapDialog.confirm({
            title: "Mensaje del Sistema",
            message: "Confirma desea salvar la venta?",
            type: 'type-warning',
            closable: false,
            btnOKClass: 'btn-warning',
            callback: function(result) {
                if(result) {
                    var cliente = encodeURIComponent(document.getElementById("cliente_idcliente").options[document.getElementById("cliente_idcliente").selectedIndex].innerHTML);
                    var params = "cliente="+cliente;
                    processDIV("/TPos/Punto/vender.jsp",params,"/TPos/Punto/punto.jsp","","page-wrapper");
                }
            }
        });
    }
    
    loadPanel('Punto/categorias.jsp','','categorias');
    loadPanel('Detallepunto/detallepunto.jsp','','detalle');

</script>

<%
facade.close();
ufacade.close();
sfacade.close();
cfacade.close();
pfacade.close();
tfacade.close();
%>