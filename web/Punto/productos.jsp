<%-- 
    Document   : productos
    Created on : Oct 6, 2019, 1:35:19 AM
    Author     : czarate
--%>

<jsp:useBean id="isfacade" scope="page" class="facades.InventariosucursalFacade"/>
<jsp:useBean id="pfacade" scope="page" class="facades.ProductoFacade"/>
<jsp:useBean id="cfacade" scope="page" class="facades.CategoriaFacade"/>

<%
int idcategoria = 0;
if(request.getParameter("idcategoria")!=null){
    idcategoria = Integer.parseInt(String.valueOf(request.getParameter("idcategoria")));
}
beans.Categoria categoria = cfacade.getCategoriaByID(idcategoria);
int idsucursal = ((beans.Sucursal)session.getAttribute("sucursal")).getIdsucursal();
%>

<div class="well well-sm">
    <h4><center><b><%=categoria.getNombre()%></b></center></h4>
    <div class="row">
        <%for(beans.Producto producto:pfacade.getProductoByCategoria(categoria.getIdcategoria())){
            beans.Inventariosucursal inventariosucursal = isfacade.getInventariosucursalBySucursalANDProducto(idsucursal, producto.getIdproducto());
            if(inventariosucursal.getCantidad() > 0){
                
            }%>
            <div class="col-lg-3 col-md-4 col-xs-6 thumb">
                <a class="thumbnail" href="#" onclick="javascript:addProducto(<%=producto.getIdproducto()%>,1,<%=producto.getPrecio()%>)">
                    <%if(producto.getFoto()==null){%>
                        <img class="img-responsive" src='<%=request.getContextPath()%>/images/100x100.gif' height='100px' width='100px' alt='Image'><div class='caption'><h6><center><%=producto.getNombre()%></center></h6></div>
                    <%}else{%>
                        <img class="img-responsive" src='<%=request.getContextPath()%>/Upload/descargar.jsp?id=<%=producto.getIdproducto()%>&tipo=producto' height='100px' width='100px' alt='Image'><div class='caption'><h6><center><%=producto.getNombre()%></center></h6></div>
                    <%}%>
                </a>
            </div>
        <%}%>
    </div>
</div>

<%
pfacade.close();
cfacade.close();
isfacade.close();
%>