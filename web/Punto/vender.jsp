<%-- 
    Document   : agregar
    Created on : 8/05/2018, 12:48:53 AM
    Author     : czara
--%>

<jsp:useBean id="pfacade" scope="page" class="facades.ProductoFacade"/>
<jsp:useBean id="vfacade" scope="page" class="facades.VentaFacade"/>
<jsp:useBean id="dvfacade" scope="page" class="facades.DetalleventaFacade"/>
<jsp:useBean id="dpfacade" scope="page" class="facades.DetallepagoFacade"/>

<%
String cliente = "";
if(request.getParameter("cliente")!=null){
    cliente = String.valueOf(request.getParameter("cliente"));
}

//registrando la venta
beans.Venta venta = new beans.Venta();
venta.setIdventa(0);
venta.setFventa(new java.util.Date());
venta.setEstado(0);
venta.setCliente(cliente);
venta.setUsuario(((beans.Usuario)session.getAttribute("usuario")).getNombre());
venta.setSucursal(((beans.Sucursal)session.getAttribute("sucursal")));
int idventa = vfacade.saveVenta(venta);
venta = vfacade.getVentaByID(idventa);

//registrando los productos vendidos
java.util.List<tools.Detalle> detalles = new java.util.ArrayList();
if(session.getAttribute("detallespunto")!=null){
    detalles = (java.util.List<tools.Detalle>)session.getAttribute("detallespunto");
}
for(tools.Detalle detalle:detalles){
    beans.Producto producto = pfacade.getProductoByID(detalle.getIdproducto());
    beans.Detalleventa detalleventa = new beans.Detalleventa();
    detalleventa.setIddetalleventa(0);
    detalleventa.setVenta(venta);
    detalleventa.setProducto(producto.getNombre());
    detalleventa.setMarca(producto.getMarca());
    detalleventa.setUnidad(producto.getUnidad());
    detalleventa.setCategoria(producto.getCategoria().getNombre());
    detalleventa.setCantidad(detalle.getCantidad());
    detalleventa.setPrecio(detalle.getPrecio());
    if(producto.getImpuesto()==0){
        detalleventa.setImpuesto(0.0);
    }else{
        detalleventa.setImpuesto((double)session.getAttribute("impuesto"));
    }
    dvfacade.saveDetalleventa(detalleventa);
}

//registrando los pagos y exhibiciones
java.util.List<tools.Pago> pagos = new java.util.ArrayList();
if(session.getAttribute("detallespago")!=null){
    pagos = (java.util.List<tools.Pago>)session.getAttribute("detallespago");
}
for(tools.Pago pago:pagos){
    beans.Detallepago detallepago = new beans.Detallepago();
    detallepago.setIddetallepago(0);
    detallepago.setMonto(pago.getMonto());
    detallepago.setTipo(pago.getTipo());
    detallepago.setVenta(venta);
    dpfacade.saveDetallepago(detallepago);
}

pfacade.close();
vfacade.close();
dvfacade.close();
dpfacade.close();
%>