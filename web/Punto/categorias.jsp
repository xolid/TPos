<%-- 
    Document   : articulos
    Created on : Oct 4, 2019, 3:47:06 PM
    Author     : czarate
--%>

<jsp:useBean id="cfacade" scope="page" class="facades.CategoriaFacade"/>

<%
Integer idcategoria = null;
%>

<div class="row">
    <div class="col-xs-12">
        <div class="well well-sm">
            <div id="carousel-slider" class="carousel slide">
                <div class="carousel-inner">
                    <%
                    boolean first = true;
                    java.util.List<beans.Categoria> categorias = cfacade.getCategoriaAll();
                    for(int i=0;i<categorias.size();i+=4){
                        if(first){
                            first = false;
                            idcategoria = categorias.get(i).getIdcategoria();
                            out.print("<div class='item active'>");
                        }else{
                            out.print("<div class='item'>");
                        }
                        out.print("<div class='row'>");
                        beans.Categoria categoria1 = categorias.get(i);
                        try{
                            if(categoria1!=null){
                                if(categoria1.getFoto()==null){
                                    out.print("<div class='col-xs-3'><a href='#' class='thumbnailnomargin' onclick='javascript:loadCategory("+categoria1.getIdcategoria()+")'><img src='"+request.getContextPath()+"/images/100x100.gif' height='100px' width='100px' alt='Image'><div class='caption'><h6><center>"+categoria1.getNombre()+"</center></h6></div></a></div>");
                                }else{
                                    out.print("<div class='col-xs-3'><a href='#' class='thumbnailnomargin' onclick='javascript:loadCategory("+categoria1.getIdcategoria()+")'><img src='"+request.getContextPath()+"/Upload/descargar.jsp?id="+categoria1.getIdcategoria()+"&tipo=categoria' height='100px' width='100px' alt='Image'><div class='caption'><h6><center>"+categoria1.getNombre()+"</center></h6></div></a></div>");
                                }
                            }   
                        }catch(Exception e){}
                        try{
                            beans.Categoria categoria2 = categorias.get(i+1);
                            if(categoria2!=null){
                                if(categoria2.getFoto()==null){
                                    out.print("<div class='col-xs-3'><a href='#' class='thumbnailnomargin' onclick='javascript:loadCategory("+categoria2.getIdcategoria()+")'><img src='"+request.getContextPath()+"/images/100x100.gif' height='100px' width='100px' alt='Image'><div class='caption'><h6><center>"+categoria2.getNombre()+"</center></h6></div></a></div>");
                                }else{
                                    out.print("<div class='col-xs-3'><a href='#' class='thumbnailnomargin' onclick='javascript:loadCategory("+categoria2.getIdcategoria()+")'><img src='"+request.getContextPath()+"/Upload/descargar.jsp?id="+categoria2.getIdcategoria()+"&tipo=categoria' height='100px' width='100px' alt='Image'><div class='caption'><h6><center>"+categoria2.getNombre()+"</center></h6></div></a></div>");
                                }
                            }
                        }catch(Exception e){}
                        try{
                            beans.Categoria categoria3 = categorias.get(i+2);
                            if(categoria3!=null){
                                if(categoria3.getFoto()==null){
                                    out.print("<div class='col-xs-3'><a href='#' class='thumbnailnomargin' onclick='javascript:loadCategory("+categoria3.getIdcategoria()+")'><img src='"+request.getContextPath()+"/images/100x100.gif' height='100px' width='100px' alt='Image'><div class='caption'><h6><center>"+categoria3.getNombre()+"</center></h6></div></a></div>");
                                }else{
                                    out.print("<div class='col-xs-3'><a href='#' class='thumbnailnomargin' onclick='javascript:loadCategory("+categoria3.getIdcategoria()+")'><img src='"+request.getContextPath()+"/Upload/descargar.jsp?id="+categoria3.getIdcategoria()+"&tipo=categoria' height='100px' width='100px' alt='Image'><div class='caption'><h6><center>"+categoria3.getNombre()+"</center></h6></div></a></div>");
                                }
                            }
                        }catch(Exception e){}
                        try{
                            beans.Categoria categoria4 = categorias.get(i+3);
                            if(categoria4!=null){
                                if(categoria4.getFoto()==null){
                                    out.print("<div class='col-xs-3'><a href='#' class='thumbnailnomargin' onclick='javascript:loadCategory("+categoria4.getIdcategoria()+")'><img src='"+request.getContextPath()+"/images/100x100.gif' height='100px' width='100px' alt='Image'><div class='caption'><h6><center>"+categoria4.getNombre()+"</center></h6></div></a></div>");
                                }else{
                                    out.print("<div class='col-xs-3'><a href='#' class='thumbnailnomargin' onclick='javascript:loadCategory("+categoria4.getIdcategoria()+")'><img src='"+request.getContextPath()+"/Upload/descargar.jsp?id="+categoria4.getIdcategoria()+"&tipo=categoria' height='100px' width='100px' alt='Image'><div class='caption'><h6><center>"+categoria4.getNombre()+"</center></h6></div></a></div>");
                                }
                            }
                        }catch(Exception e){}
                        out.print("</div>");
                        out.print("</div>");
                    }%>
                </div>
                <a class="left carousel-control" href="#carousel-slider" data-slide="prev"><</a> 
                <a class="right carousel-control" href="#carousel-slider" data-slide="next">></a> 
            </div>
        </div>
    </div>
</div>
                
<%
if(idcategoria!=null){
    out.print("<script>");
    out.print("loadCategory("+idcategoria+");");
    out.print("</script>");
}
cfacade.close();
%>