<%-- 
    Document   : categoria
    Created on : Jan 9, 2016, 7:42:35 PM
    Author     : czara
--%>

<jsp:useBean id="categoria" class="beans.Categoria" scope="page"/>
<jsp:setProperty name="categoria" property="*" />
<jsp:useBean id="facade" scope="page" class="facades.CategoriaFacade"/>

<%if(request.getParameter("accion")!=null){
    if(request.getParameter("accion").equals("1")){
        if(categoria.getIdcategoria()==0){
            facade.saveCategoria(categoria);
        }else{
            facade.updateCategoria(categoria);
        }
    }
    if(request.getParameter("accion").equals("2")){
        facade.deleteCategoria(categoria);        
    }
}
if(request.getParameter("id")!=null){
    categoria=facade.getCategoriaByID(Integer.parseInt((String)request.getParameter("id")));
}else{
    categoria.setIdcategoria(0);
    categoria.setNombre("");
}%>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Catalogos del Sistema - Categoria</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-9">
        <div class="panel panel-primary">
            <div class="panel-heading">
                Definicion de la Categoria
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <form categoriae="form" id="form1" name="form1" action="categoria.jsp" method="post">
                    <input type="hidden" id="idcategoria" name="idcategoria" value="<%=categoria.getIdcategoria()%>"/>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="nombre">Nombre: </label>
                                <input class="form-control input-sm" id="nombre" name="nombre" type="text" maxlength="45" value="<%=categoria.getNombre()%>" autocomplete="off" required autofocus/>
                            </div>      
                        </div>
                    </div>
                </form>                
            </div>
            <div class="panel-footer">
                <div class="row">
                    <div class="col-lg-12">
                        <button class="btn btn-primary btn-sm" type="button" onclick="javascript:save('Categoria/categoria.jsp','Categoria/categoriaListado.jsp','form1','page-wrapper');"><i class="fa fa-save fa-fw"></i> Guardar</button>
                        <%if(categoria.getIdcategoria()!=0){%>
                            <button class="btn btn-danger btn-sm" type="button" onclick="javascript:erase('Categoria/categoria.jsp','Categoria/categoriaListado.jsp','form1','page-wrapper');"><i class="fa fa-eraser fa-fw"></i> Borrar</button>
                        <%}%>
                        <button class="btn btn-default btn-sm" type="button" onclick="javascript:go2to('Categoria/categoriaListado.jsp','page-wrapper');"><i class="fa fa-backward fa-fw"></i> Volver</button>                 
                    </div>
                </div>
            </div>
            <!-- /.panel-footer -->
        </div>
        <!-- /.panel -->
    </div>
    <%if(categoria.getIdcategoria()>0){%>
        <div class="col-md-3">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Imagen de la Categoria
                </div>
                <div class="panel-body">
                    <div class="text-center">
                        <%if(categoria.getFoto()==null){%>
                            <img class="profile-user-img img-fluid img-circle" src="<%=request.getContextPath()%>/images/default-150x150.png" height="150" width="150" alt="User profile picture">
                        <%}else{%>
                            <img class="profile-user-img img-fluid img-circle" src="<%=request.getContextPath()%>/Upload/descargar.jsp?id=<%=categoria.getIdcategoria()%>&tipo=categoria" height="150" width="150" alt="Product profile picture">
                        <%}%>
                    </div>
                </div>
                <div class="panel-footer">
                    <div class="row">
                        <div class="col-lg-6">
                            <a href="#" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModalUploadCategoria"><i class="fa fa-picture-o fa-fw"></i> <%=categoria.getFoto()==null?"Subir":"Cambiar"%></a>
                        </div>
                        <div class="col-lg-6 text-right">
                            <%if(categoria.getFoto()!=null){%>
                                <a href="#" class="btn btn-danger btn-sm" onclick="javascript:remove();"><i class="fa fa-times fa-fw"></i> Remover</a>
                            <%}%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <%}%>
</div>
<!-- /.row -->
<!-- /.row -->
<div class="modal fade" id="myModalUploadCategoria" tabindex="-1" role="dialog" aria-labelledby="myModalLabelCategoria" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Seleccionar Archivo</h4>
            </div>
            <div class="modal-body">
                <object data="<%=request.getContextPath()%>/Upload/seleccionarIMG.jsp?id=<%=categoria.getIdcategoria()%>&tipo=categoria" width="100%" height="120">
                    <embed src="<%=request.getContextPath()%>/Upload/seleccionarIMG.jsp?id=<%=categoria.getIdcategoria()%>&tipo=categoria" width="100%" height="120"></embed>
                </object>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<script>

    window.closeModal = function(){
        $('#myModalUploadCategoria').modal('hide');
        setTimeout(function(){
            loadPanel('Categoria/categoria.jsp','id=<%=categoria.getIdcategoria()%>','page-wrapper');
        },250);
    };

    function remove(){
        var params = "id=<%=categoria.getIdcategoria()%>&tipo=categoria";
        processDIV("/TPos/Upload/remover.jsp",params,"Categoria/categoria.jsp","id=<%=categoria.getIdcategoria()%>","page-wrapper");
    }

</script>

<%
facade.close();
%>