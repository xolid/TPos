<%-- 
    Document   : disponibilidad
    Created on : May 9, 2018, 4:35:24 PM
    Author     : czarate
--%>

<jsp:useBean id="sfacade" scope="page" class="facades.SucursalFacade"/>
<jsp:useBean id="afacade" scope="page" class="facades.AlmacenFacade"/>
<jsp:useBean id="ifacade" scope="page" class="facades.InventariosucursalFacade"/>

<%
int idproducto = 0;
if(request.getParameter("idproducto")!=null){
    idproducto = Integer.parseInt(String.valueOf(request.getParameter("idproducto")));
}
%>

<%for(beans.Sucursal sucursal:sfacade.getSucursalAll()){%>
    <dl>
        <%beans.Inventariosucursal inventario = ifacade.getInventariosucursalBySucursalANDProducto(sucursal.getIdsucursal(),idproducto);
        if(inventario != null){
            if(inventario.getProducto().getTipo()==0){
                String colorBadge = "";
                if(inventario.getCantidad()==0){
                    colorBadge = "label-danger";
                }else{
                    if(inventario.getCantidad() < inventario.getMinimo()){
                        colorBadge = "label-warning";
                    }else{
                        colorBadge = "label-success";
                    }
                }
                out.print("<dt><span class=\"label "+colorBadge+"\">"+inventario.getCantidad()+"</span> <abbr title=\"Direccion: "+sucursal.getDireccion()+" Telefono: "+sucursal.getTelefono()+"\">"+sucursal.getNombre()+"</abbr></dt>");
            }else{
                out.print("<dt><span class=\"label label-info\">Disponible</span> <abbr title=\"Direccion: "+sucursal.getDireccion()+" Telefono: "+sucursal.getTelefono()+"\">"+sucursal.getNombre()+"</abbr></dt>");
            }
        }else{
            out.print("<dt><span class=\"label label-default\">No Disponible</span> <abbr title=\"Direccion: "+sucursal.getDireccion()+" Telefono: "+sucursal.getTelefono()+"\">"+sucursal.getNombre()+"</abbr></dt>");
        }%>                                    
    </dl>    
<%}
sfacade.close();
afacade.close();
ifacade.close();
%>