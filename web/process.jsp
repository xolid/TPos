<%-- 
    Document   : process2
    Created on : Feb 7, 2016, 7:18:03 PM
    Author     : czara
--%>
 
<%@page import="java.util.*"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page import="org.json.simple.JSONArray"%>

<jsp:useBean id="facade" scope="page" class="facades.ProductoFacade"/>
<jsp:useBean id="sfacade" scope="page" class="facades.SucursalFacade"/>

<%
java.util.Locale mexico = new java.util.Locale("es", "MX");
java.text.NumberFormat formatoImporte = java.text.NumberFormat.getCurrencyInstance(mexico);
String[] cols = { "codigo" , "pnombre" , "clave", "unidad", "cnombre", "tipo", "precio" };
JSONObject result = new JSONObject();
JSONArray array = new JSONArray();
int amount = 10;
int start = 0;
int echo = 0;
int col = 0;
String dir = "asc";
String sStart = request.getParameter("start");
String sAmount = request.getParameter("length");
String sEcho = request.getParameter("draw");
String sCol = request.getParameter("order[0][column]");
String sdir = request.getParameter("order[0][dir]");
String buscar = request.getParameter("search[value]");
if (sStart != null) {
    start = Integer.parseInt(sStart);
    if (start < 0)
        start = 0;
}
if (sAmount != null) {
    amount = Integer.parseInt(sAmount);
    if (amount < 10 || amount > 100)
        amount = 10;
}
if (sEcho != null) {
    echo = Integer.parseInt(sEcho);
}
if (sCol != null) {
    col = Integer.parseInt(sCol);
    if (col < 0 || col > 6)
        col = 0;
}
if (sdir != null) {
    if (!sdir.equals("asc"))
        dir = "desc";
}
String colName = cols[col];
long total = facade.getTotal();

List<Object[]> productos = facade.getProductoForProcessOrderable(buscar, colName, dir, start, amount);
for(Object[] p:productos){
    JSONArray ja = new JSONArray();
    ja.add("<a href=\"#\" onclick=\"javascript:abrir("+p[0]+",'"+p[2]+"')\">"+p[2]+"</a>");
    ja.add(p[1]);
    ja.add(p[3]);    
    ja.add(p[4]);
    ja.add(p[5]);
    ja.add(new tools.Tipo().getNombre(Integer.parseInt(String.valueOf(p[6]))));
    ja.add(formatoImporte.format(new java.math.BigDecimal(String.valueOf(p[7]))));
    array.add(ja);
}
long totalAfterFilter = facade.getProductoForProcessCount(buscar);
result.put("draw", sEcho);
result.put("recordsTotal", total);
result.put("recordsFiltered", totalAfterFilter);
result.put("data", array);
response.setContentType("application/json");
response.setHeader("Cache-Control", "no-store");
out.print(result);

facade.close();  
sfacade.close();
%>

