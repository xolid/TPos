/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facades;

import java.util.List;
import beans.Detallecompra;
import org.hibernate.Query;
import org.hibernate.Session;
import hibernate.HibernateUtil;
import org.hibernate.SQLQuery;

/**
 *
 * @author solid
 */
public class DetallecompraFacade {

    private Session session;

    public DetallecompraFacade() {
        session = HibernateUtil.getSessionFactory().openSession();
    }

    public Detallecompra getDetallecompraByID(int iddetallecompra) {
        Detallecompra detallecompra = (Detallecompra) session.get(Detallecompra.class, iddetallecompra);
        return detallecompra;
    }

    public Integer saveDetallecompra(Detallecompra detallecompra) {
        Integer identifier = 0;
        try {
            session.getTransaction().begin();
            identifier = (Integer) session.save(detallecompra);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
        return identifier;
    }

    public void updateDetallecompra(Detallecompra detallecompra) {
        try {
            session.getTransaction().begin();
            Detallecompra o = getDetallecompraByID(detallecompra.getIddetallecompra());
            o.setCompra(detallecompra.getCompra());
            o.setProducto(detallecompra.getProducto());
            o.setMarca(detallecompra.getMarca());
            o.setUnidad(detallecompra.getUnidad());
            o.setCantidad(detallecompra.getCantidad());
            o.setPrecio(detallecompra.getPrecio());
            o.setImpuesto(detallecompra.getImpuesto());
            session.update(o);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
    }

    public void deleteDetallecompra(Detallecompra detallecompra) {
        try {
            session.getTransaction().begin();
            Detallecompra o = getDetallecompraByID(detallecompra.getIddetallecompra());
            session.delete(o);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
    }
    
    public int deleteByCompra(int idcompra) {
        session.getTransaction().begin();
        SQLQuery q = session.createSQLQuery("delete from Detallecompra as detallecompra where compra_idcompra = :idcompra");
        q.setInteger("idcompra", idcompra);
        int total = q.executeUpdate();
        session.getTransaction().commit();
        return total;
    }

    public List<Detallecompra> getDetallecompraAll() {
        session.getTransaction().begin();
        Query q = session.createQuery("from Detallecompra as detallecompra");
        List<Detallecompra> detallecompras = (List<Detallecompra>) q.list();
        session.getTransaction().commit();
        return detallecompras;
    }

    public long getTotal() {
        session.getTransaction().begin();
        Query q = session.createQuery("select count(*) from Detallecompra as detallecompra");
        long total = (Long) q.uniqueResult();
        session.getTransaction().commit();
        return total;
    }
    
    public List<Detallecompra> getDetallecompraByCompra(int idcompra) {
        session.getTransaction().begin();
        Query q = session.createQuery("from Detallecompra as detallecompra where compra_idcompra = :idcompra");
        q.setInteger("idcompra", idcompra);
        List<Detallecompra> detallecompras = (List<Detallecompra>) q.list();
        session.getTransaction().commit();
        return detallecompras;
    }

    public void close(){
        session.clear();
        session.close();
    }
    
    @Override
    public void finalize(){
        try {
            close();
        } finally {
            try {
                super.finalize();
            } catch (Throwable ex) { }
        }
    }
    
}
