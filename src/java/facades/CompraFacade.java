/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facades;

import java.util.List;
import beans.Compra;
import org.hibernate.Query;
import org.hibernate.Session;
import hibernate.HibernateUtil;
import org.hibernate.SQLQuery;

/**
 *
 * @author solid
 */
public class CompraFacade {

    private Session session;

    public CompraFacade() {
        session = HibernateUtil.getSessionFactory().openSession();
    }

    public Compra getCompraByID(int idcompra) {
        Compra compra = (Compra) session.get(Compra.class, idcompra);
        return compra;
    }

    public Integer saveCompra(Compra compra) {
        Integer identifier = 0;
        try {
            session.getTransaction().begin();
            identifier = (Integer) session.save(compra);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
        return identifier;
    }

    public void updateCompra(Compra compra) {
        try {
            session.getTransaction().begin();
            Compra o = getCompraByID(compra.getIdcompra());
            o.setFcompra(compra.getFcompra());
            o.setEstado(compra.getEstado());
            o.setProveedor(compra.getProveedor());
            o.setUsuario(compra.getUsuario());
            o.setAlmacen(compra.getAlmacen());
            session.update(o);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
    }

    public void deleteCompra(Compra compra) {
        try {
            session.getTransaction().begin();
            Compra o = getCompraByID(compra.getIdcompra());
            session.delete(o);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
    }

    public List<Compra> getCompraAll() {
        session.getTransaction().begin();
        Query q = session.createQuery("from Compra as compra");
        List<Compra> compras = (List<Compra>) q.list();
        session.getTransaction().commit();
        return compras;
    }

    public long getTotal() {
        session.getTransaction().begin();
        Query q = session.createQuery("select count(*) from Compra as compra");
        long total = (Long) q.uniqueResult();
        session.getTransaction().commit();
        return total;
    }
    
    public List<beans.Compra> getCompraForProcess(String buscar, String colname, String dir, int index, int size) {
        session.getTransaction().begin();
        Query q = session.createQuery("FROM Compra compra WHERE (proveedor like :buscar) or (usuario like :buscar) or (fcompra like :buscar)) order by "+colname+" "+dir);
        q.setString("buscar", "%"+buscar+"%");
        q.setFirstResult(index);
        q.setMaxResults(size);
        List<beans.Compra> compras = (List<beans.Compra>) q.list();
        session.getTransaction().commit();
        return compras;
    }
    
    public Long getCompraForProcessCount(String buscar) {
        session.getTransaction().begin();
        Query q = session.createQuery("select count(*) FROM Compra compra WHERE ((proveedor like :buscar) or (usuario like :buscar) or (fcompra like :buscar)) ");
        q.setString("buscar", "%"+buscar+"%");
        Long total =  (Long)q.uniqueResult();
        session.getTransaction().commit();
        return total;
    }

    public void close(){
        session.clear();
        session.close();
    }
    
    @Override
    public void finalize(){
        try {
            close();
        } finally {
            try {
                super.finalize();
            } catch (Throwable ex) { }
        }
    }
    
}
