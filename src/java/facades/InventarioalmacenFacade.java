/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facades;

import beans.Inventarioalmacen;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import hibernate.HibernateUtil;
import org.hibernate.SQLQuery;

/**
 *
 * @author solid
 */
public class InventarioalmacenFacade {

    private Session session;

    public InventarioalmacenFacade() {
        session = HibernateUtil.getSessionFactory().openSession();
    }

    public Inventarioalmacen getInventarioalmacenByID(int idinventarioalmacen) {
        Inventarioalmacen inventarioalmacen = (Inventarioalmacen) session.get(Inventarioalmacen.class, idinventarioalmacen);
        return inventarioalmacen;
    }

    public Integer saveInventarioalmacen(Inventarioalmacen inventarioalmacen) {
        Integer identifier = 0;
        try {
            session.getTransaction().begin();
            identifier = (Integer) session.save(inventarioalmacen);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
        return identifier;
    }

    public void updateInventarioalmacen(Inventarioalmacen inventarioalmacen) {
        try {
            session.getTransaction().begin();
            Inventarioalmacen o = getInventarioalmacenByID(inventarioalmacen.getIdinventarioalmacen());
            o.setCantidad(inventarioalmacen.getCantidad());
            o.setMaximo(inventarioalmacen.getMaximo());
            o.setMinimo(inventarioalmacen.getMinimo());
            o.setAlmacen(inventarioalmacen.getAlmacen());
            o.setProducto(inventarioalmacen.getProducto());
            session.update(o);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
    }

    public void deleteInventarioalmacen(Inventarioalmacen inventarioalmacen) {
        try {
            session.getTransaction().begin();
            Inventarioalmacen o = getInventarioalmacenByID(inventarioalmacen.getIdinventarioalmacen());
            session.delete(o);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
    }

    public List<Inventarioalmacen> getInventarioalmacenAll() {
        session.getTransaction().begin();
        Query q = session.createQuery("from Inventarioalmacen as inventarioalmacen");
        List<Inventarioalmacen> inventarioalmacens = (List<Inventarioalmacen>) q.list();
        session.getTransaction().commit();
        return inventarioalmacens;
    }
    
    public Inventarioalmacen getInventarioalmacenByAlmacenANDProducto(int idalmacen, int idproducto) {
        session.getTransaction().begin();
        Query q = session.createQuery("from Inventarioalmacen as inventarioalmacen where almacen_idalmacen = :idalmacen and producto_idproducto = :idproducto");
        q.setInteger("idalmacen", idalmacen);
        q.setInteger("idproducto", idproducto);
        q.setMaxResults(1);
        Inventarioalmacen inventarioalmacen = (Inventarioalmacen) q.uniqueResult();
        session.getTransaction().commit();
        return inventarioalmacen;
    }

    public long getTotal() {
        session.getTransaction().begin();
        Query q = session.createQuery("select count(*) from Inventarioalmacen as inventarioalmacen");
        long total = (Long) q.uniqueResult();
        session.getTransaction().commit();
        return total;
    }
 
    public List<Inventarioalmacen> getInventarioalmacenForProcess(String buscar, int idalmacen, String colname, String dir, int index, int size) {
        session.getTransaction().begin();
        Query q = session.createQuery("from Inventarioalmacen as inventarioalmacen where almacen_idalmacen = :idalmacen order by "+colname+" "+dir);
        q.setInteger("idalmacen",idalmacen);
        q.setFirstResult(index);
        q.setMaxResults(size);
        List<Inventarioalmacen> inventarioalmacens = (List<Inventarioalmacen>) q.list();
        session.getTransaction().commit();
        return inventarioalmacens;
    }
    
    public Long getInventarioalmacenForProcessCount(String buscar, int idalmacen) {
        session.getTransaction().begin();
        Query q = session.createQuery("select count(*) from Inventarioalmacen as inventarioalmacen where almacen_idalmacen = :idalmacen");
        q.setInteger("idalmacen",idalmacen);
        long total = (Long) q.uniqueResult();
        session.getTransaction().commit();
        return total;
    }
    
    public List<Object[]> getInventarioalmacenForProcessJOINProducto(String buscar, int idalmacen, String colname, String dir, int index, int size) {
        session.getTransaction().begin();
        SQLQuery q = session.createSQLQuery("select "
                + "inventarioalmacen.idinventarioalmacen as idinventarioalmacen,"
                + "producto.codigo as codigo,"
                + "producto.nombre as nombre,"
                + "inventarioalmacen.cantidad as cantidad,"
                + "inventarioalmacen.minimo as minimo,"
                + "inventarioalmacen.maximo as maximo, "
                + "producto.tipo as tipo "
                + "from inventarioalmacen "
                + "inner join producto on producto.idproducto = inventarioalmacen.producto_idproducto "
                + "where "
                + "almacen_idalmacen = :idalmacen "
                + "and ((producto.codigo like :buscar) or (producto.nombre like :buscar)) "
                + "order by "+colname+" "+dir);
        q.setInteger("idalmacen",idalmacen);
        q.setString("buscar", "%"+buscar+"%");
        q.setFirstResult(index);
        q.setMaxResults(size);
        List<Object[]> inventarioalmacens = (List<Object[]>) q.list();
        session.getTransaction().commit();
        return inventarioalmacens;
    }
    
    public java.math.BigInteger getInventarioalmacenForProcessCountJOINProducto(String buscar, int idalmacen) {
        session.getTransaction().begin();
        SQLQuery q = session.createSQLQuery("select "
                + "count(*) "
                + "from inventarioalmacen inner join producto on producto.idproducto = inventarioalmacen.producto_idproducto "
                + "where almacen_idalmacen = :idalmacen "
                + "and ((producto.codigo like :buscar) or (producto.nombre like :buscar))");
        q.setInteger("idalmacen",idalmacen);
        q.setString("buscar", "%"+buscar+"%");
        java.math.BigInteger total = (java.math.BigInteger) q.uniqueResult();
        session.getTransaction().commit();
        return total;
    }

    public void close(){
        session.clear();
        session.close();
    }
    
    @Override
    public void finalize(){
        try {
            close();
        } finally {
            try {
                super.finalize();
            } catch (Throwable ex) { }
        }
    }
    
}
