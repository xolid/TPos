/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facades;

import java.util.List;
import beans.Detalleventa;
import org.hibernate.Query;
import org.hibernate.Session;
import hibernate.HibernateUtil;
import org.hibernate.SQLQuery;

/**
 *
 * @author solid
 */
public class DetalleventaFacade {

    private Session session;

    public DetalleventaFacade() {
        session = HibernateUtil.getSessionFactory().openSession();
    }

    public Detalleventa getDetalleventaByID(int iddetalleventa) {
        Detalleventa detalleventa = (Detalleventa) session.get(Detalleventa.class, iddetalleventa);
        return detalleventa;
    }

    public Integer saveDetalleventa(Detalleventa detalleventa) {
        Integer identifier = 0;
        try {
            session.getTransaction().begin();
            identifier = (Integer) session.save(detalleventa);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
        return identifier;
    }

    public void updateDetalleventa(Detalleventa detalleventa) {
        try {
            session.getTransaction().begin();
            Detalleventa o = getDetalleventaByID(detalleventa.getIddetalleventa());
            o.setVenta(detalleventa.getVenta());
            o.setProducto(detalleventa.getProducto());
            o.setMarca(detalleventa.getMarca());
            o.setUnidad(detalleventa.getUnidad());
            o.setCantidad(detalleventa.getCantidad());
            o.setPrecio(detalleventa.getPrecio());
            o.setImpuesto(detalleventa.getImpuesto());
            session.update(o);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
    }

    public void deleteDetalleventa(Detalleventa detalleventa) {
        try {
            session.getTransaction().begin();
            Detalleventa o = getDetalleventaByID(detalleventa.getIddetalleventa());
            session.delete(o);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
    }
    
    public int deleteByVenta(int idventa) {
        session.getTransaction().begin();
        SQLQuery q = session.createSQLQuery("delete from Detalleventa as detalleventa where venta_idventa = :idventa");
        q.setInteger("idventa", idventa);
        int total = q.executeUpdate();
        session.getTransaction().commit();
        return total;
    }

    public List<Detalleventa> getDetalleventaAll() {
        session.getTransaction().begin();
        Query q = session.createQuery("from Detalleventa as detalleventa");
        List<Detalleventa> detalleventas = (List<Detalleventa>) q.list();
        session.getTransaction().commit();
        return detalleventas;
    }

    public long getTotal() {
        session.getTransaction().begin();
        Query q = session.createQuery("select count(*) from Detalleventa as detalleventa");
        long total = (Long) q.uniqueResult();
        session.getTransaction().commit();
        return total;
    }
    
    public List<Detalleventa> getDetalleventaByVenta(int idventa) {
        session.getTransaction().begin();
        Query q = session.createQuery("from Detalleventa as detalleventa where venta_idventa = :idventa");
        q.setInteger("idventa", idventa);
        List<Detalleventa> detalleventas = (List<Detalleventa>) q.list();
        session.getTransaction().commit();
        return detalleventas;
    }    

    public void close(){
        session.clear();
        session.close();
    }
    
    @Override
    public void finalize(){
        try {
            close();
        } finally {
            try {
                super.finalize();
            } catch (Throwable ex) { }
        }
    }
    
}
