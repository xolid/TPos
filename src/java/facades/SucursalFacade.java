/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facades;

import java.util.List;
import beans.Sucursal;
import org.hibernate.Query;
import org.hibernate.Session;
import hibernate.HibernateUtil;

/**
 *
 * @author solid
 */
public class SucursalFacade {

    private Session session;

    public SucursalFacade() {
        session = HibernateUtil.getSessionFactory().openSession();
    }

    public Sucursal getSucursalByID(int idsucursal) {
        Sucursal sucursal = (Sucursal) session.get(Sucursal.class, idsucursal);
        return sucursal;
    }

    public Integer saveSucursal(Sucursal sucursal) {
        Integer identifier = 0;
        try {
            session.getTransaction().begin();
            identifier = (Integer) session.save(sucursal);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
        return identifier;
    }

    public void updateSucursal(Sucursal sucursal) {
        try {
            session.getTransaction().begin();
            Sucursal o = getSucursalByID(sucursal.getIdsucursal());
            o.setNombre(sucursal.getNombre());
            o.setDireccion(sucursal.getDireccion());
            o.setTelefono(sucursal.getTelefono());
            o.setIva(sucursal.getIva());
            session.update(o);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
    }

    public void deleteSucursal(Sucursal sucursal) {
        try {
            session.getTransaction().begin();
            Sucursal o = getSucursalByID(sucursal.getIdsucursal());
            session.delete(o);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
    }

    public List<Sucursal> getSucursalAll() {
        session.getTransaction().begin();
        Query q = session.createQuery("from Sucursal as sucursal order by nombre");
        List<Sucursal> sucursals = (List<Sucursal>) q.list();
        session.getTransaction().commit();
        return sucursals;
    }

    public long getTotal() {
        session.getTransaction().begin();
        Query q = session.createQuery("select count(*) from Sucursal as sucursal");
        long total = (Long) q.uniqueResult();
        session.getTransaction().commit();
        return total;
    }
 
    public List<Sucursal> getSucursalForProcess(String buscar, String colname, String dir, int index, int size) {
        session.getTransaction().begin();
        Query q = session.createQuery("from Sucursal as sucursal where ((nombre like :buscar)) order by "+colname+" "+dir);
        q.setString("buscar", "%"+buscar+"%");
        q.setFirstResult(index);
        q.setMaxResults(size);
        List<Sucursal> sucursals = (List<Sucursal>) q.list();
        session.getTransaction().commit();
        return sucursals;
    }
    
    public Long getSucursalForProcessCount(String buscar) {
        session.getTransaction().begin();
        Query q = session.createQuery("select count(*) from Sucursal as sucursal where ((nombre like :buscar))");
        q.setString("buscar", "%"+buscar+"%");
        long total = (Long) q.uniqueResult();
        session.getTransaction().commit();
        return total;
    }

    public void close(){
        session.clear();
        session.close();
    }
    
    @Override
    public void finalize(){
        try {
            close();
        } finally {
            try {
                super.finalize();
            } catch (Throwable ex) { }
        }
    }
    
}
