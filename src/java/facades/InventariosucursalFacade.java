/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facades;

import beans.Inventariosucursal;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import hibernate.HibernateUtil;
import org.hibernate.SQLQuery;

/**
 *
 * @author solid
 */
public class InventariosucursalFacade {

    private Session session;

    public InventariosucursalFacade() {
        session = HibernateUtil.getSessionFactory().openSession();
    }

    public Inventariosucursal getInventariosucursalByID(int idinventariosucursal) {
        Inventariosucursal inventariosucursal = (Inventariosucursal) session.get(Inventariosucursal.class, idinventariosucursal);
        return inventariosucursal;
    }

    public Integer saveInventariosucursal(Inventariosucursal inventariosucursal) {
        Integer identifier = 0;
        try {
            session.getTransaction().begin();
            identifier = (Integer) session.save(inventariosucursal);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
        return identifier;
    }

    public void updateInventariosucursal(Inventariosucursal inventariosucursal) {
        try {
            session.getTransaction().begin();
            Inventariosucursal o = getInventariosucursalByID(inventariosucursal.getIdinventariosucursal());
            o.setCantidad(inventariosucursal.getCantidad());
            o.setMaximo(inventariosucursal.getMaximo());
            o.setMinimo(inventariosucursal.getMinimo());
            o.setSucursal(inventariosucursal.getSucursal());
            o.setProducto(inventariosucursal.getProducto());
            session.update(o);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
    }

    public void deleteInventariosucursal(Inventariosucursal inventariosucursal) {
        try {
            session.getTransaction().begin();
            Inventariosucursal o = getInventariosucursalByID(inventariosucursal.getIdinventariosucursal());
            session.delete(o);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
    }

    public List<Inventariosucursal> getInventariosucursalAll() {
        session.getTransaction().begin();
        Query q = session.createQuery("from Inventariosucursal as inventariosucursal");
        List<Inventariosucursal> inventariosucursals = (List<Inventariosucursal>) q.list();
        session.getTransaction().commit();
        return inventariosucursals;
    }
    
    public Inventariosucursal getInventariosucursalBySucursalANDProducto(int idsucursal, int idproducto) {
        session.getTransaction().begin();
        Query q = session.createQuery("from Inventariosucursal as inventariosucursal where sucursal_idsucursal = :idsucursal and producto_idproducto = :idproducto");
        q.setInteger("idsucursal", idsucursal);
        q.setInteger("idproducto", idproducto);
        q.setMaxResults(1);
        Inventariosucursal inventariosucursal = (Inventariosucursal) q.uniqueResult();
        session.getTransaction().commit();
        return inventariosucursal;
    }

    public long getTotal() {
        session.getTransaction().begin();
        Query q = session.createQuery("select count(*) from Inventariosucursal as inventariosucursal");
        long total = (Long) q.uniqueResult();
        session.getTransaction().commit();
        return total;
    }

/*
    public List<Inventariosucursal> getInventariosucursalForProcess(String buscar, int idsucursal, String colname, String dir, int index, int size) {
        session.getTransaction().begin();
        Query q = session.createQuery("from Inventariosucursal as inventariosucursal where sucursal_idsucursal = :idsucursal order by "+colname+" "+dir);
        q.setInteger("idsucursal",idsucursal);
        q.setFirstResult(index);
        q.setMaxResults(size);
        List<Inventariosucursal> inventariosucursals = (List<Inventariosucursal>) q.list();
        session.getTransaction().commit();
        return inventariosucursals;
    }
    
    public Long getInventariosucursalForProcessCount(String buscar, int idsucursal) {
        session.getTransaction().begin();
        Query q = session.createQuery("select count(*) from Inventariosucursal as inventariosucursal where sucursal_idsucursal = :idsucursal");
        q.setInteger("idsucursal",idsucursal);
        long total = (Long) q.uniqueResult();
        session.getTransaction().commit();
        return total;
    }

*/
    
    
    public List<Object[]> getInventariosucursalForProcessJOINProducto(String buscar, int idsucursal, String colname, String dir, int index, int size) {
        session.getTransaction().begin();
        SQLQuery q = session.createSQLQuery("select "
                + "inventariosucursal.idinventariosucursal as idinventariosucursal,"
                + "producto.codigo as codigo,"
                + "producto.nombre as nombre,"
                + "inventariosucursal.cantidad as cantidad,"
                + "inventariosucursal.minimo as minimo,"
                + "inventariosucursal.maximo as maximo, "
                + "producto.tipo as tipo "
                + "from inventariosucursal "
                + "inner join producto on producto.idproducto = inventariosucursal.producto_idproducto "
                + "where "
                + "sucursal_idsucursal = :idsucursal "
                + "and ((producto.codigo like :buscar) or (producto.nombre like :buscar)) "
                + "order by "+colname+" "+dir);
        q.setInteger("idsucursal",idsucursal);
        q.setString("buscar", "%"+buscar+"%");
        q.setFirstResult(index);
        q.setMaxResults(size);
        List<Object[]> inventarioalmacens = (List<Object[]>) q.list();
        session.getTransaction().commit();
        return inventarioalmacens;
    }
    
    public java.math.BigInteger getInventariosucursalForProcessCountJOINProducto(String buscar, int idsucursal) {
        session.getTransaction().begin();
        SQLQuery q = session.createSQLQuery("select "
                + "count(*) "
                + "from inventariosucursal inner join producto on producto.idproducto = inventariosucursal.producto_idproducto "
                + "where sucursal_idsucursal = :idsucursal "
                + "and ((producto.codigo like :buscar) or (producto.nombre like :buscar))");
        q.setInteger("idsucursal",idsucursal);
        q.setString("buscar", "%"+buscar+"%");
        java.math.BigInteger total = (java.math.BigInteger) q.uniqueResult();
        session.getTransaction().commit();
        return total;
    }

    public void close(){
        session.clear();
        session.close();
    }
    
    @Override
    public void finalize(){
        try {
            close();
        } finally {
            try {
                super.finalize();
            } catch (Throwable ex) { }
        }
    }
    
}
