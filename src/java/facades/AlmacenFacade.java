/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facades;

import java.util.List;
import beans.Almacen;
import org.hibernate.Query;
import org.hibernate.Session;
import hibernate.HibernateUtil;
import org.hibernate.SQLQuery;

/**
 *
 * @author solid
 */
public class AlmacenFacade {

    private Session session;

    public AlmacenFacade() {
        session = HibernateUtil.getSessionFactory().openSession();
    }

    public Almacen getAlmacenByID(int idalmacen) {
        Almacen almacen = (Almacen) session.get(Almacen.class, idalmacen);
        return almacen;
    }

    public Integer saveAlmacen(Almacen almacen) {
        Integer identifier = 0;
        try {
            session.getTransaction().begin();
            identifier = (Integer) session.save(almacen);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
        return identifier;
    }

    public void updateAlmacen(Almacen almacen) {
        try {
            session.getTransaction().begin();
            Almacen o = getAlmacenByID(almacen.getIdalmacen());
            o.setNombre(almacen.getNombre());
            session.update(o);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
    }

    public void deleteAlmacen(Almacen almacen) {
        try {
            session.getTransaction().begin();
            Almacen o = getAlmacenByID(almacen.getIdalmacen());
            session.delete(o);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
    }

    public List<Almacen> getAlmacenAll() {
        session.getTransaction().begin();
        Query q = session.createQuery("from Almacen as almacen order by nombre");
        List<Almacen> almacens = (List<Almacen>) q.list();
        session.getTransaction().commit();
        return almacens;
    }
    
    public List<Almacen> getAlmacenBySucursal(int idsucursal) {
        session.getTransaction().begin();
        Query q = session.createQuery("from Almacen as almacen where sucursal_idsucursal = :idsucursal");
        q.setInteger("idsucursal", idsucursal);
        List<Almacen> almacens = (List<Almacen>) q.list();
        session.getTransaction().commit();
        return almacens;
    }

    public long getTotal() {
        session.getTransaction().begin();
        Query q = session.createQuery("select count(*) from Almacen as almacen");
        long total = (Long) q.uniqueResult();
        session.getTransaction().commit();
        return total;
    }
 
    public List<Almacen> getAlmacenForProcess(String buscar, String colname, String dir, int index, int size) {
        session.getTransaction().begin();
        Query q = session.createQuery("from Almacen as almacen where ((nombre like :buscar)) order by "+colname+" "+dir);
        q.setString("buscar", "%"+buscar+"%");
        q.setFirstResult(index);
        q.setMaxResults(size);
        List<Almacen> almacens = (List<Almacen>) q.list();
        session.getTransaction().commit();
        return almacens;
    }
    
    public Long getAlmacenForProcessCount(String buscar) {
        session.getTransaction().begin();
        Query q = session.createQuery("select count(*) from Almacen as almacen where ((nombre like :buscar))");
        q.setString("buscar", "%"+buscar+"%");
        long total = (Long) q.uniqueResult();
        session.getTransaction().commit();
        return total;
    }
    
    /*
    public List<Object[]> getAlmacenForProcessJOINSucursal(String buscar, String colname, String dir, int index, int size) {
        session.getTransaction().begin();
        SQLQuery q = session.createSQLQuery("select "
                + "almacen.idalmacen as idalmacen, "
                + "almacen.nombre as a_nombre, "
                + "sucursal.nombre as s_nombre "
                + "from Almacen as almacen inner join Sucursal as sucursal on almacen.sucursal_idsucursal = sucursal.idsucursal "
                + "where "
                + "((almacen.nombre like :buscar) or (sucursal.nombre like :buscar)) "
                + "order by "+colname+" "+dir);
        q.setString("buscar", "%"+buscar+"%");
        q.setFirstResult(index);
        q.setMaxResults(size);
        List<Object[]> almacens = (List<Object[]>) q.list();
        session.getTransaction().commit();
        return almacens;
    }
    
    public java.math.BigInteger getAlmacenForProcessCountJOINSucursal(String buscar) {
        session.getTransaction().begin();
        SQLQuery q = session.createSQLQuery("select "
                + "count(*) "
                + "from Almacen as almacen inner join Sucursal as sucursal on almacen.sucursal_idsucursal = sucursal.idsucursal "
                + "where "
                + "((almacen.nombre like :buscar) or (sucursal.nombre like :buscar))");
        q.setString("buscar", "%"+buscar+"%");
        java.math.BigInteger total = (java.math.BigInteger) q.uniqueResult();
        session.getTransaction().commit();
        return total;
    }
*/

    public void close(){
        session.clear();
        session.close();
    }
    
    @Override
    public void finalize(){
        try {
            close();
        } finally {
            try {
                super.finalize();
            } catch (Throwable ex) { }
        }
    }
    
}
