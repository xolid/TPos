/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facades;

import java.util.List;
import beans.Categoria;
import org.hibernate.Query;
import org.hibernate.Session;
import hibernate.HibernateUtil;

/**
 *
 * @author solid
 */
public class CategoriaFacade {

    private Session session;

    public CategoriaFacade() {
        session = HibernateUtil.getSessionFactory().openSession();
    }

    public Categoria getCategoriaByID(int idcategoria) {
        Categoria categoria = (Categoria) session.get(Categoria.class, idcategoria);
        return categoria;
    }

    public Integer saveCategoria(Categoria categoria) {
        Integer identifier = 0;
        try {
            session.getTransaction().begin();
            identifier = (Integer) session.save(categoria);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
        return identifier;
    }

    public void updateCategoria(Categoria categoria) {
        try {
            session.getTransaction().begin();
            Categoria o = getCategoriaByID(categoria.getIdcategoria());
            o.setNombre(categoria.getNombre());
            session.update(o);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
    }

    public void deleteCategoria(Categoria categoria) {
        try {
            session.getTransaction().begin();
            Categoria o = getCategoriaByID(categoria.getIdcategoria());
            session.delete(o);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
    }

    public List<Categoria> getCategoriaAll() {
        session.getTransaction().begin();
        Query q = session.createQuery("from Categoria as categoria order by nombre");
        List<Categoria> categorias = (List<Categoria>) q.list();
        session.getTransaction().commit();
        return categorias;
    }

    public long getTotal() {
        session.getTransaction().begin();
        Query q = session.createQuery("select count(*) from Categoria as categoria");
        long total = (Long) q.uniqueResult();
        session.getTransaction().commit();
        return total;
    }
 
    public List<Categoria> getCategoriaForProcess(String buscar, String colname, String dir, int index, int size) {
        session.getTransaction().begin();
        Query q = session.createQuery("from Categoria as categoria where ((nombre like :buscar)) order by "+colname+" "+dir);
        q.setString("buscar", "%"+buscar+"%");
        q.setFirstResult(index);
        q.setMaxResults(size);
        List<Categoria> categorias = (List<Categoria>) q.list();
        session.getTransaction().commit();
        return categorias;
    }
    
    public Long getCategoriaForProcessCount(String buscar) {
        session.getTransaction().begin();
        Query q = session.createQuery("select count(*) from Categoria as categoria where ((nombre like :buscar))");
        q.setString("buscar", "%"+buscar+"%");
        long total = (Long) q.uniqueResult();
        session.getTransaction().commit();
        return total;
    }

    public void close(){
        session.clear();
        session.close();
    }
    
    @Override
    public void finalize(){
        try {
            close();
        } finally {
            try {
                super.finalize();
            } catch (Throwable ex) { }
        }
    }
    
}
