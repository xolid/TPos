/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facades;

import java.util.List;
import beans.Tipo;
import org.hibernate.Query;
import org.hibernate.Session;
import hibernate.HibernateUtil;

/**
 *
 * @author solid
 */
public class TipoFacade {

    private Session session;

    public TipoFacade() {
        session = HibernateUtil.getSessionFactory().openSession();
    }

    public Tipo getTipoByID(int idtipo) {
        Tipo tipo = (Tipo) session.get(Tipo.class, idtipo);
        return tipo;
    }

    public Integer saveTipo(Tipo tipo) {
        Integer identifier = 0;
        try {
            session.getTransaction().begin();
            identifier = (Integer) session.save(tipo);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
        return identifier;
    }

    public void updateTipo(Tipo tipo) {
        try {
            session.getTransaction().begin();
            Tipo o = getTipoByID(tipo.getIdtipo());
            o.setNombre(tipo.getNombre());
            session.update(o);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
    }

    public void deleteTipo(Tipo tipo) {
        try {
            session.getTransaction().begin();
            Tipo o = getTipoByID(tipo.getIdtipo());
            session.delete(o);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
    }

    public List<Tipo> getTipoAll() {
        session.getTransaction().begin();
        Query q = session.createQuery("from Tipo as tipo order by nombre");
        List<Tipo> tipos = (List<Tipo>) q.list();
        session.getTransaction().commit();
        return tipos;
    }

    public long getTotal() {
        session.getTransaction().begin();
        Query q = session.createQuery("select count(*) from Tipo as tipo");
        long total = (Long) q.uniqueResult();
        session.getTransaction().commit();
        return total;
    }
 
    public List<Tipo> getTipoForProcess(String buscar, String colname, String dir, int index, int size) {
        session.getTransaction().begin();
        Query q = session.createQuery("from Tipo as tipo where ((nombre like :buscar)) order by "+colname+" "+dir);
        q.setString("buscar", "%"+buscar+"%");
        q.setFirstResult(index);
        q.setMaxResults(size);
        List<Tipo> tipos = (List<Tipo>) q.list();
        session.getTransaction().commit();
        return tipos;
    }
    
    public Long getTipoForProcessCount(String buscar) {
        session.getTransaction().begin();
        Query q = session.createQuery("select count(*) from Tipo as tipo where ((nombre like :buscar))");
        q.setString("buscar", "%"+buscar+"%");
        long total = (Long) q.uniqueResult();
        session.getTransaction().commit();
        return total;
    }

    public void close(){
        session.clear();
        session.close();
    }
    
    @Override
    public void finalize(){
        try {
            close();
        } finally {
            try {
                super.finalize();
            } catch (Throwable ex) { }
        }
    }
    
}
