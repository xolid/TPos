/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facades;

import java.util.List;
import beans.Cliente;
import org.hibernate.Query;
import org.hibernate.Session;
import hibernate.HibernateUtil;

/**
 *
 * @author solid
 */
public class ClienteFacade {

    private Session session;

    public ClienteFacade() {
        session = HibernateUtil.getSessionFactory().openSession();
    }

    public Cliente getClienteByID(int idcliente) {
        Cliente cliente = (Cliente) session.get(Cliente.class, idcliente);
        return cliente;
    }

    public Integer saveCliente(Cliente cliente) {
        Integer identifier = 0;
        try {
            session.getTransaction().begin();
            identifier = (Integer) session.save(cliente);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
        return identifier;
    }

    public void updateCliente(Cliente cliente) {
        try {
            session.getTransaction().begin();
            Cliente o = getClienteByID(cliente.getIdcliente());
            o.setNombre(cliente.getNombre());
            o.setRazon(cliente.getRazon());
            o.setRfc(cliente.getRfc());
            o.setDireccion(cliente.getDireccion());
            o.setTelefono(cliente.getTelefono());
            o.setDireccion(cliente.getDireccion());
            session.update(o);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
    }

    public void deleteCliente(Cliente cliente) {
        try {
            session.getTransaction().begin();
            Cliente o = getClienteByID(cliente.getIdcliente());
            session.delete(o);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
    }

    public List<Cliente> getClienteAll() {
        session.getTransaction().begin();
        Query q = session.createQuery("from Cliente as cliente order by nombre");
        List<Cliente> clientes = (List<Cliente>) q.list();
        session.getTransaction().commit();
        return clientes;
    }

    public long getTotal() {
        session.getTransaction().begin();
        Query q = session.createQuery("select count(*) from Cliente as cliente");
        long total = (Long) q.uniqueResult();
        session.getTransaction().commit();
        return total;
    }
 
    public List<Cliente> getClienteForProcess(String buscar, String colname, String dir, int index, int size) {
        session.getTransaction().begin();
        Query q = session.createQuery("from Cliente as cliente where ((nombre like :buscar) or (rfc like :buscar) or (razon like :buscar)) order by "+colname+" "+dir);
        q.setString("buscar", "%"+buscar+"%");
        q.setFirstResult(index);
        q.setMaxResults(size);
        List<Cliente> clientes = (List<Cliente>) q.list();
        session.getTransaction().commit();
        return clientes;
    }
    
    public Long getClienteForProcessCount(String buscar) {
        session.getTransaction().begin();
        Query q = session.createQuery("select count(*) from Cliente as cliente where ((nombre like :buscar) or (rfc like :buscar) or (razon like :buscar))");
        q.setString("buscar", "%"+buscar+"%");
        long total = (Long) q.uniqueResult();
        session.getTransaction().commit();
        return total;
    }

    public void close(){
        session.clear();
        session.close();
    }
    
    @Override
    public void finalize(){
        try {
            close();
        } finally {
            try {
                super.finalize();
            } catch (Throwable ex) { }
        }
    }
    
}
