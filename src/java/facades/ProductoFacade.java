/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facades;

import java.util.List;
import beans.Producto;
import org.hibernate.Query;
import org.hibernate.Session;
import hibernate.HibernateUtil;
import org.hibernate.SQLQuery;

/**
 *
 * @author solid
 */
public class ProductoFacade {

    private Session session;

    public ProductoFacade() {
        session = HibernateUtil.getSessionFactory().openSession();
    }

    public Producto getProductoByID(int idproducto) {
        Producto producto = (Producto) session.get(Producto.class, idproducto);
        return producto;
    }

    public Integer saveProducto(Producto producto) {
        Integer identifier = 0;
        try {
            session.getTransaction().begin();
            identifier = (Integer) session.save(producto);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
        return identifier;
    }

    public void updateProducto(Producto producto) {
        try {
            session.getTransaction().begin();
            Producto o = getProductoByID(producto.getIdproducto());
            o.setCodigo(producto.getCodigo());
            o.setNombre(producto.getNombre());
            o.setMarca(producto.getMarca());
            o.setClave(producto.getClave());
            o.setUnidad(producto.getUnidad());
            o.setPrecio(producto.getPrecio());
            o.setTipo(producto.getTipo());
            o.setImpuesto(producto.getImpuesto());
            o.setCategoria(producto.getCategoria());
            session.update(o);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
    }

    public void deleteProducto(Producto producto) {
        try {
            session.getTransaction().begin();
            Producto o = getProductoByID(producto.getIdproducto());
            session.delete(o);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
    }

    public List<Producto> getProductoAll() {
        session.getTransaction().begin();
        Query q = session.createQuery("from Producto as producto order by nombre");
        List<Producto> productos = (List<Producto>) q.list();
        session.getTransaction().commit();
        return productos;
    }
    
    public Producto getProductoByNombre(String nombre) {
        session.getTransaction().begin();
        Query q = session.createQuery("from Producto as producto where nombre = :nombre");
        q.setString("nombre", nombre);
        q.setMaxResults(1);
        Producto productos = (Producto) q.uniqueResult();
        session.getTransaction().commit();
        return productos;
    }
    
    public List<Producto> getProductoByCategoria(int idcategoria) {
        session.getTransaction().begin();
        Query q = session.createQuery("from Producto as producto where categoria_idcategoria = :idcategoria");
        q.setInteger("idcategoria", idcategoria);
        List<Producto> productos = (List<Producto>) q.list();
        session.getTransaction().commit();
        return productos;
    }

    public long getTotal() {
        session.getTransaction().begin();
        Query q = session.createQuery("select count(*) from Producto as producto");
        long total = (Long) q.uniqueResult();
        session.getTransaction().commit();
        return total;
    }
    
    public List<Object[]> getProductoForProcessByAlmacenMARKED(String buscar, int idalmacen, String colname, String dir, int index, int size) {
        session.getTransaction().begin();
        SQLQuery q = session.createSQLQuery("select "
                + "producto.idproducto, "
                + "producto.nombre as pnombre, "
                + "producto.codigo, "
                + "producto.clave, "
                + "producto.unidad, "
                + "categoria.nombre as cnombre, "
                + "(select idinventarioalmacen from inventarioalmacen where inventarioalmacen.producto_idproducto = producto.idproducto and inventarioalmacen.almacen_idalmacen = :idalmacen limit 1) as usado, "
                + "producto.tipo "
                + "from producto "
                + "     inner join categoria on producto.categoria_idcategoria = categoria.idcategoria "
                + "where ((producto.codigo like :buscar) or (producto.nombre like :buscar) or (producto.clave like :buscar) or (producto.unidad like :buscar) or (categoria.nombre like :buscar)) order by "+colname+" "+dir);
        q.setInteger("idalmacen", idalmacen);
        q.setString("buscar", "%"+buscar+"%");
        q.setFirstResult(index);
        q.setMaxResults(size);
        List<Object[]> productos = (List<Object[]>) q.list();
        session.getTransaction().commit();
        return productos;
    }
      
    public List<Object[]> getProductoForProcessBySucursalMARKED(String buscar, int idsucursal, String colname, String dir, int index, int size) {
        session.getTransaction().begin();
        SQLQuery q = session.createSQLQuery("select "
                + "producto.idproducto, "
                + "producto.nombre as pnombre, "
                + "producto.codigo, "
                + "producto.clave, "
                + "producto.unidad, "
                + "categoria.nombre as cnombre, "
                + "(select idinventariosucursal from inventariosucursal where inventariosucursal.producto_idproducto = producto.idproducto and inventariosucursal.sucursal_idsucursal = :idsucursal limit 1) as usado, "
                + "producto.tipo "
                + "from producto "
                + "     inner join categoria on producto.categoria_idcategoria = categoria.idcategoria "
                + "where ((producto.codigo like :buscar) or (producto.nombre like :buscar) or (producto.clave like :buscar) or (producto.unidad like :buscar) or (categoria.nombre like :buscar)) order by "+colname+" "+dir);
        q.setInteger("idsucursal", idsucursal);
        q.setString("buscar", "%"+buscar+"%");
        q.setFirstResult(index);
        q.setMaxResults(size);
        List<Object[]> productos = (List<Object[]>) q.list();
        session.getTransaction().commit();
        return productos;
    }


    public List<Object[]> getProductoForProcessOrderable(String buscar, String colname, String dir, int index, int size) {
        session.getTransaction().begin();
        SQLQuery q = session.createSQLQuery("select "
                + "     producto.idproducto, "
                + "     producto.nombre pnombre,  "
                + "     producto.codigo,  "
                + "     producto.clave,  "
                + "     producto.unidad,  "
                + "     categoria.nombre cnombre,  "
                + "     producto.tipo, "
                + "     producto.precio, "
                + "     producto.marca "
                + "from producto "
                + "     inner join categoria on producto.categoria_idcategoria = categoria.idcategoria "
                + "where ((producto.codigo like :buscar) or (producto.nombre like :buscar) or (producto.clave like :buscar) or (producto.unidad like :buscar) or (categoria.nombre like :buscar)) order by "+colname+" "+dir);
        q.setString("buscar", "%"+buscar+"%");
        q.setFirstResult(index);
        q.setMaxResults(size);
        List<Object[]> productos = (List<Object[]>) q.list();
        session.getTransaction().commit();
        return productos;
    }
    
    public List<Object[]> getProductoForProcessOrderableOnlyPROD(String buscar, String colname, String dir, int index, int size) {
        session.getTransaction().begin();
        SQLQuery q = session.createSQLQuery("select "
                + "     producto.idproducto, "
                + "     producto.nombre pnombre,  "
                + "     producto.codigo,  "
                + "     producto.clave,  "
                + "     producto.unidad,  "
                + "     categoria.nombre cnombre,  "
                + "     producto.tipo, "
                + "     producto.precio, "
                + "     producto.marca "
                + "from producto "
                + "     inner join categoria on producto.categoria_idcategoria = categoria.idcategoria "
                + "where ((producto.codigo like :buscar) or (producto.nombre like :buscar) or (producto.clave like :buscar) or (producto.unidad like :buscar) or (categoria.nombre like :buscar)) and producto.tipo = 0 order by "+colname+" "+dir);
        q.setString("buscar", "%"+buscar+"%");
        q.setFirstResult(index);
        q.setMaxResults(size);
        List<Object[]> productos = (List<Object[]>) q.list();
        session.getTransaction().commit();
        return productos;
    }    
   
    public Long getProductoForProcessCount(String buscar) {
        session.getTransaction().begin();
        Query q = session.createQuery("select count(*) from Producto as producto where ((producto.codigo like :buscar) or (producto.nombre like :buscar) or (producto.clave like :buscar) or (producto.unidad like :buscar) or (categoria.nombre like :buscar))");
        q.setString("buscar", "%"+buscar+"%");
        long total = (Long) q.uniqueResult();
        session.getTransaction().commit();
        return total;
    }
    
    public void close(){
        session.clear();
        session.close();
    }
    
    @Override
    public void finalize(){
        try {
            close();
        } finally {
            try {
                super.finalize();
            } catch (Throwable ex) { }
        }
    }
    
}
