/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facades;

import java.util.List;
import beans.Detallepago;
import org.hibernate.Query;
import org.hibernate.Session;
import hibernate.HibernateUtil;
import org.hibernate.SQLQuery;

/**
 *
 * @author solid
 */
public class DetallepagoFacade {

    private Session session;

    public DetallepagoFacade() {
        session = HibernateUtil.getSessionFactory().openSession();
    }

    public Detallepago getDetallepagoByID(int iddetallepago) {
        Detallepago detallepago = (Detallepago) session.get(Detallepago.class, iddetallepago);
        return detallepago;
    }

    public Integer saveDetallepago(Detallepago detallepago) {
        Integer identifier = 0;
        try {
            session.getTransaction().begin();
            identifier = (Integer) session.save(detallepago);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
        return identifier;
    }

    public void updateDetallepago(Detallepago detallepago) {
        try {
            session.getTransaction().begin();
            Detallepago o = getDetallepagoByID(detallepago.getIddetallepago());
            o.setMonto(detallepago.getMonto());
            o.setTipo(detallepago.getTipo());
            o.setVenta(detallepago.getVenta());
            session.update(o);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
    }

    public void deleteDetallepago(Detallepago detallepago) {
        try {
            session.getTransaction().begin();
            Detallepago o = getDetallepagoByID(detallepago.getIddetallepago());
            session.delete(o);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
    }
    
    public int deleteByVenta(int idventa) {
        session.getTransaction().begin();
        SQLQuery q = session.createSQLQuery("delete from Detallepago as detallepago where venta_idventa = :idventa");
        q.setInteger("idventa", idventa);
        int total = q.executeUpdate();
        session.getTransaction().commit();
        return total;
    }

    public long getTotal() {
        session.getTransaction().begin();
        Query q = session.createQuery("select count(*) from Detallepago as detallepago");
        long total = (Long) q.uniqueResult();
        session.getTransaction().commit();
        return total;
    }
    
    public List<Detallepago> getDetallepagoByVenta(int idventa) {
        session.getTransaction().begin();
        Query q = session.createQuery("from Detallepago as detallepago where venta_idventa = :idventa");
        q.setInteger("idventa", idventa);
        List<Detallepago> detallepagos = (List<Detallepago>) q.list();
        session.getTransaction().commit();
        return detallepagos;
    }    

    public void close(){
        session.clear();
        session.close();
    }
    
    @Override
    public void finalize(){
        try {
            close();
        } finally {
            try {
                super.finalize();
            } catch (Throwable ex) { }
        }
    }
    
}
