/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facades;

import java.util.List;
import beans.Venta;
import org.hibernate.Query;
import org.hibernate.Session;
import hibernate.HibernateUtil;
import org.hibernate.SQLQuery;

/**
 *
 * @author solid
 */
public class VentaFacade {

    private Session session;

    public VentaFacade() {
        session = HibernateUtil.getSessionFactory().openSession();
    }

    public Venta getVentaByID(int idventa) {
        Venta venta = (Venta) session.get(Venta.class, idventa);
        return venta;
    }

    public Integer saveVenta(Venta venta) {
        Integer identifier = 0;
        try {
            session.getTransaction().begin();
            identifier = (Integer) session.save(venta);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
        return identifier;
    }

    public void updateVenta(Venta venta) {
        try {
            session.getTransaction().begin();
            Venta o = getVentaByID(venta.getIdventa());
            o.setFventa(venta.getFventa());
            o.setEstado(venta.getEstado());
            o.setCliente(venta.getCliente());
            o.setUsuario(venta.getUsuario());
            o.setSucursal(venta.getSucursal());
            session.update(o);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
    }

    public void deleteVenta(Venta venta) {
        try {
            session.getTransaction().begin();
            Venta o = getVentaByID(venta.getIdventa());
            session.delete(o);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
    }

    public List<Venta> getVentaAll() {
        session.getTransaction().begin();
        Query q = session.createQuery("from Venta as venta");
        List<Venta> ventas = (List<Venta>) q.list();
        session.getTransaction().commit();
        return ventas;
    }

    public long getTotal() {
        session.getTransaction().begin();
        Query q = session.createQuery("select count(*) from Venta as venta");
        long total = (Long) q.uniqueResult();
        session.getTransaction().commit();
        return total;
    }
    
    public List<beans.Venta> getVentaBySucursalForProcess(String buscar, int idsucursal, String colname, String dir, int index, int size) {
        session.getTransaction().begin();
        Query q = session.createQuery("FROM Venta venta WHERE ((cliente like :buscar) or (usuario like :buscar) or (fventa like :buscar)) and sucursal_idsucursal = :idsucursal order by "+colname+" "+dir);
        q.setString("buscar", "%"+buscar+"%");
        q.setInteger("idsucursal", idsucursal);
        q.setFirstResult(index);
        q.setMaxResults(size);
        List<beans.Venta> ventas = (List<beans.Venta>) q.list();
        session.getTransaction().commit();
        return ventas;
    }
    
    public Long getVentaBySucursalForProcessCount(String buscar, int idsucursal) {
        session.getTransaction().begin();
        Query q = session.createQuery("select count(*) FROM Venta venta WHERE ((cliente like :buscar) or (usuario like :buscar) or (fventa like :buscar)) and sucursal_idsucursal = :idsucursal");
        q.setString("buscar", "%"+buscar+"%");
        q.setInteger("idsucursal", idsucursal);
        Long total =  (Long)q.uniqueResult();
        session.getTransaction().commit();
        return total;
    }
    
    public List<beans.Venta> getVentaForProcess(String buscar, String colname, String dir, int index, int size) {
        session.getTransaction().begin();
        Query q = session.createQuery("FROM Venta venta WHERE (cliente like :buscar) or (usuario like :buscar) or (fventa like :buscar)) order by "+colname+" "+dir);
        q.setString("buscar", "%"+buscar+"%");
        q.setFirstResult(index);
        q.setMaxResults(size);
        List<beans.Venta> ventas = (List<beans.Venta>) q.list();
        session.getTransaction().commit();
        return ventas;
    }
    
    public Long getVentaForProcessCount(String buscar) {
        session.getTransaction().begin();
        Query q = session.createQuery("select count(*) FROM Venta venta WHERE ((cliente like :buscar) or (usuario like :buscar) or (fventa like :buscar)) ");
        q.setString("buscar", "%"+buscar+"%");
        Long total =  (Long)q.uniqueResult();
        session.getTransaction().commit();
        return total;
    }

    public void close(){
        session.clear();
        session.close();
    }
    
    @Override
    public void finalize(){
        try {
            close();
        } finally {
            try {
                super.finalize();
            } catch (Throwable ex) { }
        }
    }
    
}
