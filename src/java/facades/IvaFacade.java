/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facades;

import java.util.List;
import beans.Iva;
import org.hibernate.Query;
import org.hibernate.Session;
import hibernate.HibernateUtil;

/**
 *
 * @author solid
 */
public class IvaFacade {

    private Session session;

    public IvaFacade() {
        session = HibernateUtil.getSessionFactory().openSession();
    }

    public Iva getIvaByID(int idiva) {
        Iva iva = (Iva) session.get(Iva.class, idiva);
        return iva;
    }

    public Integer saveIva(Iva iva) {
        Integer identifier = 0;
        try {
            session.getTransaction().begin();
            identifier = (Integer) session.save(iva);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
        return identifier;
    }

    public void updateIva(Iva iva) {
        try {
            session.getTransaction().begin();
            Iva o = getIvaByID(iva.getIdiva());
            o.setNombre(iva.getNombre());
            o.setPorcentaje(iva.getPorcentaje());
            session.update(o);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
    }

    public void deleteIva(Iva iva) {
        try {
            session.getTransaction().begin();
            Iva o = getIvaByID(iva.getIdiva());
            session.delete(o);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
    }

    public List<Iva> getIvaAll() {
        session.getTransaction().begin();
        Query q = session.createQuery("from Iva as iva order by nombre");
        List<Iva> ivas = (List<Iva>) q.list();
        session.getTransaction().commit();
        return ivas;
    }

    public long getTotal() {
        session.getTransaction().begin();
        Query q = session.createQuery("select count(*) from Iva as iva");
        long total = (Long) q.uniqueResult();
        session.getTransaction().commit();
        return total;
    }
 
    public List<Iva> getIvaForProcess(String buscar, String colname, String dir, int index, int size) {
        session.getTransaction().begin();
        Query q = session.createQuery("from Iva as iva where ((nombre like :buscar)) order by "+colname+" "+dir);
        q.setString("buscar", "%"+buscar+"%");
        q.setFirstResult(index);
        q.setMaxResults(size);
        List<Iva> ivas = (List<Iva>) q.list();
        session.getTransaction().commit();
        return ivas;
    }
    
    public Long getIvaForProcessCount(String buscar) {
        session.getTransaction().begin();
        Query q = session.createQuery("select count(*) from Iva as iva where ((nombre like :buscar))");
        q.setString("buscar", "%"+buscar+"%");
        long total = (Long) q.uniqueResult();
        session.getTransaction().commit();
        return total;
    }

    public void close(){
        session.clear();
        session.close();
    }
    
    @Override
    public void finalize(){
        try {
            close();
        } finally {
            try {
                super.finalize();
            } catch (Throwable ex) { }
        }
    }
    
}
