/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facades;

import java.util.List;
import beans.Usuario;
import org.hibernate.Query;
import org.hibernate.Session;
import hibernate.HibernateUtil;

/**
 *
 * @author solid
 */
public class UsuarioFacade {

    private Session session;

    public UsuarioFacade() {
        session = HibernateUtil.getSessionFactory().openSession();
    }

    public Usuario getUsuarioByID(int idusuario) {
        Usuario usuario = (Usuario) session.get(Usuario.class, idusuario);
        return usuario;
    }

    public Integer saveUsuario(Usuario usuario) {
        Integer identifier = 0;
        try {
            session.getTransaction().begin();
            identifier = (Integer) session.save(usuario);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
        return identifier;
    }

    public void updateUsuario(Usuario usuario) {
        try {
            session.getTransaction().begin();
            Usuario o = getUsuarioByID(usuario.getIdusuario());
            o.setNombre(usuario.getNombre());
            o.setEmail(usuario.getEmail());
            o.setPass(usuario.getPass());
            o.setTelefono(usuario.getTelefono());
            o.setFingreso(usuario.getFingreso());
            o.setFegreso(usuario.getFegreso());
            o.setRol(usuario.getRol());
            o.setSucursal(usuario.getSucursal());
            session.update(o);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
    }

    public void deleteUsuario(Usuario usuario) {
        try {
            session.getTransaction().begin();
            Usuario o = getUsuarioByID(usuario.getIdusuario());
            session.delete(o);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
    }

    public List<Usuario> getUsuarioAll() {
        session.getTransaction().begin();
        Query q = session.createQuery("from Usuario as usuario order by nombre");
        List<Usuario> usuarios = (List<Usuario>) q.list();
        session.getTransaction().commit();
        return usuarios;
    }

    public long getTotal() {
        session.getTransaction().begin();
        Query q = session.createQuery("select count(*) from Usuario as usuario");
        long total = (Long) q.uniqueResult();
        session.getTransaction().commit();
        return total;
    }

    public Usuario getUsuarioByMailPass(String email, String password) {
        session.getTransaction().begin();
        Query q = session.createQuery("from Usuario as usuario where email = :email and pass = :password and fegreso is null");
        q.setString("email", email);
        q.setString("password", password);
        Usuario usuario = (Usuario) q.uniqueResult();
        session.getTransaction().commit();
        return usuario;
    }
 
    public List<Usuario> getUsuarioForProcess(String buscar, String colname, String dir, int index, int size) {
        session.getTransaction().begin();
        Query q = session.createQuery("from Usuario as usuario where ((nombre like :buscar) or (email like :buscar) or (fingreso like :buscar)) order by "+colname+" "+dir);
        q.setString("buscar", "%"+buscar+"%");
        q.setFirstResult(index);
        q.setMaxResults(size);
        List<Usuario> usuarios = (List<Usuario>) q.list();
        session.getTransaction().commit();
        return usuarios;
    }
    
    public Long getUsuarioForProcessCount(String buscar) {
        session.getTransaction().begin();
        Query q = session.createQuery("select count(*) from Usuario as usuario where ((nombre like :buscar) or (email like :buscar) or (fingreso like :buscar))");
        q.setString("buscar", "%"+buscar+"%");
        long total = (Long) q.uniqueResult();
        session.getTransaction().commit();
        return total;
    }

    public void close(){
        session.clear();
        session.close();
    }
    
    @Override
    public void finalize(){
        try {
            close();
        } finally {
            try {
                super.finalize();
            } catch (Throwable ex) { }
        }
    }
    
}
