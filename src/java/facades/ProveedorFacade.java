/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facades;

import java.util.List;
import beans.Proveedor;
import org.hibernate.Query;
import org.hibernate.Session;
import hibernate.HibernateUtil;

/**
 *
 * @author solid
 */
public class ProveedorFacade {

    private Session session;

    public ProveedorFacade() {
        session = HibernateUtil.getSessionFactory().openSession();
    }

    public Proveedor getProveedorByID(int idproveedor) {
        Proveedor proveedor = (Proveedor) session.get(Proveedor.class, idproveedor);
        return proveedor;
    }

    public Integer saveProveedor(Proveedor proveedor) {
        Integer identifier = 0;
        try {
            session.getTransaction().begin();
            identifier = (Integer) session.save(proveedor);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
        return identifier;
    }

    public void updateProveedor(Proveedor proveedor) {
        try {
            session.getTransaction().begin();
            Proveedor o = getProveedorByID(proveedor.getIdproveedor());
            o.setNombre(proveedor.getNombre());
            o.setRazon(proveedor.getRazon());
            o.setRfc(proveedor.getRfc());
            o.setDireccion(proveedor.getDireccion());
            o.setTelefono(proveedor.getTelefono());
            o.setDireccion(proveedor.getDireccion());
            session.update(o);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
    }

    public void deleteProveedor(Proveedor proveedor) {
        try {
            session.getTransaction().begin();
            Proveedor o = getProveedorByID(proveedor.getIdproveedor());
            session.delete(o);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
    }

    public List<Proveedor> getProveedorAll() {
        session.getTransaction().begin();
        Query q = session.createQuery("from Proveedor as proveedor order by nombre");
        List<Proveedor> proveedors = (List<Proveedor>) q.list();
        session.getTransaction().commit();
        return proveedors;
    }

    public long getTotal() {
        session.getTransaction().begin();
        Query q = session.createQuery("select count(*) from Proveedor as proveedor");
        long total = (Long) q.uniqueResult();
        session.getTransaction().commit();
        return total;
    }
 
    public List<Proveedor> getProveedorForProcess(String buscar, String colname, String dir, int index, int size) {
        session.getTransaction().begin();
        Query q = session.createQuery("from Proveedor as proveedor where ((nombre like :buscar) or (rfc like :buscar) or (razon like :buscar)) order by "+colname+" "+dir);
        q.setString("buscar", "%"+buscar+"%");
        q.setFirstResult(index);
        q.setMaxResults(size);
        List<Proveedor> proveedors = (List<Proveedor>) q.list();
        session.getTransaction().commit();
        return proveedors;
    }
    
    public Long getProveedorForProcessCount(String buscar) {
        session.getTransaction().begin();
        Query q = session.createQuery("select count(*) from Proveedor as proveedor where ((nombre like :buscar) or (rfc like :buscar) or (razon like :buscar))");
        q.setString("buscar", "%"+buscar+"%");
        long total = (Long) q.uniqueResult();
        session.getTransaction().commit();
        return total;
    }

    public void close(){
        session.clear();
        session.close();
    }
    
    @Override
    public void finalize(){
        try {
            close();
        } finally {
            try {
                super.finalize();
            } catch (Throwable ex) { }
        }
    }
    
}
